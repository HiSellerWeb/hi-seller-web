@echo off

taskkill java*
tskill java*

rd projects\commons\build /s /q
rd projects\commons\dist /s /q

rd projects\account\build /s /q
rd projects\account\dist /s /q

rd projects\checkout\build /s /q
rd projects\checkout\dist /s /q

rd projects\stock\build /s /q
rd projects\stock\dist /s /q

rd projects\front\build /s /q
rd projects\front\dist /s /q

CD "../tomcat\work\Catalina\localhost"
rd account /s  /q
rd checkout /s /q
rd stock /s /q
rd front /s /q