@echo off

taskkill java*
tskill java*

rd projects\commons\lib /s /q
rd projects\account\lib /s /q
rd projects\checkout\lib /s /q
rd projects\stock\lib /s /q
rd projects\front\lib /s /q

CD "..\tomcat\work\Catalina\localhost"
rd account /s  /q
rd checkout /s /q
rd stock /s /q
rd front /s /q