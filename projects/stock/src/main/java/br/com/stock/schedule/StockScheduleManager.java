package br.com.stock.schedule;

import br.com.commons.appConfig.AppConfig;
import br.com.commons.schedule.AbstractScheduleManager;
import br.com.stock.config.ConfigEnum;
import br.com.stock.schedule.task.StockTask;
import br.com.stock.service.SKUService;
import javax.annotation.PostConstruct;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public final class StockScheduleManager extends AbstractScheduleManager {

    private static final Log LOG = LogFactory.getLog(StockScheduleManager.class);
    private StockTask stockTask;

    private final SKUService skuService;
    private final AppConfig appConfig;

    public StockScheduleManager(SKUService skuServive, AppConfig appConfig) {
        this.skuService = skuServive;
        this.appConfig = appConfig;

        schedule();
    }

    @Override
    @PostConstruct
    public void schedule() {
        scheduleStock();
    }

    private void scheduleStock() {
        cancelSchedule(stockTask);
        stockTask = new StockTask(skuService);
        scheduleTask(stockTask, appConfig.getValueLongDefault(0L, ConfigEnum.TIME_TO_VALIDATE_STOCK, 1L), appConfig.getValueLongDefault(0L, ConfigEnum.TIME_TO_VALIDATE_STOCK, 720L));
    }
}
