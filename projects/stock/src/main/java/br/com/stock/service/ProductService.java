package br.com.stock.service;

import br.com.commons.exception.ConflictException;
import br.com.commons.exception.NotFoundException;
import br.com.stock.dao.ProductDao;
import br.com.stock.domain.Product;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class ProductService {

    static final Logger LOG = Logger.getLogger(ProductService.class);

    @Autowired
    private ProductDao productDao;

    @Autowired
    private CategoryService categoryService;

    public Product create(Product product) throws Exception {
        if (productDao.findByName(product.getStoreId(), product.getName()) != null) {
            throw new ConflictException();
        } else {
            product.setCategory(categoryService.findById(product.getCategory().getId()));

            return productDao.save(product);
        }
    }

    public Product update(Product product) throws NotFoundException {
        product.setCategory(categoryService.findById(product.getCategoryId()));
        return productDao.update(product);
    }

    public List<Product> findAll(Long storeId) {
        return productDao.findAll(storeId);
    }

    public List<Product> findByCategoryId(Long storeId, Long categoryId) {
        return productDao.findByCategoryId(storeId, categoryId);
    }

    public Product findById(Long productId) throws Exception {
        Product product = productDao.findById(productId);

        if (product == null) {
            throw new NotFoundException();
        }
        return product;
    }

    public Product findByName(Long storeId, String productName) throws Exception {
        Product product = productDao.findByName(storeId, productName);

        if (product == null) {
            throw new NotFoundException();
        }
        return product;
    }

    public void remove(Long productId) throws Exception {
        final Product product = productDao.findById(productId);
        if (product == null) {
            throw new NotFoundException();
        }

        productDao.delete(product);
    }

}
