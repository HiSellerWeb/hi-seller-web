package br.com.stock.dao;

import br.com.commons.dao.AbstractDao;
import br.com.stock.domain.Category;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

public class CategoryDao extends AbstractDao<Category> {

    @Override
    public Category save(Category category) {
        category.setId(null);
        return super.save(category);
    }

    @Override
    public Category update(Category category) {
        return super.update(category);
    }

    @Override
    public void delete(Category category) {
        super.delete(category);
    }

    public List<Category> findAll(Long storeId) {
        List<Criterion> restrictions = new ArrayList<>();
        restrictions.add(Restrictions.eq("storeId", storeId));
        return super.findByRestrictions(Category.class, restrictions, Order.asc("name"));
    }

    public Category findById(Long id) {
        return super.findById(Category.class, id);
    }

    public Category findByName(Long storeId, String category) {
        List<Criterion> restrictions = new ArrayList<>();
        restrictions.add(Restrictions.eq("storeId", storeId));
        restrictions.add(Restrictions.like("name", category).ignoreCase());
        List<Category> categories = super.findByRestrictions(Category.class, restrictions);

        if (categories != null && categories.size() >= 1) {
            return categories.get(0);
        }

        return null;
    }

}
