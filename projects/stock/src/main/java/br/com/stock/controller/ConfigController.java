package br.com.stock.controller;

import br.com.commons.appConfig.AppConfig;
import br.com.stock.config.ConfigEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class ConfigController {

    @Autowired
    private AppConfig appConfig;

    @RequestMapping(method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<String> updateProperties() throws Exception {
        appConfig.loadAppConfigProperties(0L, ConfigEnum.values());
        return new ResponseEntity<>(AppConfig.getPROPERTIES().toString(), HttpStatus.OK);
    }

}
