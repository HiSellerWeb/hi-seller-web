package br.com.stock.dao;

import br.com.commons.dao.AbstractDao;
import br.com.stock.domain.Product;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

public class ProductDao extends AbstractDao<Product> {

    @Override
    public Product save(Product product) {
        product.setId(null);
        return super.save(product);
    }

    @Override
    public Product update(Product product) {
        return super.update(product);
    }

    @Override
    public void delete(Product product) {
        super.delete(product);
    }

    public Product findById(Long id) {
        return super.findById(Product.class, id);
    }

    public List<Product> findByCategoryId(Long storeId, Long categoryId) {
        List<Criterion> restrictions = new ArrayList<>();
        restrictions.add(Restrictions.eq("storeId", storeId));
        restrictions.add(Restrictions.eq("category.id", categoryId));
        List<Product> products = super.findByRestrictions(Product.class, restrictions, Order.asc("name"));

        return products;
    }

    public Product findByName(Long storeId, String product) {
        List<Criterion> restrictions = new ArrayList<>();
        restrictions.add(Restrictions.eq("storeId", storeId));
        restrictions.add(Restrictions.like("name", product).ignoreCase());
        List<Product> categories = super.findByRestrictions(Product.class, restrictions);

        if (categories != null && categories.size() >= 1) {
            return categories.get(0);
        }

        return null;
    }

    public List<Product> findAll(Long storeId) {
        List<Criterion> restrictions = new ArrayList<>();
        restrictions.add(Restrictions.eq("storeId", storeId));
        return super.findByRestrictions(Product.class, restrictions, Order.asc("name"));
    }
}
