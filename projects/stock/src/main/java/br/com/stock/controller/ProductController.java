package br.com.stock.controller;

import br.com.commons.appConfig.AppConfig;
import br.com.commons.controller.AbstractController;
import br.com.commons.helper.DomainCopyProperties;
import br.com.commons.json.ProductJson;
import br.com.stock.config.ConfigEnum;
import br.com.stock.domain.Product;
import br.com.stock.service.ProductService;
import br.com.stock.validator.ProductValidator;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "/product")
public class ProductController extends AbstractController {

    @Autowired
    private ProductService productService;

    @Autowired
    private ProductValidator productValidator;

    @Autowired
    private AppConfig appConfig;

    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/{token}", method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity<ProductJson> create(@RequestHeader(value = "Authorization") String authorization, @PathVariable("token") String token, @RequestBody ProductJson productJson, HttpServletRequest request, HttpServletResponse response) throws Exception {
        Long storeId = validateAuthorization(authorization);
        validateManagerToken(appConfig.getValue(0L, ConfigEnum.ACCOUNT_ENDPOINT), storeId, token);
        productValidator.validate(productJson);

        Product product = new Product(productJson);
        product.setStoreId(storeId);

        product = productService.create(product);

        return new ResponseEntity<>(product.toJson(), HttpStatus.CREATED);
    }

    @SuppressWarnings({"unchecked"})
    @RequestMapping(value = "/{token}", method = RequestMethod.PUT)
    public @ResponseBody
    ResponseEntity<ProductJson> edit(@RequestHeader(value = "Authorization") String authorization, @PathVariable("token") String token, @RequestBody ProductJson productJson, HttpServletRequest request, HttpServletResponse response) throws Exception {
        Long storeId = validateAuthorization(authorization);
        validateManagerToken(appConfig.getValue(0L, ConfigEnum.ACCOUNT_ENDPOINT), storeId, token);
        productValidator.validate(productJson);

        Product productToUpdate = productService.findById(productJson.getId()); // busca o producto na base para se ter o objeto do hibernate
        DomainCopyProperties.copyPropertiesIgnored(new Product(productJson), productToUpdate);
        productToUpdate = productService.update(productToUpdate); // salva o objeto do hibernate normalmente.

        return new ResponseEntity<>(productToUpdate.toJson(), HttpStatus.OK);
    }

    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/{token}/{productId}", method = RequestMethod.DELETE)
    public @ResponseBody
    ResponseEntity<ProductJson> delete(@RequestHeader(value = "Authorization") String authorization, @PathVariable("token") String token, @PathVariable("productId") Long productId, HttpServletRequest request, HttpServletResponse response) throws Exception {
        Long storeId = validateAuthorization(authorization);
        validateManagerToken(appConfig.getValue(0L, ConfigEnum.ACCOUNT_ENDPOINT), storeId, token);

        productService.remove(productId);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/{productId}", method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<ProductJson> findById(@RequestHeader(value = "Authorization") String authorization, @PathVariable("productId") Long productId, HttpServletRequest request, HttpServletResponse response) throws Exception {
        final Product product = productService.findById(productId);

        return new ResponseEntity<>(product.toJson(), HttpStatus.OK);
    }

    @RequestMapping(value = "/name/{productName}", method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<ProductJson> findByName(@RequestHeader(value = "Authorization") String authorization, @PathVariable("productName") String productName, HttpServletRequest request, HttpServletResponse response) throws Exception {
        Long storeId = validateAuthorization(authorization);
        final Product product = productService.findByName(storeId, productName);

        return new ResponseEntity<>(product.toJson(), HttpStatus.OK);
    }

    @RequestMapping(value = "/category/{categoryId}", method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<List<ProductJson>> findByCategoryId(@RequestHeader(value = "Authorization") String authorization, @PathVariable("categoryId") Long categoryId, HttpServletRequest request, HttpServletResponse response) throws Exception {
        Long storeId = validateAuthorization(authorization);
        List<Product> products = productService.findByCategoryId(storeId, categoryId);

        List<ProductJson> productsJson = new ArrayList<>(products.size());

        for (Product product : products) {
            productsJson.add(product.toJson());
        }

        return new ResponseEntity<>(productsJson, HttpStatus.OK);
    }
}
