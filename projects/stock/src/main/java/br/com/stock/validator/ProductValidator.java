package br.com.stock.validator;

import br.com.commons.json.ProductJson;
import br.com.commons.validator.AbstractValidator;
import br.com.commons.validator.RestrictionType;

public class ProductValidator extends AbstractValidator<ProductJson> {

    @Override
    protected void apply(ProductJson productJson) {
        if (productJson == null) {
            setInvalid(true).setError("productJson", RestrictionType.REQUIRED, "Objeto nulo");
            return;
        }

        isNull(productJson.getName()).setError("productJson.name", RestrictionType.REQUIRED, "Obrigatório informar o nome do produto");
        if (productJson.getName() != null) {
            isBlank(productJson.getName()).setError("productJson.name", RestrictionType.REQUIRED, "Nome do produto não pode estar em branco");
            isLengthBetween(productJson.getName(), 3, 20).setError("productJson.name", RestrictionType.EXACT_LENGTH, "Nome do produto deve conter entre 3 e 20 caracteres");
        }

        if (productJson.getDescription() != null) {
            isLengthBetween(productJson.getDescription(), 0, 9000).setError("productJson.description", RestrictionType.EXACT_LENGTH, "A descrição do produto deve conter no máximo 9000 caracteres");
        }

        if (productJson.getShotDescription() != null) {
            isLengthBetween(productJson.getShotDescription(), 0, 255).setError("productJson.shotDescription", RestrictionType.EXACT_LENGTH, "A pequena descrição do produto deve conter no máximo 255 caracteres");
        }
    }
}
