package br.com.stock.service;

import br.com.commons.exception.ConflictException;
import br.com.commons.exception.NotFoundException;
import br.com.stock.dao.CategoryDao;
import br.com.stock.domain.Category;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class CategoryService {

    static final Logger LOG = Logger.getLogger(CategoryService.class);

    @Autowired
    private CategoryDao categoryDao;

    public Category create(Category category) throws Exception {
        if (categoryDao.findByName(category.getStoreId(), category.getName()) != null) {
            throw new ConflictException();
        } else {
            return categoryDao.save(category);
        }
    }

    public List<Category> findAll(Long storeId) {
        return categoryDao.findAll(storeId);
    }

    public void remove(Long categoryId) throws Exception {
        final Category category = categoryDao.findById(categoryId);
        if (category == null) {
            throw new NotFoundException();
        }

        categoryDao.delete(category);
    }

    public void update(Category category) throws NotFoundException {
        categoryDao.update(category);
    }

    public Category findById(Long categoryId) throws NotFoundException {
        Category category = categoryDao.findById(categoryId);

        if (category == null) {
            throw new NotFoundException();
        }
        return category;
    }

    public Category findByName(Long storeId, String categoryName) throws NotFoundException {
        Category category = categoryDao.findByName(storeId, categoryName);

        if (category == null) {
            throw new NotFoundException();
        }
        return category;
    }
}
