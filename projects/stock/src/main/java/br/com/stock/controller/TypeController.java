package br.com.stock.controller;

import br.com.commons.domain.TypeEnum;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "/type")
public class TypeController {

    @RequestMapping(method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<String> findAll(HttpServletRequest request, HttpServletResponse response) throws Exception {
        return new ResponseEntity<>(TypeEnum.allToJson(), HttpStatus.OK);
    }

}
