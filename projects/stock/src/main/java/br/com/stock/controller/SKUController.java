package br.com.stock.controller;

import br.com.commons.appConfig.AppConfig;
import br.com.commons.controller.AbstractController;
import br.com.commons.helper.DomainCopyProperties;
import br.com.commons.json.ProductMovJson;
import br.com.commons.json.SKUJson;
import br.com.stock.config.ConfigEnum;
import br.com.stock.domain.SKU;
import br.com.stock.service.SKUService;
import br.com.stock.validator.SKUValidator;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "/sku")
public class SKUController extends AbstractController {

    @Autowired
    private SKUService skuService;

    @Autowired
    private SKUValidator skuValidator;

    @Autowired
    private AppConfig appConfig;

    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/{token}", method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity<SKUJson> create(@RequestHeader(value = "Authorization") String authorization, @PathVariable("token") String token, @RequestBody SKUJson skuJson, HttpServletRequest request, HttpServletResponse response) throws Exception {
        Long storeId = validateAuthorization(authorization);
        validateManagerToken(appConfig.getValue(0L, ConfigEnum.ACCOUNT_ENDPOINT), storeId, token);
        skuValidator.validate(skuJson);
        SKU sku = new SKU(skuJson);
        sku.setStoreId(storeId);
        sku.setAvailable(sku.getStock());
        skuService.create(sku);

        return new ResponseEntity<>(sku.toJson(), HttpStatus.CREATED);
    }

    @SuppressWarnings({"unchecked"})
    @RequestMapping(value = "/{token}", method = RequestMethod.PUT)
    public @ResponseBody
    ResponseEntity<SKUJson> edit(@RequestHeader(value = "Authorization") String authorization, @PathVariable("token") String token, @RequestBody SKUJson skuJson, HttpServletRequest request, HttpServletResponse response) throws Exception {
        Long storeId = validateAuthorization(authorization);
        validateManagerToken(appConfig.getValue(0L, ConfigEnum.ACCOUNT_ENDPOINT), storeId, token);
        skuValidator.validate(skuJson);

        SKU skuToUpdate = skuService.findById(skuJson.getId());
        DomainCopyProperties.copyPropertiesIgnored(new SKU(skuJson), skuToUpdate);
        skuToUpdate = skuService.update(skuToUpdate);

        return new ResponseEntity<>(skuToUpdate.toJson(), HttpStatus.OK);
    }

    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/{token}/{skuId}", method = RequestMethod.DELETE)
    public @ResponseBody
    ResponseEntity<SKUJson> delete(@RequestHeader(value = "Authorization") String authorization, @PathVariable("token") String token, @PathVariable("skuId") Long skuId, HttpServletRequest request, HttpServletResponse response) throws Exception {
        Long storeId = validateAuthorization(authorization);
        validateManagerToken(appConfig.getValue(0L, ConfigEnum.ACCOUNT_ENDPOINT), storeId, token);

        final SKU skuToDelete = skuService.findById(skuId);
        if (skuService.findByProductId(skuId) != null) {
            skuService.remove(skuToDelete.getId());
        } else {
            skuService.remove(skuId);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/stockToSession", method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity<ProductMovJson> stockToSession(@RequestHeader(value = "Authorization") String authorization, @RequestBody ProductMovJson productMovJson, HttpServletRequest request, HttpServletResponse response) throws Exception {
//        TODO: Hebert - Ver uma forma de tornar o acesso a estes caras mais seguro
        Integer quant = skuService.stockToSession(productMovJson);

        productMovJson.setQuantity(quant);

        return new ResponseEntity<>(productMovJson, HttpStatus.ACCEPTED);
    }

    @RequestMapping(value = "/sessionToOrder", method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity<ProductMovJson> sessionToOrder(@RequestHeader(value = "Authorization") String authorization, @RequestBody ProductMovJson productMovJson, HttpServletRequest request, HttpServletResponse response) throws Exception {
        Integer quant = skuService.sessionToOrder(productMovJson);

        productMovJson.setQuantity(quant);

        return new ResponseEntity<>(productMovJson, HttpStatus.ACCEPTED);
    }

    @RequestMapping(value = "/orderToSold", method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity<ProductMovJson> orderToSold(@RequestHeader(value = "Authorization") String authorization, @RequestBody ProductMovJson productMovJson, HttpServletRequest request, HttpServletResponse response) throws Exception {
        Integer quant = skuService.orderToSold(productMovJson);

        productMovJson.setQuantity(quant);

        return new ResponseEntity<>(productMovJson, HttpStatus.ACCEPTED);
    }

    @RequestMapping(value = "/{skuId}", method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<SKUJson> findById(@RequestHeader(value = "Authorization") String authorization, @PathVariable("skuId") Long skuId, HttpServletRequest request, HttpServletResponse response) throws Exception {
        final SKU sku = skuService.findById(skuId);

        return new ResponseEntity<>(sku.toJson(), HttpStatus.OK);
    }

    @RequestMapping(value = "/product/{productId}", method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<List<SKUJson>> findByProductId(@RequestHeader(value = "Authorization") String authorization, @PathVariable("productId") Long productId, HttpServletRequest request, HttpServletResponse response) throws Exception {
        List<SKU> skus = skuService.findByProductId(productId);
        List<SKUJson> skusJson = new ArrayList<SKUJson>(skus.size());
        for (SKU sku : skus) {
            skusJson.add(sku.toJson());
        }
        return new ResponseEntity<>(skusJson, HttpStatus.OK);
    }
}
