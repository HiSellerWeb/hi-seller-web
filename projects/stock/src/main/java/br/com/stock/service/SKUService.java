package br.com.stock.service;

import br.com.commons.exception.ConflictException;
import br.com.commons.exception.NotFoundException;
import br.com.commons.json.ProductMovJson;
import br.com.stock.dao.SKUDao;
import br.com.stock.domain.SKU;
import java.util.List;
import java.util.logging.Level;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class SKUService {

    static final Logger LOG = Logger.getLogger(SKUService.class);

    @Autowired
    private ProductService productService;

    @Autowired
    private SKUDao skuDao;

    public SKU create(SKU sku) throws Exception {
        if (skuDao.findByCode(sku.getStoreId(), sku.getCode()) != null) {
            throw new ConflictException();
        } else {
            sku.setProduct(productService.findById(sku.getProduct().getId()));

            return skuDao.save(sku);
        }
    }

    public SKU update(SKU sku) throws NotFoundException, Exception {
        return skuDao.update(sku);
    }

    public List<SKU> findByProductId(Long product) {
        return skuDao.findByProductId(product);
    }

    public SKU findById(Long skuId) throws Exception {
        SKU sku = skuDao.findById(skuId);

        if (sku == null) {
            throw new NotFoundException();
        }
        return sku;
    }

    public Integer stockToSession(ProductMovJson productMovJson) {
        try {
            Integer quantity = 0;
            SKU sku = this.findById(productMovJson.getId());
            if (sku != null) {
                if (sku.getAvailable() >= productMovJson.getQuantity()) {
                    quantity = productMovJson.getQuantity();
                } else {
                    quantity = sku.getAvailable();
                }

                sku.setAvailable(sku.getAvailable() - quantity);
                sku.setSessionReserve(sku.getSessionReserve() + quantity);
                skuDao.update(sku);
            }

            return quantity;

        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(SKUService.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
    }

    public Integer sessionToOrder(ProductMovJson productMovJson) {
        try {
            Integer quantity = productMovJson.getQuantity();
            SKU sku = this.findById(productMovJson.getId());
            if (sku != null) {
                sku.setSessionReserve(sku.getSessionReserve() - quantity);
                sku.setOrderReserve(sku.getOrderReserve() + quantity);
                skuDao.update(sku);
            }

            return quantity;

        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(SKUService.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
    }

    public Integer orderToSold(ProductMovJson productMovJson) {
        try {
            Integer quantity = productMovJson.getQuantity();
            SKU sku = this.findById(productMovJson.getId());
            if (sku != null) {
                sku.setOrderReserve(sku.getOrderReserve() - quantity);
                sku.setSold(sku.getSold() + quantity);
                skuDao.update(sku);
            }
            return quantity;

        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(SKUService.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
    }

    public void remove(Long skuId) throws Exception {
        final SKU product = skuDao.findById(skuId);
        if (product == null) {
            throw new NotFoundException();
        }

        skuDao.delete(product);
    }

    public void verifyStockSKUs() {
        //Criar o array list
        List<SKU> skus = skuDao.findAll();
        if (skus != null) {
            for (SKU sku : skus) {
                Integer stock = sku.getStock();
                Integer sum = sku.getAvailable() + sku.getOrderReserve() + sku.getSessionReserve();
                if (!stock.equals(sum)) {
                    LOG.error("SKU: " + sku.getId() + " está com o estoque incorreto!");
                }
            }
        }
    }

}
