package br.com.stock.domain;

import br.com.commons.json.ProductJson;
import br.com.commons.json.SKUJson;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapKeyColumn;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Table(name = "sku")
@Cache(usage = CacheConcurrencyStrategy.NONE)
public class SKU implements Serializable {

    private static final long serialVersionUID = 8642828016091607932L;

    @Id
    @SequenceGenerator(name = "sku_id", sequenceName = "sku_id_seg")
    @GeneratedValue(generator = "sku_id", strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "storeId", nullable = false, updatable = false)
    private Long storeId;

    @ManyToOne(cascade = CascadeType.REMOVE)
    @JoinColumn(name = "product", nullable = false)
    private Product product;

    @ElementCollection
    @Column(name = "image", nullable = false)
    private List<String> images = new ArrayList<>();

    @Column(name = "price", precision = 7, scale = 2, nullable = false)
    private BigDecimal price = BigDecimal.ZERO;

    @Column(name = "code", length = 20)
    private String code;

    @Column(name = "weight", columnDefinition = "int default 0", nullable = false)
    private Long weight = 0L;

    @Column(name = "available", columnDefinition = "int default 0", nullable = false)
    private Integer available = 0;

    @Column(name = "session_reserve", columnDefinition = "int default 0", nullable = false)
    private Integer sessionReserve = 0;

    @Column(name = "order_reserve", columnDefinition = "int default 0", nullable = false)
    private Integer orderReserve = 0;

    @Column(name = "sold", columnDefinition = "int default 0", nullable = false)
    private Integer sold = 0;

    @Column(name = "stock", columnDefinition = "int default 0", nullable = false)
    private Integer stock = 0;

    @Column(name = "name", length = 20)
    private String name = "";

    @ElementCollection
    @MapKeyColumn(name = "rowKey")
    @Column(name = "row")
    private Map<String, String> rows;

    public SKU() {
    }

    public SKU(SKUJson json) {
        if (json == null) {
            return;
        }

        this.id = json.getId();
        this.storeId = json.getStoreId();
        this.images = json.getImages();
        this.price = json.getPrice();
        this.code = json.getCode();
        this.weight = json.getWeight();
        this.available = json.getAvailable();
        this.sessionReserve = json.getSessionReserve();
        this.orderReserve = json.getOrderReserve();
        this.sold = json.getSold();
        this.stock = json.getStock();
        this.rows = json.getRows();
        this.name = json.getName();

        if (json.getProduct() != null) {
            this.product = new Product(json.getProduct());
        }
    }

    public SKUJson toJson() {
        SKUJson json = new SKUJson();

        json.setId(id);
        json.setStoreId(storeId);
        json.setImages(images);
        json.setPrice(price);
        json.setCode(code);
        json.setWeight(weight);
        json.setAvailable(available);
        json.setSessionReserve(sessionReserve);
        json.setOrderReserve(orderReserve);
        json.setSold(sold);
        json.setStock(stock);
        json.setRows(rows);
        json.setName(name);

        if (product != null) {
            ProductJson productJson = new ProductJson(product.getId());
            productJson.setName(product.getName());
            json.setProduct(productJson);
        }

        return json;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getWeight() {
        return weight;
    }

    public void setWeight(Long weight) {
        this.weight = weight;
    }

    public Integer getAvailable() {
        return available;
    }

    public void setAvailable(Integer available) {
        this.available = available;
    }

    public Integer getSold() {
        return sold;
    }

    public void setSold(Integer sold) {
        this.sold = sold;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public Map<String, String> getRows() {
        return rows;
    }

    public void setRows(Map<String, String> rows) {
        this.rows = rows;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public Integer getSessionReserve() {
        return sessionReserve;
    }

    public void setSessionReserve(Integer sessionReserve) {
        this.sessionReserve = sessionReserve;
    }

    public Integer getOrderReserve() {
        return orderReserve;
    }

    public void setOrderReserve(Integer orderReserve) {
        this.orderReserve = orderReserve;
    }

    public Long getStoreId() {
        return storeId;
    }

    public void setStoreId(Long storeId) {
        this.storeId = storeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
