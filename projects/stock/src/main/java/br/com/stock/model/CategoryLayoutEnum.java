package br.com.stock.model;

import org.springframework.util.StringUtils;

public enum CategoryLayoutEnum {

    GENERAL("GENERAL"),
    PERFUME("PERFUME");

    private String name;

    CategoryLayoutEnum(String name) {
        this.name = name;
    }
    
    public static CategoryLayoutEnum getByString(String category) {
        if (StringUtils.isEmpty(category)) {
            return null;
        }

        for (CategoryLayoutEnum categoryEnum : CategoryLayoutEnum.values()) {
            if (category.equals(categoryEnum.getName())) {
                return categoryEnum;
            }
        }
        return null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
