package br.com.stock.controller;

import br.com.commons.appConfig.AppConfig;
import br.com.commons.controller.AbstractController;
import br.com.commons.helper.DomainCopyProperties;
import br.com.commons.helper.StringUtils;
import br.com.commons.json.CategoryJson;
import br.com.commons.json.CategoryListJson;
import br.com.stock.config.ConfigEnum;
import br.com.stock.domain.Category;
import br.com.stock.service.CategoryService;
import br.com.stock.validator.CategoryValidator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "/category")
public class CategoryController extends AbstractController {

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private CategoryValidator categoryValidator;

    @Autowired
    private AppConfig appConfig;

    @SuppressWarnings("unchecked")
    @RequestMapping(method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity<CategoryJson> create(@RequestHeader(value = "Authorization") String authorization, @PathVariable("token") String token, @RequestBody CategoryJson categoryJson, HttpServletRequest request, HttpServletResponse response) throws Exception {
        Long storeId = validateAuthorization(authorization);
        if (categoryJson != null) {
            categoryJson.setStoreId(storeId);
        }
        categoryValidator.validate(categoryJson);

        validateManagerToken(appConfig.getValue(0L, ConfigEnum.ACCOUNT_ENDPOINT), storeId, token);

        Category category = new Category(categoryJson);
        String urlName = category.getName().replaceAll(" ", "-");
        category.setUrlName(urlName.toLowerCase());
        category = categoryService.create(category);

        return new ResponseEntity<>(category.toJson(), HttpStatus.CREATED);
    }

    @RequestMapping(method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<CategoryListJson> findAll(@RequestHeader(value = "Authorization") String authorization, HttpServletRequest request, HttpServletResponse response) throws Exception {
        Long storeId = validateAuthorization(authorization);
        final List<Category> categories = categoryService.findAll(storeId);
        final CategoryListJson categoryListJson = new CategoryListJson();

        if (categories != null && !categories.isEmpty()) {
            for (Category category : categories) {
                categoryListJson.add(category.toJson());
            }
        }

        return new ResponseEntity<>(categoryListJson, HttpStatus.OK);
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<CategoryJson> find(@RequestParam(value = "categoryName", required = false) String categoryName, @RequestHeader(value = "Authorization") String authorization, HttpServletRequest request, HttpServletResponse response) throws Exception {
        final Long storeId = validateAuthorization(authorization);
        Category category = new Category();

        if (!StringUtils.isBlank(categoryName)) {
            category = categoryService.findByName(storeId, categoryName);
        }

        return new ResponseEntity<>(category.toJson(), HttpStatus.OK);
    }

    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/{token}/{categoryId}", method = RequestMethod.DELETE)
    public @ResponseBody
    ResponseEntity<CategoryJson> delete(@RequestHeader(value = "Authorization") String authorization, @PathVariable("token") String token, @PathVariable("categoryId") Long categoryId, HttpServletRequest request, HttpServletResponse response) throws Exception {
        Long storeId = validateAuthorization(authorization);
        validateManagerToken(appConfig.getValue(0L, ConfigEnum.ACCOUNT_ENDPOINT), storeId, token);
        categoryService.remove(categoryId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @SuppressWarnings({"unchecked"})
    @RequestMapping(value = "/{token}", method = RequestMethod.PUT)
    public @ResponseBody
    ResponseEntity<CategoryJson> edit(@RequestHeader(value = "Authorization") String authorization, @PathVariable("token") String token, @RequestBody CategoryJson categoryJson, HttpServletRequest request, HttpServletResponse response) throws Exception {
        Long storeId = validateAuthorization(authorization);
        validateManagerToken(appConfig.getValue(0L, ConfigEnum.ACCOUNT_ENDPOINT), storeId, token);
        categoryValidator.validate(categoryJson);
        final Category categoryToUpdate = categoryService.findById(categoryJson.getId());
        DomainCopyProperties.copyPropertiesIgnored(new Category(categoryJson), categoryToUpdate);

        categoryService.update(categoryToUpdate);

        return new ResponseEntity<>(categoryToUpdate.toJson(), HttpStatus.OK);
    }
}
