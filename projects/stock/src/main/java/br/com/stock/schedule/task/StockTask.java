package br.com.stock.schedule.task;

import br.com.stock.service.SKUService;
import java.util.TimerTask;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class StockTask extends TimerTask {

    private static final Log LOG = LogFactory.getLog(StockTask.class);

    private final SKUService skuService;

    public StockTask(SKUService skuService) {
        this.skuService = skuService;
    }

    @Override
    public void run() {
        try {
            skuService.verifyStockSKUs();
        } catch (Exception e) {
            LOG.error("Ocorreu um erro ao verificar o estoque dos skus: ", e);
        }
    }
}
