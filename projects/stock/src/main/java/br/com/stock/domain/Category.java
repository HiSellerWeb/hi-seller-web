package br.com.stock.domain;

import br.com.commons.domain.TypeEnum;
import br.com.commons.json.CategoryJson;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Table(name = "category")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Category implements Serializable {

    private static final long serialVersionUID = 8746387L;

    @Id
    @SequenceGenerator(name = "category_id", sequenceName = "category_id_seg")
    @GeneratedValue(generator = "category_id", strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "storeId", nullable = false, updatable = false)
    private Long storeId;

    @Column(name = "name", nullable = false, unique = true, length = 255)
    private String name;

    @Column(name = "urlName", nullable = false)
    private String urlName;

    @Column(name = "image", nullable = false)
    private String image;

    @Column(name = "description", nullable = false, columnDefinition = "VARCHAR(5000)")
    private String description;

    @Column(name = "productType", columnDefinition = "varchar(15)", nullable = false)
    @Enumerated(EnumType.STRING)
    private TypeEnum type;

    @Column(name = "layoutFile", nullable = false)
    private String layoutFile;

    @OneToMany(mappedBy = "category", cascade = CascadeType.REFRESH)
    private List<Product> products = new ArrayList<>();

    public Category() {
    }

    public Category(CategoryJson categoryJson) {
        if (categoryJson != null) {
            this.id = categoryJson.getId();
            this.storeId = categoryJson.getStoreId();
            this.name = categoryJson.getName();
            this.description = categoryJson.getDescription();
            this.image = categoryJson.getImage();
            this.urlName = categoryJson.getUrlName();
            this.type = TypeEnum.valueOf(categoryJson.getType());
            this.layoutFile = categoryJson.getLayoutFile();
        }
    }

    public CategoryJson toJson() {
        CategoryJson json = new CategoryJson();
        json.setId(getId());
        json.setName(getName());
        json.setDescription(getDescription());
        json.setImage(getImage());
        json.setUrlName(getUrlName());
        json.setType(getType().toString());
        json.setLayoutFile(getLayoutFile());

        return json;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrlName() {
        return urlName;
    }

    public void setUrlName(String urlName) {
        this.urlName = urlName;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public Long getStoreId() {
        return storeId;
    }

    public void setStoreId(Long storeId) {
        this.storeId = storeId;
    }

    public TypeEnum getType() {
        return type;
    }

    public void setType(TypeEnum type) {
        this.type = type;
    }

    public String getLayoutFile() {
        return layoutFile;
    }

    public void setLayoutFile(String layoutFile) {
        this.layoutFile = layoutFile;
    }
}
