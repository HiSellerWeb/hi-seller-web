package br.com.stock.controller;

import br.com.commons.domain.TypeEnum;
import br.com.stock.dao.CategoryDao;
import br.com.stock.domain.Category;
import br.com.stock.domain.Product;
import br.com.stock.domain.SKU;
import br.com.stock.service.CategoryService;
import br.com.stock.service.ProductService;
import br.com.stock.service.SKUService;
import com.google.common.collect.ImmutableMap;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "/init")
public class InitController {

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private ProductService productService;

    @Autowired
    CategoryDao categoryDao;

    @Autowired
    private SKUService skuService;

    @SuppressWarnings("unchecked")
    @RequestMapping(method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<List<Serializable>> init(HttpServletRequest request, HttpServletResponse response) throws Exception {

        Category category1 = new Category();
        category1.setStoreId(0L);
        category1.setName("Perfume");
        category1.setUrlName("perfume");
        category1.setType(TypeEnum.PERFUME);
        category1.setLayoutFile("perfume_layout1");
        category1.setDescription("Descrição de teste para a categoria de perfumes.");
        category1.setImage("http://1.bp.blogspot.com/_6sQmhvKIghQ/RxljuQ_w7GI/AAAAAAAAAfg/-Bhzp-wzkM8/s400/flores-magnolia_1082_1.jpg");
        category1 = categoryService.create(category1);

        Product product1 = new Product();
        product1.setStoreId(0L);
        product1.setCategory(category1);
        product1.setName("Azarro");
        product1.setDescription("Descrição completa do perfume azarro");
        product1.setShotDescription("Descrição curta do perfume");

        SKU sku1 = new SKU();
        sku1.setStoreId(0L);
        sku1.setCode("M01");
        sku1.setPrice(BigDecimal.valueOf(35.50));
        sku1.setImages(Arrays.asList("http://d2fvaoynuecth8.cloudfront.net/assets/39518/produtos/72903/hidratante-dovesensitive.jpg", "http://boticario.vteximg.com.br/arquivos/ids/161566-1000-1000/Floratta-in-Blue-Creme-Hidratante-200ml.jpg"));
        sku1.setStock(1000);
        sku1.setWeight(10L);
        sku1.setAvailable(1000);
        sku1.setProduct(product1);
        sku1.setRows(ImmutableMap.<String, String>builder()
                .put("gender", "M")
                .put("inspiration", "Azarro")
                .put("type", "Amadeirado")
                .put("intensity", "Forte")
                .put("size", "35")
                .build());

        productService.create(product1);
        skuService.create(sku1);

        Category category2 = new Category();
        category2.setStoreId(0L);
        category2.setName("hidratante");
        category2.setUrlName("hidratante");
        category2.setType(TypeEnum.DEFAULT);
        category2.setLayoutFile("padrao_layout1");
        category2.setDescription("Descrição de teste para a categoria de Hidratante.");
        category2.setImage("http://1.bp.blogspot.com/_6sQmhvKIghQ/RxljuQ_w7GI/AAAAAAAAAfg/-Bhzp-wzkM8/s400/flores-magnolia_1082_1.jpg");

        category2 = categoryService.create(category2);

        Product product2 = new Product();
        product2.setStoreId(0L);
        product2.setCategory(category2);
        product2.setName("Hidratante Ferrari Black");
        product2.setDescription("Descrição completa do perfume Ferrari Black");
        product2.setShotDescription("Descrição curta do perfume");

//        product2.setCode("M02");
//        product2.setPrice(BigDecimal.valueOf(37.50));
//        product2.setAvailable(100);
//        product2.setWeight(100L);
//        product2.setImageUrl("http://boticario.vteximg.com.br/arquivos/ids/161566-1000-1000/Floratta-in-Blue-Creme-Hidratante-200ml.jpg");
//        productService.create(product2);
//        Product perfumeProduct = new Product();
//        perfumeProduct.setCategory(category1);
//        perfumeProduct.setCode("P01");
//        perfumeProduct.setName("Perfume Ferrari Black");
//        perfumeProduct.setDescription("Descrição completa do perfume Ferrari Black");
//        perfumeProduct.setPrice(BigDecimal.valueOf(17.50));
//        perfumeProduct.setAvailable(100);
//        perfumeProduct.setWeight(50L);
//        perfumeProduct.setImageUrl("http://boticario.vteximg.com.br/arquivos/ids/161566-1000-1000/Floratta-in-Blue-Creme-Hidratante-200ml.jpg");
//        perfumeProduct.setShotDescription("Descrição curta do perfume");
//
//        perfumeProduct = productService.create(perfumeProduct);
//        Perfume perfume = new Perfume();
//        perfume.setProduct(perfumeProduct);
//        perfume.setInspiration("Ferrari Black");
//        perfume.setIntensity(IntensityEnum.SOFT);
//        perfume.setRecomendedGender(RecomendedGenderEnum.MALE);
//        perfume.setSize(BigDecimal.valueOf(50.0));
//        perfume.setType(type);
//
//        perfumeService.create(perfume);
        return new ResponseEntity<>(null, HttpStatus.CREATED);
    }
}
