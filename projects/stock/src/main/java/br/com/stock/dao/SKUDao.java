package br.com.stock.dao;

import br.com.commons.dao.AbstractDao;
import br.com.stock.domain.SKU;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

public class SKUDao extends AbstractDao<SKU> {

    @Override
    public SKU save(SKU sku) {
        sku.setId(null);
        return super.save(sku);
    }

    @Override
    public SKU update(SKU sku) {
        return super.merge(sku);
    }

    @Override
    public void delete(SKU product) {
        super.delete(product);
    }

    public SKU findById(Long id) {
        return super.findById(SKU.class, id);
    }

    public List<SKU> findByProductId(Long productId) {
        List<Criterion> restrictions = new ArrayList<>();
        restrictions.add(Restrictions.eq("product.id", productId));
        List<SKU> skus = super.findByRestrictions(SKU.class, restrictions, Order.asc("id"));

        return skus;
    }

    public SKU findByCode(Long storeId, String code) {
        List<Criterion> restrictions = new ArrayList<>();
        restrictions.add(Restrictions.eq("storeId", storeId));
        restrictions.add(Restrictions.eq("code", code));
        List<SKU> skus = super.findByRestrictions(SKU.class, restrictions, Order.asc("id"));

        if (skus != null && skus.size() >= 1) {
            return skus.get(0);
        }

        return null;
    }

    public List<SKU> findAll(Long storeId) {
        List<Criterion> restrictions = new ArrayList<>();
        restrictions.add(Restrictions.eq("storeId", storeId));
        return super.findByRestrictions(SKU.class, restrictions, Order.asc("product"));
    }

    /**
     * Usar com cautela, este método retorna todos os skus da base, independente
     * do storeId
     */
    public List<SKU> findAll() {
        List<Criterion> restrictions = new ArrayList<>();
        return super.findByRestrictions(SKU.class, restrictions, Order.asc("product"));
    }
}
