package br.com.stock.model;

import org.springframework.util.StringUtils;

public enum IntensityEnum {

    SOFT("Suave"),
    MODERATE("Moderado"),
    INTENSE("Intenso");

    private String intensity;

    IntensityEnum(String intensity) {
        this.setIntensity(intensity);
    }

    public static IntensityEnum getByString(String intensity) {
        if (StringUtils.isEmpty(intensity)) {
            return null;
        }

        for (IntensityEnum intensityEnum : IntensityEnum.values()) {
            if (intensity.equals(intensityEnum.getIntensity())) {
                return intensityEnum;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return this.name();
    }

    public String getIntensity() {
        return intensity;
    }

    public void setIntensity(String intensity) {
        this.intensity = intensity;
    }

}
