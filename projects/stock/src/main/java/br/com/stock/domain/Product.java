package br.com.stock.domain;

import br.com.commons.json.ProductJson;
import br.com.commons.json.SKUJson;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
@Table(name = "product")
@Cache(usage = CacheConcurrencyStrategy.NONE)
public class Product implements Serializable {

    private static final long serialVersionUID = 13472364236419L;

    @Id
    @SequenceGenerator(name = "product_id", sequenceName = "product_id_seg")
    @GeneratedValue(generator = "product_id", strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "storeId", nullable = false, updatable = false)
    private Long storeId;

    @Column(name = "name", nullable = false)
    private String name;

    @ManyToOne(cascade = CascadeType.REFRESH)
    @JoinColumn(name = "category", nullable = false)
    private Category category;

    @Column(name = "description", length = 9000)
    private String description;

    @Column(name = "shot_description", length = 255)
    private String shotDescription;

    @OneToMany(mappedBy = "product", cascade = CascadeType.REMOVE)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<SKU> skus = new ArrayList<>();

    public Product() {

    }

    public Product(ProductJson json) {
        if (json == null) {
            return;
        }

        this.id = json.getId();
        this.storeId = json.getStoreId();
        this.name = json.getName();
        this.category = new Category(json.getCategory());
        this.description = json.getDescription();
        this.shotDescription = json.getShotDescription();

        if (json.getSkus() != null) {
            for (SKUJson skuJson : json.getSkus()) {
                addSKU(new SKU(skuJson));
            }
        }
    }

    public ProductJson toJson() {
        ProductJson json = new ProductJson();

        json.setId(getId());
        json.setStoreId(getStoreId());
        json.setName(getName());
        json.setDescription(getDescription());
        json.setShotDescription(getShotDescription());

        if (getCategory() != null) {
            json.setCategory(getCategory().toJson());
        }

        if (getSkus() != null && getSkus().size() > 0) {
            List<SKUJson> skusJson = new ArrayList<>(getSkus().size());
            for (SKU sku : getSkus()) {
                skusJson.add(sku.toJson());
            }
            json.setSkus(skusJson);
        }

        return json;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Category getCategory() {
        return category;
    }

    public Long getCategoryId() {
        return category.getId();
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getShotDescription() {
        return shotDescription;
    }

    public void setShotDescription(String shotDescription) {
        this.shotDescription = shotDescription;
    }

    public List<SKU> getSkus() {
        return skus;
    }

    public void setSkus(List<SKU> skus) {
        this.skus = skus;
    }

    public void addSKU(SKU sku) {
        if (this.skus == null) {
            this.skus = new ArrayList<>();
        }
        this.skus.add(sku);
    }

    public Long getStoreId() {
        return storeId;
    }

    public void setStoreId(Long storeId) {
        this.storeId = storeId;
    }
}
