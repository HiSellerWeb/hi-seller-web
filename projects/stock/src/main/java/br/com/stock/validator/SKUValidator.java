package br.com.stock.validator;

import br.com.commons.json.ProductTypeJson;
import br.com.commons.json.SKUJson;
import br.com.commons.validator.AbstractValidator;
import br.com.commons.validator.RestrictionType;
import br.com.stock.domain.Product;
import br.com.stock.service.CategoryService;
import br.com.stock.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;

public class SKUValidator extends AbstractValidator<SKUJson> {

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private ProductService productService;

    @Override
    protected void apply(SKUJson skuJson) {
        if (skuJson == null) {
            setInvalid(true).setError("skuJson", RestrictionType.REQUIRED, "Objeto nulo.");
            return;
        }

        if (skuJson.getProduct() == null) {
            setInvalid(true).setError("skuJson.product", RestrictionType.REQUIRED, "Produto não informado.");
            return;
        }

        isBlank(skuJson.getProduct().getId()).setError("skuJson.product", RestrictionType.REQUIRED, "O produto informado não está cadastrado no sistema.");
        isBlank(skuJson.getPrice()).setError("skuJson.price", RestrictionType.REQUIRED, "Obrigatório informar o campo preço.");
        isBlank(skuJson.getStock()).setError("skuJson.stock", RestrictionType.REQUIRED, "Obrigatório informar o campo estoque.");
        isBlank(skuJson.getWeight()).setError("skuJson.weight", RestrictionType.REQUIRED, "Obrigatório informar o campo peso.");

        Product product = null;
        try {
            product = productService.findById(skuJson.getProduct().getId());
        } catch (Exception ex) {

        }

        if (product == null) {
            setInvalid(true).setError("skuJson.product", RestrictionType.REQUIRED, "Produto inválido.");
            return;
        }

        if (product.getCategory().getType() != null) {
            for (ProductTypeJson field : product.getCategory().getType().getFields()) {

                String rowValue = skuJson.getRows().get(field.getFieldName());
                String rowFieldName = "productJson." + field.getFieldName();

                if (field.isRequest()) {
                    isNull(rowValue).setError("skuJson" + rowFieldName, RestrictionType.REQUIRED, "Obrigatório preencher o campo " + field.getFrontEndName());
                    if (rowValue != null) {
                        isBlank(rowValue).setError("skuJson" + rowFieldName, RestrictionType.REQUIRED, "Obrigatório preencher o campo " + field.getFrontEndName());
                    }
                }

                isLengthBetween(rowValue, field.getMin(), field.getMax()).setError("skuJson" + rowFieldName, RestrictionType.EXACT_LENGTH, "O campo " + field.getFrontEndName() + " deve conter entre " + field.getMin() + " e " + field.getMax() + " caracteres");
            }
        }
    }
}
