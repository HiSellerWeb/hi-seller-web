package br.com.stock.tests.service;

import br.com.commons.exception.ConflictException;
import br.com.commons.exception.NotFoundException;
import br.com.stock.AbstractTest;
import br.com.stock.dao.CategoryDao;
import br.com.stock.domain.Category;
import br.com.stock.service.CategoryService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class CategoryServiceTest extends AbstractTest {

    private Category category;
    private Category category2;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private CategoryDao categoryDao;

    @Before
    public void setUp() {
        categoryDao.createQuery("delete category").executeUpdate();

        category = new Category();
        category.setName("Category Test");
        category.setDescription("Description category teste");
        category.setImage("C:\\teste.jpg");
        category.setUrlName("Teste");

        category2 = new Category();
        category2.setName("Category Test 2");
        category2.setDescription("Description category2 teste");
        category2.setImage("C:\\teste2.jpg");
        category2.setUrlName("Teste");
    }

    @Test
    public void create() {
        try {
            Category createdCategory = categoryService.create(category);
            Category categoryFound = categoryDao.findById(createdCategory.getId());

            Assert.assertEquals(category.getName(), categoryFound.getName());
        } catch (Exception ex) {
            Assert.fail("Não deveria lançar exception");
        }
    }

    @Test
    public void categoryAlreadyExists() throws Exception {
        try {
            categoryDao.save(category);

            categoryService.create(category);
            Assert.fail("Deveria lançar ConflictException");
        } catch (ConflictException ex) {
        }
    }

    @Test
    public void categoryRemove() throws Exception {
        Category savedCategory = categoryDao.save(category);

        categoryService.remove(savedCategory.getId());

        Assert.assertNull(categoryDao.findById(savedCategory.getId()));

    }

    @Test
    public void categoryNotFoundToRemove() throws Exception {
        try {
            categoryService.remove(0L);
            Assert.fail("Deveria lançar NotFoundException");
        } catch (NotFoundException e) {
        }
    }

    @Test
    public void findAll() throws Exception {
        categoryDao.save(category);
        categoryDao.save(category2);

//        List<Category> categories = categoryService.findAll();
//        Assert.assertEquals(2, categories.size());
//        Assert.assertEquals("Category Test", categories.get(0).getName());
//        Assert.assertEquals("Category Test 2", categories.get(1).getName());
    }

    @Test
    public void findById() throws Exception {
        Category savedCategory = categoryDao.save(category);

        Category categoryFound = categoryService.findById(savedCategory.getId());

        Assert.assertEquals(category.getName(), categoryFound.getName());
    }

    @Test
    public void findByIdNotFound() throws Exception {
        try {
            categoryService.findById(10L);
            Assert.fail("Deveria lançar NotFoundException");
        } catch (NotFoundException e) {
        }
    }

}
