package br.com.account.helper;

import br.com.account.domain.Address;
import br.com.account.domain.Customer;
import br.com.account.domain.Seller;

public class Objects {

    public static Customer getDefaltCustomer() {
        Customer customer = new Customer();
        customer.setCpf("08151957611");
        customer.setEmail("cliente@teste.com");
        customer.setName("Cliente Teste");
        customer.setNickName("C Teste");
        customer.setPassword("12341234");
        customer.setStoreId(0L);

        return customer;
    }

    public static Seller getDefaltSeller() {
        Seller seller = new Seller();
        seller.setCpf("08151957611");
        seller.setEmail("vendedor@teste.com");
        seller.setName("Vendedor Teste");
        seller.setNickName("V Teste");
        seller.setPassword("12341234");
        seller.setCode("123");
        seller.setStoreId(0L);

        return seller;
    }

    public static Address getDefaultAddress() {
        Address address = new Address();
        address.setActive(Boolean.TRUE);
        address.setZipCode("89012510");
        address.setUf("SC");
        address.setCity("Blumenau");
        address.setNeighborhood("Victor Konder");
        address.setStreet("Rua Max Hering");
        address.setNumber("19");
        address.setComplement("Apto 401");
        address.setRecipient("Cliente");
        address.setStoreId(0L);

        return address;
    }
}
