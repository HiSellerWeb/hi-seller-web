package br.com.account.service;

import br.com.account.AbstractTest;
import br.com.account.helper.Objects;
import br.com.commons.exception.ForbidenException;
import br.com.commons.json.AuthenticateJson;
import br.com.commons.security.Authentication;
import br.com.commons.security.AuthenticationRole;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class AuthenticateServiceTest extends AbstractTest {

    @Autowired
    private AuthenticateService categoryService;

    private AuthenticateJson authenticateJson;

    @Before
    @Override
    public void setUp() {
        super.setUp();

        sellerDao.save(Objects.getDefaltSeller());

        authenticateJson = new AuthenticateJson();
        authenticateJson.setEmail("vendedor@teste.com");
        authenticateJson.setPassword("12341234");
    }

    @Test
    public void authenticateSeller() {
        try {
            String authenticate = categoryService.authenticate(0L, authenticateJson, AuthenticationRole.SELLER);
            Authentication authentication = Authentication.fromString(authenticate);
            Assert.assertEquals(AuthenticationRole.SELLER, authentication.getAuthenticationRole());
        } catch (Exception ex) {
            Assert.fail("Não deveria lançar exception");
        }
    }

    @Test
    public void failToAuthenticateSeller() throws Exception {
        try {
            authenticateJson.setPassword("123123");
            String authenticate = categoryService.authenticate(0L, authenticateJson, AuthenticationRole.SELLER);
            Authentication.fromString(authenticate);
            Assert.fail("Deveria lançar exception");
        } catch (ForbidenException ex) {
            Assert.assertTrue("Usuário/senha inválidos.".equals(ex.getMessage()));
        }
    }
}
