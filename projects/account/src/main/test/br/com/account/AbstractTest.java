package br.com.account;

import br.com.account.dao.SellerDao;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@ContextConfiguration({"../../../resources/spring/testDataSource.xml", "../../../resources/spring/testApplicationContext.xml"})
@Transactional(propagation = Propagation.REQUIRED)
public class AbstractTest extends AbstractTransactionalJUnit4SpringContextTests {

    @Autowired
    public SellerDao sellerDao;

    @Before
    public void setUp() {
        sellerDao.createQuery("delete Customer").executeUpdate();
        sellerDao.createQuery("delete Manager").executeUpdate();
        sellerDao.createQuery("delete Seller").executeUpdate();
        sellerDao.createQuery("delete Account").executeUpdate();
    }

    //Para não gerar erro de classe sem teste
    @Test
    public void byPass() {
        Assert.assertTrue(Boolean.TRUE);
    }
}
