package br.com.account.controller;

import br.com.account.domain.Seller;
import br.com.account.service.AuthenticateService;
import br.com.account.service.SellerService;
import br.com.account.validator.AccountJsonValidator;
import br.com.commons.controller.AbstractController;
import br.com.commons.exception.ForbidenException;
import br.com.commons.helper.DomainCopyProperties;
import br.com.commons.json.AlabastrumAccountJson;
import br.com.commons.json.AuthenticateJson;
import br.com.commons.json.AuthenticationTokenJson;
import br.com.commons.json.SellerJson;
import br.com.commons.security.Authentication;
import br.com.commons.security.AuthenticationRole;
import java.math.BigInteger;
import java.security.MessageDigest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "/seller")
public class SellerController extends AbstractController {

    @Autowired
    private SellerService sellerService;

    @Autowired
    private AccountJsonValidator accountJsonValidator;

    @Autowired
    private AuthenticateService authenticateService;

    @RequestMapping(value = "/{codeOrNick}", method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<SellerJson> findByCodeOrNick(@RequestHeader(value = "Authorization") String authorization, @PathVariable("codeOrNick") String codeOrNick, HttpServletRequest request, HttpServletResponse response) throws Exception {
        Long storeId = validateAuthorization(authorization);
        final Seller seller = sellerService.findByNickOrCode(storeId, codeOrNick);

        //Regra de tela. Como o recurso não é protegido por senha nem todos os dados devem ser expostos
        SellerJson sellerJson = seller.toJson();
        sellerJson.setCpf(null);
        sellerJson.setPassword(null);

        return new ResponseEntity<>(sellerJson, HttpStatus.OK);
    }

    @RequestMapping(value = "/token/{token}", method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<SellerJson> findByToken(@RequestHeader(value = "Authorization") String authorization, @PathVariable("token") String token, HttpServletRequest request, HttpServletResponse response) throws Exception {
        validateAuthorization(authorization);
        token = authenticateService.validateToken(token);
        Seller seller = sellerService.findByToken(token);
        SellerJson sellerJson = seller.toJson();

        sellerJson.setAuthenticationToken(token);
        sellerJson.setPassword(null);

        return new ResponseEntity<>(sellerJson, HttpStatus.OK);
    }

    @SuppressWarnings("unchecked")
    @RequestMapping(method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity<AuthenticationTokenJson> create(@RequestHeader(value = "Authorization") String authorization, @RequestBody SellerJson sellerJson, HttpServletRequest request, HttpServletResponse response) throws Exception {
        validateAuthorization(authorization);
        accountJsonValidator.validate(sellerJson);
        Seller seller = new Seller(sellerJson);
        seller = sellerService.create(seller);
        Authentication authentication = new Authentication(seller.getId(), AuthenticationRole.SELLER);

        return new ResponseEntity<>(new AuthenticationTokenJson(authentication.getAuthenticationCode()), HttpStatus.CREATED);
    }

    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/{token}", method = RequestMethod.PUT)
    public @ResponseBody
    ResponseEntity<AuthenticationTokenJson> update(@RequestHeader(value = "Authorization") String authorization, @PathVariable("token") String token, @RequestBody SellerJson sellerJson, HttpServletRequest request, HttpServletResponse response) throws Exception {
        validateAuthorization(authorization);
        token = authenticateService.validateToken(token);
        Seller sellerToUpdate = sellerService.findByToken(token); // Busca o vendedor da base.
        Seller seller = new Seller(sellerJson); // cria um vendedor a partir do JSON.
        DomainCopyProperties.copyPropertiesIgnored(seller, sellerToUpdate, new String[]{"password"}); // copia todos os atributos, exceto o password, do seller para o sellerToUpdate.
        sellerToUpdate = sellerService.update(sellerToUpdate);
        Authentication authentication = new Authentication(sellerToUpdate.getId(), AuthenticationRole.SELLER);

        return new ResponseEntity<>(new AuthenticationTokenJson(authentication.getAuthenticationCode()), HttpStatus.CREATED);
    }

    @SuppressWarnings("unchecked")
    @RequestMapping(value = "importFromAlabstrum", method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity<AuthenticationTokenJson> importFromAlabstrum(@RequestHeader(value = "Authorization") String authorization, @RequestBody AuthenticateJson authenticateJson, HttpServletRequest request, HttpServletResponse response) throws Exception {
        validateAuthorization(authorization);
        MessageDigest m = MessageDigest.getInstance("MD5");
        m.update(authenticateJson.getPassword().getBytes(), 0, authenticateJson.getPassword().length());
        String md5 = new BigInteger(1, m.digest()).toString(16);

        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("codigo", authenticateJson.getEmail());
        map.add("senha", md5);

        AlabastrumAccountJson alabastrumAccountJson = null;

        try {
            ResponseEntity<AlabastrumAccountJson> getForEntity = getRestTemplate().postForEntity("http://escritoriovirtual.alabastrum.com.br/loginAPI/autenticar", map, AlabastrumAccountJson.class);
            alabastrumAccountJson = getForEntity.getBody();
        } catch (Exception e) {
            throw new ForbidenException("Código ou senha do escritório virtual inválido!");
        }

        Seller seller = new Seller();
        seller.setEmail(alabastrumAccountJson.getUsuario().geteMail());
        seller.setCpf(alabastrumAccountJson.getUsuario().getCPF() == null ? "00000000000" : alabastrumAccountJson.getUsuario().getCPF());
        seller.setName(alabastrumAccountJson.getUsuario().getvNome());
        seller.setNickName(alabastrumAccountJson.getUsuario().getvNome());
        seller.setCode(authenticateJson.getEmail());
        seller.setPassword(authenticateJson.getPassword());

        seller = sellerService.create(seller);

        Authentication authentication = new Authentication(seller.getId(), AuthenticationRole.SELLER);

        return new ResponseEntity<>(new AuthenticationTokenJson(authentication.getAuthenticationCode()), HttpStatus.CREATED);
    }

}
