package br.com.account.dao;

import br.com.account.dao.interf.IAddressDao;
import br.com.account.domain.Address;
import br.com.commons.dao.AbstractDao;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

public class AddressDao extends AbstractDao<Address> implements IAddressDao {

    @Override
    public Address save(Address address) {
        return super.save(address);
    }

    public List<Address> findByCustomerId(Long customerId) {
        List<Criterion> restrictions = new ArrayList<>();
        restrictions.add(Restrictions.eq("customer.id", customerId));
        List<Address> address = super.findByRestrictions(Address.class, restrictions);

        return address;
    }
}
