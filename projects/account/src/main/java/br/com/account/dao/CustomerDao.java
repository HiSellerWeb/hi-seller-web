package br.com.account.dao;

import br.com.account.dao.interf.Authenticate;
import br.com.account.dao.interf.ICustomerDao;
import br.com.account.domain.Authenticatable;
import br.com.account.domain.Customer;
import br.com.commons.dao.AbstractDao;
import br.com.commons.security.Encrypter;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Restrictions;

public class CustomerDao extends AbstractDao<Customer> implements ICustomerDao, Authenticate {

    @Override
    public Customer save(Customer customer) {
        return super.save(customer);
    }

    @Override
    public List<Authenticatable> findByEmailAndPassword(Long storeId, String email, String password) {

        List<Criterion> restrictions = new ArrayList<>();
        restrictions.add(Restrictions.eq("storeId", storeId));

        LogicalExpression logicAnd = Restrictions.and(Restrictions.eq("email", email).ignoreCase(), Restrictions.eq("password", Encrypter.getEncryptedPassword(password)));
        restrictions.add(logicAnd);

        List<Customer> managers = super.findByRestrictions(Customer.class, restrictions);
        return new ArrayList<>(managers);
    }

    @Override
    public Customer findById(Long id) {
        return super.findById(Customer.class, id);
    }

    @Override
    public Customer update(Customer customer) {
        return super.update(customer);
    }

    @Override
    public List<Customer> findByEmailOrNick(Long storeId, String email, String nickName) {
        List<Criterion> restrictions = new ArrayList<>();
        restrictions.add(Restrictions.eq("storeId", storeId));

        LogicalExpression logicAnd = Restrictions.or(Restrictions.eq("email", email).ignoreCase(), Restrictions.eq("nickName", Encrypter.getEncryptedPassword(nickName)));
        restrictions.add(logicAnd);

        return super.findByRestrictions(Customer.class, restrictions);
    }

}
