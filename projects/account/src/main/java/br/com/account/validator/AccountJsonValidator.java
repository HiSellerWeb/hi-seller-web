package br.com.account.validator;

import br.com.commons.json.AccountJson;
import br.com.commons.validator.AbstractValidator;
import br.com.commons.validator.RestrictionType;

public class AccountJsonValidator extends AbstractValidator<AccountJson> {

    @Override
    protected void apply(AccountJson accountJson) {
        if (accountJson == null) {
            setInvalid(true).setError("customerJson", RestrictionType.REQUIRED, "Objeto nulo");
            return;
        }

        isValidCpf(accountJson.getCpf()).setError("accountJson.cpf", RestrictionType.INVALID, "CPF inválido");
        isValidEmail(accountJson.getEmail()).setError("accountJson.email", RestrictionType.INVALID, "E-mail inválido");

        isNull(accountJson.getName()).setError("accountJson.name", RestrictionType.REQUIRED, "Obrigatório informar o nome");
        if (accountJson.getName() != null) {
            isBlank(accountJson.getName()).setError("accountJson.name", RestrictionType.REQUIRED, "O campo nome não pode estar em branco");
            isLengthBetween(accountJson.getName(), 3, 50).setError("categoryJson.name", RestrictionType.EXACT_LENGTH, "O nome deve conter entre 3 e 50 caracteres");
        }

        isNull(accountJson.getPassword()).setError("accountJson.password", RestrictionType.REQUIRED, "Obrigatório informar a senha");
        if (accountJson.getPassword() != null) {
            isBlank(accountJson.getPassword()).setError("accountJson.password", RestrictionType.REQUIRED, "O campo senha não pode estar em branco");
            isLengthBetween(accountJson.getPassword(), 8, 12).setError("categoryJson.password", RestrictionType.EXACT_LENGTH, "A senha deve conter entre 8 e 12 caracteres");
        }
    }
}
