package br.com.account.controller;

import br.com.account.service.AuthenticateService;
import br.com.account.validator.AuthenticateJsonValidator;
import br.com.commons.controller.AbstractController;
import br.com.commons.json.AuthenticateJson;
import br.com.commons.json.AuthenticationTokenJson;
import br.com.commons.security.AuthenticationRole;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "/authenticate")
public class AuthenticateController extends AbstractController {

    @Autowired
    private AuthenticateService authenticateService;

    @Autowired
    private AuthenticateJsonValidator authenticateJsonValidator;

    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/seller", method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity<AuthenticationTokenJson> authenticateSeller(@RequestHeader(value = "Authorization") String authorization, @RequestBody AuthenticateJson authenticateJson, HttpServletRequest request, HttpServletResponse response) throws Exception {
        Long storeId = validateAuthorization(authorization);
        authenticateJsonValidator.validate(authenticateJson);
        String authenticationToken = authenticateService.authenticate(storeId, authenticateJson, AuthenticationRole.SELLER);
        return new ResponseEntity<>(new AuthenticationTokenJson(authenticationToken), HttpStatus.OK);
    }

    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/manager", method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity<AuthenticationTokenJson> authenticateManager(@RequestHeader(value = "Authorization") String authorization, @RequestBody AuthenticateJson authenticateJson, HttpServletRequest request, HttpServletResponse response) throws Exception {
        Long storeId = validateAuthorization(authorization);
        authenticateJsonValidator.validate(authenticateJson);
        String authenticationToken = authenticateService.authenticate(storeId, authenticateJson, AuthenticationRole.MANAGER);
        return new ResponseEntity<>(new AuthenticationTokenJson(authenticationToken), HttpStatus.OK);
    }

    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/customer", method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity<AuthenticationTokenJson> authenticateCustomer(@RequestHeader(value = "Authorization") String authorization, @RequestBody AuthenticateJson authenticateJson, HttpServletRequest request, HttpServletResponse response) throws Exception {
        Long storeId = validateAuthorization(authorization);
        authenticateJsonValidator.validate(authenticateJson);
        String authenticationToken = authenticateService.authenticate(storeId, authenticateJson, AuthenticationRole.CUSTOMER);
        return new ResponseEntity<>(new AuthenticationTokenJson(authenticationToken), HttpStatus.OK);
    }

    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/validate/{token}", method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<AuthenticationTokenJson> validate(@RequestHeader(value = "Authorization") String authorization, @PathVariable String token, HttpServletRequest request, HttpServletResponse response) throws Exception {
        validateAuthorization(authorization);
        String newToken = authenticateService.validateToken(token);
        return new ResponseEntity<>(new AuthenticationTokenJson(newToken), HttpStatus.OK);
    }

}
