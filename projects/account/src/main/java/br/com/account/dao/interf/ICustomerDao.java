package br.com.account.dao.interf;

import br.com.account.domain.Authenticatable;
import br.com.account.domain.Customer;
import java.util.List;

public interface ICustomerDao {

    public Customer save(Customer manager);

    public List<Authenticatable> findByEmailAndPassword(Long storeId, String email, String password);

    public Customer findById(Long id);

    public Customer update(Customer customer);

    public List<Customer> findByEmailOrNick(Long storeId, String email, String nickName);

}
