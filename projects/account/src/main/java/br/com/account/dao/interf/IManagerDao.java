package br.com.account.dao.interf;

import br.com.account.domain.Authenticatable;
import br.com.account.domain.Manager;
import java.util.List;

public interface IManagerDao {

    public Manager save(Manager manager);

    public List<Authenticatable> findByEmailAndPassword(Long storeId, String email, String password);

    public Manager findById(Long valueOf);

}
