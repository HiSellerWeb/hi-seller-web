package br.com.account.domain;

import br.com.commons.json.AccountJson;
import br.com.commons.security.Encrypter;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.apache.log4j.Logger;

@Entity
@Table(name = "account")
@Inheritance(strategy = InheritanceType.JOINED)
public class Account implements Authenticatable, Serializable {

    private static final Logger LOG = Logger.getLogger(Account.class.getName());
    private static final long serialVersionUID = 9057014729215679060L;

    @Id
    @SequenceGenerator(name = "account_id", sequenceName = "account_id_seg")
    @GeneratedValue(generator = "account_id", strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "storeId", nullable = false, updatable = false)
    private Long storeId;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "nickName", nullable = false)
    private String nickName;

    @Column(name = "email", nullable = false)
    private String email;

    @Column(name = "password", nullable = false, length = 512)
    private String password;

    @Column(name = "cpf", nullable = false, length = 11)
    private String cpf;

    public AccountJson toJson() {
        AccountJson accountJson = new AccountJson();
        accountJson.setId(getId());
        accountJson.setCpf(getCpf());
        accountJson.setEmail(getEmail());
        accountJson.setNickName(getNickName());
        accountJson.setPassword(getPassword());
        accountJson.setName(getName());
        accountJson.setStoreId(getStoreId());

        return accountJson;
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getPassword() {
        return password;
    }

    public Long getStoreId() {
        return storeId;
    }

    public void setStoreId(Long storeId) {
        this.storeId = storeId;
    }

    public void setPassword(String password) {
        try {
            if (password != null) {
                this.password = Encrypter.getEncryptedPassword(password);
            } else {
                this.password = password;
            }
        } catch (Exception ex) {
            LOG.error("Erro ao encriptar a senha");
        }
    }

}
