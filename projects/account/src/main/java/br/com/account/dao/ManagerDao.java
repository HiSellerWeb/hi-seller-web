package br.com.account.dao;

import br.com.account.dao.interf.Authenticate;
import br.com.account.dao.interf.IManagerDao;
import br.com.account.domain.Authenticatable;
import br.com.account.domain.Manager;
import br.com.commons.dao.AbstractDao;
import br.com.commons.security.Encrypter;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Restrictions;

public class ManagerDao extends AbstractDao<Manager> implements IManagerDao, Authenticate {

    @Override
    public Manager save(Manager manager) {
        return super.save(manager);
    }

    @Override
    public List<Authenticatable> findByEmailAndPassword(Long storeId, String email, String password) {

        List<Criterion> restrictions = new ArrayList<>();
        restrictions.add(Restrictions.eq("storeId", storeId));

        LogicalExpression logicAnd = Restrictions.and(Restrictions.eq("email", email).ignoreCase(), Restrictions.eq("password", Encrypter.getEncryptedPassword(password)));
        restrictions.add(logicAnd);
        List<Manager> managers = super.findByRestrictions(Manager.class, restrictions);
        return new ArrayList<>(managers);
    }

    @Override
    public Manager findById(Long id) {
        return super.findById(Manager.class, id);
    }

}
