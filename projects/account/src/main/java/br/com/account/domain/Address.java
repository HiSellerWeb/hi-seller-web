package br.com.account.domain;

import br.com.commons.json.AddressJson;
import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Table(name = "address")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Address implements Serializable {

    private static final long serialVersionUID = 4340163893350478119L;

    @Id
    @SequenceGenerator(name = "address_id", sequenceName = "address_id_seg")
    @GeneratedValue(generator = "address_id", strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "storeId", nullable = false, updatable = false)
    private Long storeId;

    @Column(name = "city", nullable = false)
    private String city;

    @Column(name = "uf", nullable = false, length = 2)
    private String uf;

    @Column(name = "neighborhood", nullable = false)
    private String neighborhood;

    @Column(name = "street", nullable = false)
    private String street;

    @Column(name = "number", nullable = false)
    private String number;

    @Column(name = "complement")
    private String complement;

    @Column(name = "zipCode", nullable = false)
    private String zipCode;

    @Column(name = "active", nullable = false)
    private Boolean active;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "customer", nullable = false)
    private Customer customer;

    @Column(name = "recipient", nullable = false, length = 20)
    private String recipient;

    public Address() {
    }

    public Address(AddressJson addressJson) {
        setId(addressJson.getId());
        setCity(addressJson.getCity());
        setUf(addressJson.getUf());
        setNeighborhood(addressJson.getNeighborhood());
        setStreet(addressJson.getStreet());
        setNumber(addressJson.getNumber());
        setComplement(addressJson.getComplement());
        setZipCode(addressJson.getZipCode());
        setActive(addressJson.getActive());
        setRecipient(addressJson.getRecipient());
        setStoreId(addressJson.getStoreId());
    }

    public AddressJson toJson() {
        AddressJson addressJson = new AddressJson();
        addressJson.setId(getId());
        addressJson.setCity(getCity());
        addressJson.setUf(getUf());
        addressJson.setNeighborhood(getNeighborhood());
        addressJson.setStreet(getStreet());
        addressJson.setNumber(getNumber());
        addressJson.setComplement(getComplement());
        addressJson.setZipCode(getZipCode());
        addressJson.setActive(getActive());
        addressJson.setCustomerId(getCustomer().getId());
        addressJson.setRecipient(getRecipient());

        return addressJson;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public String getNeighborhood() {
        return neighborhood;
    }

    public void setNeighborhood(String neighborhood) {
        this.neighborhood = neighborhood;
    }

    public String getComplement() {
        return complement;
    }

    public void setComplement(String complement) {
        this.complement = complement;
    }

    public String getUf() {
        return uf;
    }

    public void setUf(String uf) {
        this.uf = uf;
    }

    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    public Long getStoreId() {
        return storeId;
    }

    public void setStoreId(Long storeId) {
        this.storeId = storeId;
    }
}
