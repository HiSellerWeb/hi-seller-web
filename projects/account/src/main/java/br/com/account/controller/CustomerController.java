package br.com.account.controller;

import br.com.account.config.ConfigEnum;
import br.com.account.domain.Account;
import br.com.account.domain.Customer;
import br.com.account.service.AuthenticateService;
import br.com.account.service.CustomerService;
import br.com.account.validator.AccountJsonValidator;
import br.com.commons.appConfig.AppConfig;
import br.com.commons.controller.AbstractController;
import br.com.commons.exception.ForbidenException;
import br.com.commons.helper.DomainCopyProperties;
import br.com.commons.helper.EmailService;
import br.com.commons.json.AuthenticationTokenJson;
import br.com.commons.json.CustomerJson;
import br.com.commons.security.Authentication;
import br.com.commons.security.AuthenticationRole;
import br.com.commons.security.Encrypter;
import java.util.List;
import java.util.Random;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "/customer")
public class CustomerController extends AbstractController {

    @Autowired
    private CustomerService customerService;

    @Autowired
    private AuthenticateService authenticateService;

    @Autowired
    private AccountJsonValidator accountJsonValidator;

    @Autowired
    private AppConfig appConfig;

    @RequestMapping(value = "/{token}", method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<CustomerJson> findByToken(@RequestHeader(value = "Authorization") String authorization, @PathVariable("token") String token, HttpServletRequest request, HttpServletResponse response) throws Exception {
        validateAuthorization(authorization);
        token = authenticateService.validateToken(token);
        Customer customer = customerService.findByToken(token);
        CustomerJson customerJson = customer.toJson();

        customerJson.setAuthenticationToken(token);
        customerJson.setPassword(null);

        return new ResponseEntity<>(customerJson, HttpStatus.OK);
    }

    @RequestMapping(value = "/forgotPassword/{nickName}", method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<CustomerJson> findByNickName(@RequestHeader(value = "Authorization") String authorization, @PathVariable("nickName") String nickName, HttpServletRequest request, HttpServletResponse response) throws Exception {
        Long storeId = validateAuthorization(authorization);
        List<Account> account = customerService.findByNickName(storeId, nickName);
        if (account != null && !account.isEmpty()) {
            final Customer customerToUpdate = (Customer) customerService.findByNickName(storeId, nickName).get(0);
            String newPassword = generateRandomPassword();
            customerToUpdate.setPassword(newPassword);
            customerService.update(customerToUpdate);
            EmailService email = new EmailService();
            email.sendRecoveryPasswordEmail(customerToUpdate.getEmail(), nickName, newPassword);
        }
        CustomerJson customerJson = ((Customer) account.get(0)).toJson();
        customerJson.setPassword(null);

        return new ResponseEntity<>(customerJson, HttpStatus.OK);
    }

    private String generateRandomPassword() {
        StringBuffer sb = new StringBuffer();
        Random rand = new Random();
        for (int i = 0; i < 8; i++) {
            //sb.append(rand.nextInt(10));
            sb.append(Integer.toString(Math.abs(rand.nextInt()) % 16, 16));
        }

        return sb.toString();
    }

    @SuppressWarnings("unchecked")
    @RequestMapping(method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity<AuthenticationTokenJson> create(@RequestHeader(value = "Authorization") String authorization, @RequestBody CustomerJson customerJson, HttpServletRequest request, HttpServletResponse response) throws Exception {
        validateAuthorization(authorization);
        accountJsonValidator.validate(customerJson);
        Customer customer = new Customer(customerJson);
        customer = customerService.create(customer);
        Authentication authentication = new Authentication(customer.getId(), AuthenticationRole.CUSTOMER);

        return new ResponseEntity<>(new AuthenticationTokenJson(authentication.getAuthenticationCode()), HttpStatus.CREATED);
    }

    @SuppressWarnings({"unchecked"})
    @RequestMapping(value = "/{token}/{newPassword}", method = RequestMethod.PUT)
    public @ResponseBody
    ResponseEntity<CustomerJson> edit(@RequestHeader(value = "Authorization") String authorization, @PathVariable("token") String token, @PathVariable("newPassword") String newPassword, @RequestBody CustomerJson customerJson, HttpServletRequest request, HttpServletResponse response) throws Exception {
        Long storeId = validateAuthorization(authorization);
        validateCustomerToken(appConfig.getValue(storeId, ConfigEnum.ACCOUNT_ENDPOINT), storeId, token);
        final Customer customerToUpdate = customerService.findByToken(token);
        accountJsonValidator.validate(customerJson);

        if (Encrypter.getEncryptedPassword(customerJson.getPassword()) != null
                && Encrypter.getEncryptedPassword(customerJson.getPassword()).equals(customerToUpdate.getPassword())) {
            Customer customer = new Customer(customerJson);
            customer.setPassword(null);
            DomainCopyProperties.copyPropertiesIgnored(customer, customerToUpdate);
            if ("undefined".equals(newPassword)) {
                customerToUpdate.setPassword(customerJson.getPassword());
            } else {
                customerToUpdate.setPassword(newPassword);
            }
            customerService.update(customerToUpdate);
            customerToUpdate.setPassword(null);

//            if (!"undefined".equals(newPassword)) {
//                sendEmailChangePassword(customerToUpdate.getEmail(), customerToUpdate.getNickName());
//            }
        } else {
            throw new ForbidenException("A senha informada está incorreta!");
        }

        return new ResponseEntity<>(customerToUpdate.toJson(), HttpStatus.OK);
    }

}
