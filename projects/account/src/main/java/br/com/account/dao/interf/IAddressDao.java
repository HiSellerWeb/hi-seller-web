package br.com.account.dao.interf;

import br.com.account.domain.Address;
import java.util.List;

public interface IAddressDao {

    public Address save(Address address);

    public List<Address> findByCustomerId(Long customerId);
}
