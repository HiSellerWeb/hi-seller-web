package br.com.account.controller;

import br.com.account.domain.Address;
import br.com.account.domain.Customer;
import br.com.account.service.AddressService;
import br.com.account.service.AuthenticateService;
import br.com.account.service.CustomerService;
import br.com.commons.controller.AbstractController;
import br.com.commons.json.AddressJson;
import br.com.commons.json.AddressListJson;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "/address")
public class AddressController extends AbstractController {

    @Autowired
    private AddressService addressService;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private AuthenticateService authenticateService;

    @RequestMapping(value = "customer/{token}", method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<AddressListJson> findByCustomerId(@RequestHeader(value = "Authorization") String authorization, @PathVariable("token") String token, HttpServletRequest request, HttpServletResponse response) throws Exception {
        validateAuthorization(authorization);
        token = authenticateService.validateToken(token);
        Customer customer = customerService.findByToken(token);

        final List<Address> allAddress = addressService.findByCustomerId(customer.getId());
        final AddressListJson addressListJson = new AddressListJson();

        if (allAddress != null && !allAddress.isEmpty()) {
            for (Address ad : allAddress) {
                addressListJson.add(ad.toJson());
            }
        }
        return new ResponseEntity<>(addressListJson, HttpStatus.OK);
    }

    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/{token}", method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity<AddressJson> addAddresses(@RequestHeader(value = "Authorization") String authorization, @PathVariable("token") String token, @RequestBody AddressJson addressJson, HttpServletRequest request, HttpServletResponse response) throws Exception {
        validateAuthorization(authorization);
        token = authenticateService.validateToken(token);
        Customer customer = customerService.findByToken(token);

        Address address = new Address(addressJson);
        address.setActive(true);
        address.setCustomer(customer);
        address = addressService.create(address);

        return new ResponseEntity<>(address.toJson(), HttpStatus.CREATED);
    }

}
