package br.com.account.dao.interf;

import br.com.account.domain.Authenticatable;
import br.com.account.domain.Seller;
import java.util.List;

public interface ISellerDao {

    public Seller save(Seller seller);

    public List<Seller> findByCodeOrNick(Long storeId, String codeOrNick);

    public List<Authenticatable> findByEmailAndPassword(Long storeId, String email, String password);

    public List<Seller> findByCpf(Long storeId, String cpf);

    public Seller findById(Long valueOf);

    public Seller update(Seller seller);

}
