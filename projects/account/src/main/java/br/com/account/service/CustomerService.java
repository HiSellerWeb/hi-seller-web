package br.com.account.service;

import br.com.account.dao.interf.IAccountDao;
import br.com.account.dao.interf.ICustomerDao;
import br.com.account.domain.Account;
import br.com.account.domain.Customer;
import br.com.commons.exception.ConflictException;
import br.com.commons.exception.NotFoundException;
import br.com.commons.security.Authentication;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CustomerService {

    @Autowired
    private ICustomerDao customerDao;

    @Autowired
    private IAccountDao accountDao;

    public Customer findByToken(String token) throws Exception {
        Authentication recoveredAuthentication = Authentication.fromString(token);
        return customerDao.findById(Long.valueOf(recoveredAuthentication.getId()));
    }

    public Customer create(Customer customer) throws Exception {
        if (!accountDao.findByEmail(customer.getStoreId(), customer.getEmail()).isEmpty()) {
            throw new ConflictException("E-mail já cadastrado.");
        }
        if (!accountDao.findByNick(customer.getStoreId(), customer.getNickName()).isEmpty()) {
            throw new ConflictException("Apelido já cadastrado.");
        }
        customer.setId(null);
        return customerDao.save(customer);
    }

    public Customer update(Customer customer) {
        return customerDao.update(customer);
    }

    public List<Account> findByNickName(Long storeId, String nick) throws Exception {
        List<Account> listAccount = accountDao.findByNick(storeId, nick);
        if (listAccount.isEmpty()) {
            throw new NotFoundException("Apelido não encontrado.");
        }

        return listAccount;
    }
}
