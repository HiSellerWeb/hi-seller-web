package br.com.account.dao;

import br.com.account.dao.interf.IAccountDao;
import br.com.account.domain.Account;
import br.com.commons.dao.AbstractDao;
import java.util.Arrays;
import java.util.List;
import org.hibernate.criterion.Restrictions;

public class AccountDao extends AbstractDao<Account> implements IAccountDao {

    @Override
    public List<Account> findByEmail(Long storeId, String email) {
        return super.findByRestrictions(Account.class, Arrays.asList(Restrictions.eq("email", email).ignoreCase()));
    }

    @Override
    public List<Account> findByNick(Long storeId, String nick) {
        return super.findByRestrictions(Account.class, Arrays.asList(Restrictions.eq("nickName", nick).ignoreCase()));
    }
}
