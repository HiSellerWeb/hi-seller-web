package br.com.account.dao;

import br.com.account.dao.interf.Authenticate;
import br.com.account.dao.interf.ISellerDao;
import br.com.account.domain.Authenticatable;
import br.com.account.domain.Seller;
import br.com.commons.dao.AbstractDao;
import br.com.commons.security.Encrypter;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Restrictions;

public class SellerDao extends AbstractDao<Seller> implements ISellerDao, Authenticate {

    @Override
    public Seller save(Seller seller) {
        return super.save(seller);
    }

    @Override
    public List<Seller> findByCodeOrNick(Long storeId, String codeOrNick) {
        if (StringUtils.isEmpty(codeOrNick)) {
            return null;
        }

        List<Criterion> restrictions = new ArrayList<>();
        restrictions.add(Restrictions.eq("storeId", storeId));

        LogicalExpression logicOr = Restrictions.or(Restrictions.eq("nickName", codeOrNick).ignoreCase(), Restrictions.eq("code", codeOrNick).ignoreCase());
        restrictions.add(logicOr);
        return super.findByRestrictions(Seller.class, restrictions);
    }

    @Override
    public List<Authenticatable> findByEmailAndPassword(Long storeId, String email, String password) {

        List<Criterion> restrictions = new ArrayList<>();
        restrictions.add(Restrictions.eq("storeId", storeId));

        LogicalExpression logicAnd = Restrictions.and(Restrictions.eq("email", email).ignoreCase(), Restrictions.eq("password", Encrypter.getEncryptedPassword(password)));
        restrictions.add(logicAnd);
        List<Seller> sellers = super.findByRestrictions(Seller.class, restrictions);
        return new ArrayList<>(sellers);
    }

    @Override
    public List<Seller> findByCpf(Long storeId, String cpf) {
        List<Criterion> restrictions = new ArrayList<>();
        restrictions.add(Restrictions.eq("storeId", storeId));

        restrictions.add(Restrictions.and(Restrictions.eq("cpf", cpf).ignoreCase()));
        List<Seller> sellers = super.findByRestrictions(Seller.class, restrictions);
        return new ArrayList<>(sellers);
    }

    @Override
    public Seller findById(Long id) {
        return super.findById(Seller.class, id);
    }

    @Override
    public Seller update(Seller seller) {
        return super.update(seller);
    }
}
