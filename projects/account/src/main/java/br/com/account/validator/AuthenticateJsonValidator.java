package br.com.account.validator;

import br.com.commons.json.AuthenticateJson;
import br.com.commons.validator.AbstractValidator;
import br.com.commons.validator.RestrictionType;

public class AuthenticateJsonValidator extends AbstractValidator<AuthenticateJson> {

    @Override
    protected void apply(AuthenticateJson authenticateJson) {
        if (authenticateJson == null) {
            setInvalid(true).setError("authenticateJson", RestrictionType.REQUIRED, "Objeto nulo");
            return;
        }

        isNull(authenticateJson.getEmail()).setError("authenticateJson.email", RestrictionType.REQUIRED, "Obrigatório informar o e-mail");

        if (authenticateJson.getEmail() != null) {
            isValidEmail(authenticateJson.getEmail()).setError("authenticateJson.email", RestrictionType.INVALID, "E-mail inválido");
        }

        isNull(authenticateJson.getPassword()).setError("authenticateJson.password", RestrictionType.REQUIRED, "Obrigatório informar a senha");

        if (authenticateJson.getPassword() != null) {
            isBlank(authenticateJson.getPassword()).setError("authenticateJson.password", RestrictionType.REQUIRED, "A senha não pode estar em branco");
            isLengthBetween(authenticateJson.getPassword(), 8, 20).setError("authenticateJson.password", RestrictionType.EXACT_LENGTH, "A senha deve conter entre 8 e 20 caracteres");
        }
    }
}
