package br.com.account.domain;

import br.com.commons.json.ManagerJson;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Table(name = "manager")
@PrimaryKeyJoinColumn(name = "id")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Manager extends Account implements Serializable {

    private static final long serialVersionUID = 8849537779471482397L;

    public Manager() {
    }

    public Manager(ManagerJson managerJson) throws Exception {
        this.setId(managerJson.getId());
        this.setCpf(managerJson.getCpf());
        this.setEmail(managerJson.getEmail());
        this.setNickName(managerJson.getNickName());
        this.setPassword(managerJson.getPassword());
        this.setName(managerJson.getName());
        this.setStoreId(managerJson.getStoreId());
    }

    @Override
    public ManagerJson toJson() {
        return new ManagerJson(super.toJson());
    }

}
