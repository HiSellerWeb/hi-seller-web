package br.com.account.domain;

import br.com.commons.json.CustomerJson;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
@Table(name = "customer")
@PrimaryKeyJoinColumn(name = "id")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Customer extends Account implements Serializable {

    private static final long serialVersionUID = -8125446707717708570L;

    @OneToMany(mappedBy = "customer", cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Address> addresses = new ArrayList<>();

    public Customer() {
    }

    public Customer(CustomerJson customerJson) throws Exception {
        this.setId(customerJson.getId());
        this.setCpf(customerJson.getCpf());
        this.setEmail(customerJson.getEmail());
        this.setNickName(customerJson.getNickName());
        this.setPassword(customerJson.getPassword());
        this.setName(customerJson.getName());
        this.setStoreId(customerJson.getStoreId());
    }

    @Override
    public CustomerJson toJson() {
        CustomerJson customerJson = new CustomerJson(super.toJson());
        if (getAddresses() != null) {
            addresses.stream().forEach((address) -> {
                customerJson.addAddress(address.getId());
            });
        }
        return customerJson;
    }

    public List<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<Address> addresses) {
        if (addresses == null) {
            addresses = new ArrayList<>();
        }
        this.addresses = addresses;
    }
}
