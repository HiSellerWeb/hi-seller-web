package br.com.account.dao.interf;

import br.com.account.domain.Account;
import java.util.List;

public interface IAccountDao {

    public List<Account> findByEmail(Long storeId, String email);

    public List<Account> findByNick(Long storeId, String nick);

}
