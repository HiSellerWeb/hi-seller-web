package br.com.account.service;

import br.com.account.dao.interf.IAccountDao;
import br.com.account.dao.interf.ISellerDao;
import br.com.account.domain.Seller;
import br.com.commons.exception.ConflictException;
import br.com.commons.exception.NotFoundException;
import br.com.commons.security.Authentication;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SellerService {

    @Autowired
    private ISellerDao sellerDao;

    @Autowired
    private IAccountDao accountDao;

    public Seller findByNickOrCode(Long storeId, String nickOrCode) throws NotFoundException, ConflictException {
        List<Seller> sellers = sellerDao.findByCodeOrNick(storeId, nickOrCode);

        if (sellers == null || sellers.isEmpty()) {
            throw new NotFoundException("Vendedor não encontrado, favor tente outro código/nick");
        } else if (sellers.size() > 1) {
            throw new ConflictException();
        } else {
            return sellers.get(0);
        }
    }

    public Seller create(Seller seller) throws ConflictException {
        if (!accountDao.findByEmail(seller.getStoreId(), seller.getEmail()).isEmpty()) {
            throw new ConflictException("E-mail já cadastrado.");
        }
        if (!accountDao.findByNick(seller.getStoreId(), seller.getNickName()).isEmpty()) {
            throw new ConflictException("Apelido já cadastrado.");
        }
        return sellerDao.save(seller);
    }

    public Seller findByToken(String token) throws Exception {
        Authentication recoveredAuthentication = Authentication.fromString(token);
        return sellerDao.findById(Long.valueOf(recoveredAuthentication.getId()));
    }

    public Seller update(Seller seller) {
        return sellerDao.update(seller);
    }
}
