package br.com.account.controller;

import br.com.account.config.ConfigEnum;
import br.com.commons.appConfig.AppConfig;
import br.com.commons.controller.AbstractController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class ConfigController extends AbstractController {

    @Autowired
    private AppConfig appConfig;

    @RequestMapping(method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<String> updateProperties() throws Exception {
//        TODO: Ajustar busca storeId
        appConfig.loadAppConfigProperties(0L, ConfigEnum.values());
        return new ResponseEntity<>(AppConfig.getPROPERTIES().toString(), HttpStatus.OK);
    }

}
