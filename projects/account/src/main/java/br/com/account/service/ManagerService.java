package br.com.account.service;

import br.com.account.dao.interf.IAccountDao;
import br.com.account.dao.interf.IManagerDao;
import br.com.account.domain.Manager;
import br.com.commons.exception.ConflictException;
import br.com.commons.security.Authentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ManagerService {

    @Autowired
    private IManagerDao managerDao;

    @Autowired
    private IAccountDao accountDao;

    public Manager create(Manager manager) throws ConflictException {
        if (!accountDao.findByEmail(manager.getStoreId(), manager.getEmail()).isEmpty()) {
            throw new ConflictException("E-mail já cadastrado.");
        }
        if (!accountDao.findByNick(manager.getStoreId(), manager.getNickName()).isEmpty()) {
            throw new ConflictException("Apelido já cadastrado.");
        }
        return managerDao.save(manager);
    }

    public Manager findByToken(String token) throws Exception {
        Authentication recoveredAuthentication = Authentication.fromString(token);
        return managerDao.findById(Long.valueOf(recoveredAuthentication.getId()));
    }
}
