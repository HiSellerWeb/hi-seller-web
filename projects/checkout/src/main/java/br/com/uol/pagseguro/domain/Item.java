/*
 ************************************************************************
 Copyright [2011] [PagSeguro Internet Ltda.]

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 ************************************************************************
 */
package br.com.uol.pagseguro.domain;

import br.com.commons.json.ProductJson;
import java.math.BigDecimal;

/**
 * Represents a product/item in a transaction
 */
public class Item {

    /**
     * Product identifier, such as SKU
     */
    private Long id;

    /**
     * Product code
     */
    private String code;

    /**
     * Product description
     */
    private String description;

    /**
     * Product url image
     */
    private String urlImage;

    /**
     * Quantity
     */
    private Integer quantity = 0;

    /**
     * Product unit price
     */
    private BigDecimal amount;

    /**
     * Single unit weight, in grams
     */
    private Long weight;

    /**
     * Single unit shipping cost
     */
    private BigDecimal shippingCost;

    /**
     * Initializes a newly created instance of this type
     */
    public Item() {

    }

    /**
     * Initializes a newly created instance of this type with the specified
     * arguments
     *
     * @param id the product identifier
     * @param code the product code
     * @param description the product description
     * @param urlImage the product urlImage
     * @param quantity the product quantity
     * @param amount the product unit price
     */
    public Item(Long id, String code, String description, String urlImage, Integer quantity, BigDecimal amount) {
        this.id = id;
        this.code = code;
        this.description = description;
        this.urlImage = urlImage;
        this.quantity = quantity;
        this.amount = amount;
    }

    /**
     * Initializes a newly created instance of this type with the specified
     * arguments
     *
     * @param id the product identifier
     * @param description the product description
     * @param urlImage the product urlImage
     * @param quantity the product quantity
     * @param amount the product unit price
     * @param weight the product weight, in grams
     * @param shippingCost the product unit shippingCost
     */
    public Item(Long id, String code, String description, String urlImage, Integer quantity, BigDecimal amount, Long weight, BigDecimal shippingCost) {
        this.id = id;
        this.code = code;
        this.description = description;
        this.urlImage = urlImage;
        this.quantity = quantity;
        this.amount = amount;
        this.weight = weight;
        this.shippingCost = shippingCost;
    }

    public Item(ProductJson productJson) {
        this(productJson.getId(), productJson.getCode(), productJson.getCode() + " - " + productJson.getName(), productJson.getSkus().get(0).getImages().get(0), productJson.getQuantity(), productJson.getPrice(), productJson.getWeight(), null);
    }

    /**
     * @return the product description
     */
    public String getDescription() {
        return this.description;
    }

    /**
     * @param description the product description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the product urlImage
     */
    public String getUrlImage() {
        return this.urlImage;
    }

    /**
     * @param urlImage the product urlImage to set
     */
    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }

    /**
     * @return the product quantity
     */
    public Integer getQuantity() {
        return this.quantity;
    }

    /**
     * @param quantity the product quantity to set
     */
    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    /**
     * @return the product unit price
     */
    public BigDecimal getAmount() {
        return this.amount;
    }

    /**
     * @param amount the product unit price to set
     */
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    /**
     * @return the product unit weight, in grams
     */
    public Long getWeight() {
        return this.weight;
    }

    /**
     * @param weight the product unit weight, in grams, to set
     */
    public void setWeight(Long weight) {
        this.weight = weight;
    }

    /**
     * @return the product unit shipping cost
     */
    public BigDecimal getShippingCost() {
        return this.shippingCost;
    }

    /**
     * @param shippingCost the product unit shipping cost to set
     */
    public void setShippingCost(BigDecimal shippingCost) {
        this.shippingCost = shippingCost;
    }

    /**
     * @return the product total price
     */
    public double getTotalPrice() {
        return (this.amount.doubleValue() * this.quantity);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Item other = (Item) obj;
        if (id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!id.equals(other.id)) {
            return false;
        }
        return true;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

}
