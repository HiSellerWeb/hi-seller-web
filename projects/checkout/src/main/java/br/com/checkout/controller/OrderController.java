package br.com.checkout.controller;

import br.com.checkout.config.ConfigEnum;
import br.com.checkout.domain.PurchaseOrder;
import br.com.checkout.service.PaymentService;
import br.com.checkout.service.PurchaseOrderService;
import br.com.commons.appConfig.AppConfig;
import br.com.commons.controller.AbstractController;
import br.com.commons.exception.ForbidenException;
import br.com.commons.json.CustomerJson;
import br.com.commons.json.ManagerJson;
import br.com.commons.json.OrderJson;
import br.com.commons.json.SellerJson;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "/order")
public class OrderController extends AbstractController {

    private static final Log LOG = LogFactory.getLog(OrderController.class);

    @Autowired
    private PurchaseOrderService orderService;

    @Autowired
    private PaymentService paymentService;

    @Autowired
    private AppConfig appConfig;

    @SuppressWarnings("unchecked")
    @RequestMapping(value = "{customerToken}", method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<List<OrderJson>> getAllOrders(@RequestHeader(value = "Authorization") String authorization, @PathVariable("customerToken") String customerToken, HttpServletRequest request, HttpServletResponse response) throws Exception {
        Long storeId = validateAuthorization(authorization);
        List<OrderJson> orders = new ArrayList<>();
        CustomerJson customerJson = new CustomerJson();
        try {
            ResponseEntity<CustomerJson> entity = getForEntity(appConfig.getValue(storeId, ConfigEnum.ACCOUNT_ENDPOINT) + "/customer/" + customerToken, authorization, CustomerJson.class);
            customerJson = entity.getBody();
            if (!HttpStatus.OK.equals(entity.getStatusCode())) {
                throw new ForbidenException();
            }
        } catch (Exception e) {
            throw new ForbidenException();
        }
        List<PurchaseOrder> purchaseOrders = orderService.findAllByCustomerId(storeId, customerJson.getId());
        if (purchaseOrders != null) {
            for (PurchaseOrder order : purchaseOrders) {
                orders.add(order.toJson());
            }
        }
        return new ResponseEntity<>(orders, HttpStatus.OK);
    }

    @SuppressWarnings("unchecked")
    @RequestMapping(value = "{customerToken}/{order}", method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<OrderJson> getOrder(@RequestHeader(value = "Authorization") String authorization, @PathVariable("customerToken") String customerToken, @PathVariable("order") String order, HttpServletRequest request, HttpServletResponse response) throws Exception {
        Long storeId = validateAuthorization(authorization);
        PurchaseOrder purchaseOrder = orderService.findById(Long.valueOf(order));
        try {
            ResponseEntity<CustomerJson> entity = getForEntity(appConfig.getValue(storeId, ConfigEnum.ACCOUNT_ENDPOINT) + "/customer/" + customerToken, authorization, CustomerJson.class);
            CustomerJson customerJson = entity.getBody();
            if (!HttpStatus.OK.equals(entity.getStatusCode())
                    || !purchaseOrder.getCustomerId().equals(customerJson.getId())) {
                throw new ForbidenException();
            }
        } catch (Exception e) {
            throw new ForbidenException();
        }
        return new ResponseEntity<>(purchaseOrder.toJson(), HttpStatus.OK);
    }

    @SuppressWarnings("unchecked")
    @RequestMapping(value = "{storeId}/{customerToken}/{order}/viewTiket", method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<byte[]> viewTiket(@PathVariable("storeId") String storeId, @PathVariable("customerToken") String customerToken, @PathVariable("order") String order, HttpServletRequest request, HttpServletResponse response) throws Exception {
        PurchaseOrder purchaseOrder = orderService.findById(Long.valueOf(order));
        try {
            ResponseEntity<CustomerJson> entity = getForEntity(appConfig.getValue(Long.valueOf(storeId), ConfigEnum.ACCOUNT_ENDPOINT) + "/customer/" + customerToken, storeId, CustomerJson.class);
            CustomerJson customerJson = entity.getBody();
            if (!HttpStatus.OK.equals(entity.getStatusCode())
                    || !purchaseOrder.getCustomerId().equals(customerJson.getId())) {
                throw new ForbidenException();
            }
        } catch (Exception e) {
            throw new ForbidenException();
        }

        byte[] ticket = paymentService.getTicket(Long.valueOf(storeId), purchaseOrder);

        response.setContentType("application/pdf");
        response.setContentLength(ticket.length);
        try (ServletOutputStream ouputStream = response.getOutputStream()) {
            ouputStream.write(ticket, 0, ticket.length);
            ouputStream.flush();
            ouputStream.close();
        }

        return new ResponseEntity<>(ticket, HttpStatus.OK);
    }

    @SuppressWarnings("unchecked")
    @RequestMapping(value = "{managerToken}/findOrder", method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<List<OrderJson>> findOrderByStatus(@RequestHeader(value = "Authorization") String authorization, @PathVariable("managerToken") String managerToken, boolean paid, boolean packaged, boolean sent, boolean delivered, String orderBy, String direction, HttpServletRequest request, HttpServletResponse response) throws Exception {
        Long storeId = validateAuthorization(authorization);
        try {
            ResponseEntity<CustomerJson> entity = getForEntity(appConfig.getValue(storeId, ConfigEnum.ACCOUNT_ENDPOINT) + "/manager/" + managerToken, authorization, CustomerJson.class);
            if (!HttpStatus.OK.equals(entity.getStatusCode())) {
                throw new ForbidenException();
            }
        } catch (Exception e) {
            throw new ForbidenException();
        }

        List<PurchaseOrder> orders = orderService.findAllByStatus(storeId, paid, packaged, sent, delivered, orderBy, direction);
        List<OrderJson> ordersJson = new ArrayList<>();
        if (orders != null) {
            for (PurchaseOrder order : orders) {
                ordersJson.add(order.toJson());
            }
        }

        return new ResponseEntity<>(ordersJson, HttpStatus.OK);
    }

    @SuppressWarnings("unchecked")
    @RequestMapping(value = "{managerToken}/prepared", method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity<OrderJson> setPrepared(@RequestHeader(value = "Authorization") String authorization, @PathVariable("managerToken") String managerToken, @RequestBody OrderJson orderJson, HttpServletRequest request, HttpServletResponse response) throws Exception {
        Long storeId = validateAuthorization(authorization);
        try {
            ResponseEntity<CustomerJson> entity = getForEntity(appConfig.getValue(storeId, ConfigEnum.ACCOUNT_ENDPOINT) + "/manager/" + managerToken, authorization, CustomerJson.class);
            if (!HttpStatus.OK.equals(entity.getStatusCode())) {
                throw new ForbidenException();
            }
        } catch (Exception e) {
            throw new ForbidenException();
        }

        PurchaseOrder order = orderService.setPrepared(orderJson.getId());
        return new ResponseEntity<>(order.toJson(), HttpStatus.CREATED);
    }

    @SuppressWarnings("unchecked")
    @RequestMapping(value = "{managerToken}/sent", method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity<OrderJson> setShipped(@RequestHeader(value = "Authorization") String authorization, @PathVariable("managerToken") String managerToken, @RequestBody OrderJson orderJson, HttpServletRequest request, HttpServletResponse response) throws Exception {
        Long storeId = validateAuthorization(authorization);
        try {
            ResponseEntity<CustomerJson> entity = getForEntity(appConfig.getValue(storeId, ConfigEnum.ACCOUNT_ENDPOINT) + "/manager/" + managerToken, authorization, CustomerJson.class);
            if (!HttpStatus.OK.equals(entity.getStatusCode())) {
                throw new ForbidenException();
            }
        } catch (Exception e) {
            throw new ForbidenException();
        }

        PurchaseOrder order = orderService.setShipped(orderJson.getId(), orderJson.getShippingCode());
        return new ResponseEntity<>(order.toJson(), HttpStatus.CREATED);
    }

    @SuppressWarnings("unchecked")
    @RequestMapping(value = "{managerToken}/findSellersOrders", method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<List<SellerJson>> findOrderGrupBySeller(@RequestHeader(value = "Authorization") String authorization, @PathVariable("managerToken") String managerToken, @DateTimeFormat(pattern = "yyyy-MM-dd") Date dtStart, @DateTimeFormat(pattern = "yyyy-MM-dd") Date dtEnd, HttpServletRequest request, HttpServletResponse response) throws Exception {
        Long storeId = validateAuthorization(authorization);
        try {
            ResponseEntity<ManagerJson> entity = getForEntity(appConfig.getValue(storeId, ConfigEnum.ACCOUNT_ENDPOINT) + "/manager/" + managerToken, authorization, ManagerJson.class);
            if (!HttpStatus.OK.equals(entity.getStatusCode())) {
                throw new ForbidenException();
            }
        } catch (Exception e) {
            throw new ForbidenException();
        }

        List<SellerJson> sellersJson = orderService.findSellersOrders(authorization, storeId, dtStart, dtEnd);

        return new ResponseEntity<>(sellersJson, HttpStatus.OK);
    }

    @SuppressWarnings("unchecked")
    @RequestMapping(value = "{sellerToken}/findSellerOrders", method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<SellerJson> findOrdersBySeller(@RequestHeader(value = "Authorization") String authorization, @PathVariable("sellerToken") String sellerToken, @DateTimeFormat(pattern = "yyyy-MM-dd") Date dtStart, @DateTimeFormat(pattern = "yyyy-MM-dd") Date dtEnd, HttpServletRequest request, HttpServletResponse response) throws Exception {
        Long storeId = validateAuthorization(authorization);
        SellerJson sellersJson = null;
        try {
            ResponseEntity<SellerJson> entity = getForEntity(appConfig.getValue(storeId, ConfigEnum.ACCOUNT_ENDPOINT) + "/seller/token/" + sellerToken, authorization, SellerJson.class);
            if (!HttpStatus.OK.equals(entity.getStatusCode())) {
                throw new ForbidenException();
            }
            sellersJson = entity.getBody();
        } catch (Exception e) {
            throw new ForbidenException();
        }

        sellersJson = orderService.findBySeller(storeId, sellersJson, dtStart, dtEnd);

        return new ResponseEntity<>(sellersJson, HttpStatus.OK);
    }

}
