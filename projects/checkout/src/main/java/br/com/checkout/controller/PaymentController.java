package br.com.checkout.controller;

import br.com.checkout.config.ConfigEnum;
import br.com.checkout.domain.PurchaseOrder;
import br.com.checkout.service.PaymentService;
import br.com.commons.appConfig.AppConfig;
import br.com.commons.controller.AbstractController;
import br.com.commons.exception.ForbidenException;
import br.com.commons.json.ManagerJson;
import br.com.commons.json.OrderJson;
import br.com.commons.json.PagSeguroNotificationJson;
import br.com.commons.json.SystemInfoJson;
import br.com.commons.security.Encrypter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

@Controller
@RequestMapping(value = "/payment")
public class PaymentController extends AbstractController {

    private static final Log LOG = LogFactory.getLog(PaymentController.class);

    @Autowired
    private PaymentService paymentService;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private AppConfig appConfig;

    @SuppressWarnings("unchecked")
    @RequestMapping(value = "{managerToken}/confirmPayment", method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity<OrderJson> confirmPayment(@RequestHeader(value = "Authorization") String authorization, @PathVariable("managerToken") String managerToken, @RequestBody OrderJson orderJson, HttpServletRequest request, HttpServletResponse response) throws Exception {
        Long storeId = validateAuthorization(authorization);
        try {
            ResponseEntity<ManagerJson> entity = getForEntity(appConfig.getValue(storeId, ConfigEnum.ACCOUNT_ENDPOINT) + "/manager/" + managerToken, authorization, ManagerJson.class);
            if (!HttpStatus.OK.equals(entity.getStatusCode())) {
                throw new ForbidenException();
            }
        } catch (Exception e) {
            throw new ForbidenException();
        }
        PurchaseOrder order = paymentService.confirmPayment(authorization, storeId, orderJson.getTransaction());
        return new ResponseEntity<>(order.toJson(), HttpStatus.OK);
    }

    @SuppressWarnings("unchecked")
    @RequestMapping(value = "expirationDate", method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<SystemInfoJson> expirationDate(@RequestHeader(value = "Authorization") String authorization, HttpServletRequest request, HttpServletResponse response) throws Exception {
        Long storeId = validateAuthorization(authorization);
        SystemInfoJson expirationDate = paymentService.getExpirationDate(storeId);
        return new ResponseEntity<>(expirationDate, HttpStatus.OK);
    }

    @SuppressWarnings("unchecked")
    @RequestMapping(value = "renewExpirationDate", method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity<SystemInfoJson> renewExpirationDate(@RequestHeader(value = "Authorization") String authorization, @RequestBody SystemInfoJson info, HttpServletRequest request, HttpServletResponse response) throws Exception {
        Long storeId = validateAuthorization(authorization);
        String encryptedPassword = Encrypter.getEncryptedPassword(info.getPassword());
        if (!"d89bcea040f4a57df96dd3cc86fd8ca1".equals(encryptedPassword)) {
            throw new ForbidenException();
        }
        DateTime expirationDate = paymentService.renewExpirationDate(storeId);

        return new ResponseEntity<>(new SystemInfoJson(expirationDate.toString("dd/MM/YYYY"), true), HttpStatus.OK);
    }

    @RequestMapping(value = "pagSeguro/notification", method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity<PagSeguroNotificationJson> pagSeguroNotification(@ModelAttribute PagSeguroNotificationJson pagSeguroNotification) {
        //TODO Ajustar método para receber as notificações do PagSeguro
        LOG.info("getNotificationCode: " + pagSeguroNotification.getNotificationCode());
        LOG.info("getNotificationType: " + pagSeguroNotification.getNotificationType());

        return new ResponseEntity<>(pagSeguroNotification, HttpStatus.OK);
    }
}
