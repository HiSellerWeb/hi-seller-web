package br.com.checkout.controller;

import br.com.checkout.config.ConfigEnum;
import br.com.checkout.domain.Cart;
import br.com.checkout.domain.PurchaseOrder;
import br.com.checkout.model.PaymentTypeEnum;
import br.com.checkout.service.CartService;
import br.com.checkout.service.FreightService;
import br.com.checkout.service.PaymentService;
import br.com.checkout.service.PurchaseOrderService;
import br.com.checkout.validator.CartValidator;
import br.com.commons.appConfig.AppConfig;
import br.com.commons.controller.AbstractController;
import br.com.commons.exception.ConflictException;
import br.com.commons.exception.ForbidenException;
import br.com.commons.exception.InternalServerException;
import br.com.commons.exception.PreconditionFailedException;
import br.com.commons.json.AddressJson;
import br.com.commons.json.AuthenticationTokenJson;
import br.com.commons.json.CartJson;
import br.com.commons.json.CustomerJson;
import br.com.commons.json.FreighJson;
import br.com.commons.json.PaymentJson;
import br.com.commons.json.ProductCartListJson;
import br.com.commons.json.ProductMovJson;
import br.com.commons.json.SKUJson;
import br.com.commons.json.SkuCartJson;
import br.com.commons.json.SystemInfoJson;
import br.com.commons.security.Encrypter;
import br.com.commons.service.CustomerAPIService;
import br.com.uol.pagseguro.domain.Item;
import br.com.uol.pagseguro.domain.Phone;
import br.com.uol.pagseguro.domain.checkout.Checkout;
import br.com.uol.pagseguro.enums.Currency;
import br.com.uol.pagseguro.enums.DocumentType;
import br.com.uol.pagseguro.enums.ShippingType;
import br.com.uol.pagseguro.properties.PagSeguroConfig;
import java.math.BigDecimal;
import java.math.RoundingMode;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "/cart")
public class CartController extends AbstractController {

    private static final Log LOG = LogFactory.getLog(CartController.class);

    @Autowired
    private CartService cartService;

    @Autowired
    private FreightService freightService;

    @Autowired
    private PurchaseOrderService purchaseOrderService;

    @Autowired
    private PaymentService paymentService;

    @Autowired
    private CartValidator cartValidator;

    @Autowired
    private CustomerAPIService customerAPIService;

    @Autowired
    private AppConfig appConfig;

    @RequestMapping(value = "/{cartId}", method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<CartJson> getCart(@RequestHeader(value = "Authorization") String authorization, @PathVariable("cartId") String cartId, HttpServletRequest request, HttpServletResponse response) throws Exception {
        validateAuthorization(authorization);
        Cart cart = cartService.verify(Long.valueOf(Encrypter.decrypt(cartId)));
        return new ResponseEntity<>(cart.toJson(), HttpStatus.OK);
    }

    @RequestMapping(value = "/{cartId}/fullCart", method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<CartJson> getFullCart(@RequestHeader(value = "Authorization") String authorization, @PathVariable("cartId") String cartId, HttpServletRequest request, HttpServletResponse response) throws Exception {
        Long storeId = validateAuthorization(authorization);
        Cart cart = cartService.verify(Long.valueOf(Encrypter.decrypt(cartId)));
        ProductCartListJson cartListJson = new ProductCartListJson();
        for (SkuCartJson p : cart.getObjectProducts()) {
            ResponseEntity<SKUJson> getForEntity = getForEntity(appConfig.getValue(storeId, ConfigEnum.STOCK_ENDPOINT) + "sku/" + p.getId(), authorization, SKUJson.class);
            SKUJson skuJson = getForEntity.getBody();
            p.setName(skuJson.getProduct().getName() + " " + skuJson.getName());
            p.setPrice(skuJson.getPrice());
            p.setWeight(skuJson.getWeight());
            cartListJson.add(p);
        }
        cart.setProductsFromObjetc(cartListJson);
        cartService.update(cart);
        return new ResponseEntity<>(cart.toJson(), HttpStatus.OK);
    }

    @SuppressWarnings("unchecked")
    @RequestMapping(method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity<CartJson> create(@RequestHeader(value = "Authorization") String authorization, @RequestBody CartJson cartJson, HttpServletRequest request, HttpServletResponse response) throws Exception {
        Long storeId = validateAuthorization(authorization);
        cartJson.setStoreId(storeId);
        cartValidator.validate(cartJson);

        cartJson = cartService.create(new Cart(cartJson)).toJson();

        return new ResponseEntity<>(cartJson, HttpStatus.CREATED);
    }

    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/{cartId}", method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity<ProductMovJson> addToCart(@RequestHeader(value = "Authorization") String authorization, @PathVariable("cartId") String cartId, @RequestBody ProductMovJson productMovJson, HttpServletRequest request, HttpServletResponse response) throws Exception {
        Long storeId = validateAuthorization(authorization);
        //Verifica se o protudo ainda está licenciado para uso
        SystemInfoJson systemInfoJson = paymentService.getExpirationDate(storeId);
        if (!systemInfoJson.isIsValid()) {
            throw new ForbidenException();
        }

        //verifica se o carrinho existe e as variáveis expired e finished
        Cart cart = cartService.verify(Long.valueOf(Encrypter.decrypt(cartId)));
        SkuCartJson product = cart.getProduct(productMovJson.getId());

        if (product != null) {
            throw new ConflictException();
        }

        //Chama o serviço do stock para reservar o produto
        ResponseEntity<ProductMovJson> postForEntity = postForEntity(appConfig.getValue(storeId, ConfigEnum.STOCK_ENDPOINT) + "sku/stockToSession",
                authorization, productMovJson, ProductMovJson.class);

        productMovJson = postForEntity.getBody();

        if (productMovJson.getQuantity() == 0) {
            throw new PreconditionFailedException("Produto sem estoque disponível no momento.");
        }

        //Adiciona o produto o produto (item ou quantidade)
        cart.addProduct(productMovJson);

        //Atualiza o carrinho com os dados completos
        cartService.update(cart);

        return new ResponseEntity<>(productMovJson, HttpStatus.OK);
    }

    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/{cartId}", method = RequestMethod.PUT)
    public @ResponseBody
    ResponseEntity<ProductMovJson> updadeQuantity(@RequestHeader(value = "Authorization") String authorization, @PathVariable("cartId") String cartId, @RequestBody ProductMovJson productMovJson, HttpServletRequest request, HttpServletResponse response) throws Exception {
        Long storeId = validateAuthorization(authorization);
        //verifica se o carrinho existe e as variáveis expired e finished
        Cart cart = cartService.verify(Long.valueOf(Encrypter.decrypt(cartId)));
        SkuCartJson product = cart.getProduct(productMovJson.getId());

        if (productMovJson.getQuantity() > 20) {
            productMovJson.setQuantity(20);
        }

        productMovJson.setQuantity(productMovJson.getQuantity() - product.getQuantity());

        //Chama o serviço do stock para reservar o produto
        ResponseEntity<ProductMovJson> postForEntity = postForEntity(appConfig.getValue(storeId, ConfigEnum.STOCK_ENDPOINT) + "sku/stockToSession",
                authorization, productMovJson, ProductMovJson.class);

        productMovJson = postForEntity.getBody();

        //Adiciona o produto (item ou quantidade)
        cart.addProduct(productMovJson);

        //Atualiza o carrinho com os dados completos
        cartService.update(cart);

        return new ResponseEntity<>(productMovJson, HttpStatus.OK);
    }

    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/{cartId}/customer/", method = RequestMethod.PUT)
    public @ResponseBody
    ResponseEntity<CartJson> updadeCustomer(@RequestHeader(value = "Authorization") String authorization, @PathVariable("cartId") String cartId, @RequestBody AuthenticationTokenJson authenticationTokenJson, HttpServletRequest request, HttpServletResponse response) throws Exception {
        Long storeId = validateAuthorization(authorization);
        //verifica se o carrinho existe e as variáveis expired e finished
        Cart cart = cartService.verify(Long.valueOf(Encrypter.decrypt(cartId)));

        ResponseEntity<CustomerJson> entity = getForEntity(appConfig.getValue(storeId, ConfigEnum.ACCOUNT_ENDPOINT) + "/customer/" + authenticationTokenJson.getAuthenticationToken(),
                authorization, CustomerJson.class);
        CustomerJson customerJson = entity.getBody();

        cart.setCustomerId(customerJson.getId());
        cart = cartService.update(cart);

        return new ResponseEntity<>(cart.toJson(), HttpStatus.OK);
    }

    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/{cartId}/tiketPayment", method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity<PaymentJson> tiketPayment(@RequestHeader(value = "Authorization") String authorization, @PathVariable("cartId") String cartId, @RequestBody PaymentJson paymantJson, HttpServletRequest request, HttpServletResponse response) throws Exception {
        Long storeId = validateAuthorization(authorization);
        //verifica se o carrinho existe e as variáveis expired e finished
        Cart cart = cartService.verify(Long.valueOf(Encrypter.decrypt(cartId)));

        ResponseEntity<CustomerJson> entity = getForEntity(appConfig.getValue(storeId, ConfigEnum.ACCOUNT_ENDPOINT) + "/customer/" + paymantJson.getAuthenticationToken(),
                authorization, CustomerJson.class);
        CustomerJson customerJson = entity.getBody();

        cart.setCustomerId(customerJson.getId());
        FreighJson freighJson = freightService.freightCost(storeId, authorization, paymantJson.getAddress().getZipCode(), cart.getObjectProducts());
        cart.setFreightCost(BigDecimal.valueOf(freighJson.getPac()));
        cart = cartService.update(cart);

        PurchaseOrder order = new PurchaseOrder();
        order.setCart(cart);
        order.setCustomerId(cart.getCustomerId());
        order.setPaymentType(PaymentTypeEnum.TICKET);
        order.setSellerId(cart.getSellerId());
        order.setDeliveryAddressJson(paymantJson.getAddress());
        order.setStoreId(storeId);
        order = purchaseOrderService.create(order);

        try {
            String ticketPath = paymentService.generateTicket(storeId, order, customerJson);
            order.setTransaction(ticketPath);
            paymantJson.setAuthenticationToken(ticketPath);
            purchaseOrderService.update(order);

            cart.setFinished(true);
            cart = cartService.update(cart);

            for (SkuCartJson productCartJson : cart.getObjectProducts()) {
                //Movimenta o produto no estoque
                postForEntity(appConfig.getValue(storeId, ConfigEnum.STOCK_ENDPOINT) + "sku/sessionToOrder",
                        authorization, new ProductMovJson(productCartJson.getId(), productCartJson.getQuantity()), ProductMovJson.class);
            }

        } catch (Exception e) {
            if (order.getId() != null) {
                purchaseOrderService.delete(order);
                cart.setFinished(false);
                cartService.update(cart);
            }
            throw new InternalServerException();
        }

        paymantJson = new PaymentJson();
        paymantJson.setOrderToken(String.valueOf(order.getId()));

        return new ResponseEntity<>(paymantJson, HttpStatus.OK);
    }

    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/{order}/viewTiket", method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<byte[]> viewTiket(@RequestHeader(value = "Authorization") String authorization, @PathVariable("tiket") String order, HttpServletRequest request, HttpServletResponse response) throws Exception {
        Long storeId = validateAuthorization(authorization);
        PurchaseOrder purchaseOrder = purchaseOrderService.findById(Long.valueOf(order));
        byte[] ticket = paymentService.getTicket(storeId, purchaseOrder);

        return new ResponseEntity<>(ticket, HttpStatus.OK);
    }

    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/{cartId}/pagSeguroPayment", method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity<PaymentJson> pagSeguroPayment(@RequestHeader(value = "Authorization") String authorization, @PathVariable("cartId") String cartId, @RequestBody PaymentJson paymantJson, HttpServletRequest request, HttpServletResponse response) throws Exception {
        Long storeId = validateAuthorization(authorization);
        //verifica se o carrinho existe e as variáveis expired e finished
        Cart cart = cartService.verify(Long.valueOf(Encrypter.decrypt(cartId)));

        CustomerJson customerJson = customerAPIService.findByToken(appConfig.getValue(storeId, ConfigEnum.ACCOUNT_ENDPOINT), authorization, paymantJson.getAuthenticationToken());
        if (customerJson == null) {
            throw new ForbidenException("Usuário não localizado.");
        }

        cart.setCustomerId(customerJson.getId());
        FreighJson freighJson = freightService.freightCost(storeId, authorization, paymantJson.getAddress().getZipCode(), cart.getObjectProducts());
        cart.setFreightCost(BigDecimal.valueOf(freighJson.getPac()));
        cart = cartService.update(cart);

        PurchaseOrder order = new PurchaseOrder();
        order.setCart(cart);
        order.setCustomerId(cart.getCustomerId());
        order.setPaymentType(PaymentTypeEnum.PAG_SEGURO);
        order.setSellerId(cart.getSellerId());
        order.setDeliveryAddressJson(paymantJson.getAddress());
        order = purchaseOrderService.create(order);

        String checkoutURL = "";

        try {
            Checkout checkout = new Checkout();

            for (SkuCartJson productCartJson : order.getCart().getObjectProducts()) {
                Item item = new Item();
                item.setAmount(productCartJson.getPrice());
                item.setCode(productCartJson.getCode());
                item.setId(productCartJson.getId());
                item.setQuantity(productCartJson.getQuantity());
                item.setUrlImage(productCartJson.getImgUrl());
                item.setDescription(productCartJson.getName());
                item.setWeight(productCartJson.getWeight());
                checkout.addItem(item);
            }
            AddressJson deliveryAddress = order.getObjectDeliveryAddress();
            checkout.setShippingAddress("BRA",
                    deliveryAddress.getUf(),
                    deliveryAddress.getCity(),
                    deliveryAddress.getNeighborhood(),
                    deliveryAddress.getZipCode(),
                    deliveryAddress.getStreet(),
                    deliveryAddress.getNumber(),
                    deliveryAddress.getComplement());

            checkout.setShippingType(ShippingType.PAC);

            checkout.setShippingCost(order.getCart().getFreightCost().setScale(2, RoundingMode.CEILING));

            checkout.setSender(customerJson.getName(), customerJson.getEmail(), new Phone(), DocumentType.CPF, customerJson.getCpf());

            checkout.setCurrency(Currency.BRL);

            checkout.setReference(order.getId().toString());

            checkout.setNotificationURL(appConfig.getValue(storeId, ConfigEnum.ENDPOINT) + appConfig.getValue(storeId, ConfigEnum.PAGSEGURO_NOTIFICATION_URL));

            checkout.setRedirectURL(appConfig.getValue(storeId, ConfigEnum.FRONT_ENDPOINT) + "pedido/" + order.getId());

            Boolean onlyCheckoutCode = false;

            checkoutURL = checkout.register(PagSeguroConfig.getAccountCredentials(), onlyCheckoutCode);
            String code = checkoutURL.split("code=")[1];

            order.setTransaction(code);
            purchaseOrderService.update(order);

            cart.setFinished(true);
            cart = cartService.update(cart);

            for (SkuCartJson productCartJson : cart.getObjectProducts()) {
                //Movimenta o produto no estoque
                postForEntity(appConfig.getValue(storeId, ConfigEnum.STOCK_ENDPOINT) + "sku/sessionToOrder",
                        authorization, new ProductMovJson(productCartJson.getId(), productCartJson.getQuantity()), ProductMovJson.class);
            }

        } catch (Exception e) {
            if (order.getId() != null) {
                purchaseOrderService.delete(order);
                cart.setFinished(false);
                cartService.update(cart);
            }
            throw new InternalServerException();
        }

        paymantJson = new PaymentJson();
        paymantJson.setOrderToken(String.valueOf(order.getId()));
        paymantJson.setPagSeguroCheckoutURL(checkoutURL);

        return new ResponseEntity<>(paymantJson, HttpStatus.OK);
    }
}
