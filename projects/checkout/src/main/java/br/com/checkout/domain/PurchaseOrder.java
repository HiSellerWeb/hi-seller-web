package br.com.checkout.domain;

import br.com.checkout.model.PaymentTypeEnum;
import br.com.commons.json.AddressJson;
import br.com.commons.json.Json;
import br.com.commons.json.OrderJson;
import br.com.uol.pagseguro.enums.TransactionStatus;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Index;
import org.joda.time.DateTime;

@Entity
@Table(name = "purchaseOrder")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class PurchaseOrder implements Serializable {

    private static final long serialVersionUID = 1109826923361899489L;

    @Id
    @SequenceGenerator(name = "order_id", sequenceName = "order_id_seg")
    @GeneratedValue(generator = "order_id", strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "storeId", nullable = false, updatable = false)
    private Long storeId;

    @Column(name = "customerId", nullable = false)
    @Index(name = "order_customerid")
    private Long customerId;

    @Column(name = "sellerId", nullable = false)
    @Index(name = "order_sellerid")
    private Long sellerId;

    @Column(name = "deliveryAddress", nullable = false, columnDefinition = "TEXT")
    private String deliveryAddress = "";

    @Column(name = "transaction", nullable = false, columnDefinition = "varchar(255)")
    private String transaction = "";

    @Column(name = "pagSeguroCode", columnDefinition = "varchar(255)")
    private String pagSeguroCode = "";

    @Column(name = "paymentType", nullable = false)
    @Enumerated(EnumType.STRING)
    private PaymentTypeEnum paymentType;

    @Column(name = "dtOrder", nullable = false)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date dtOrder = DateTime.now().toDate();

    @Column(name = "paid", nullable = false)
    private boolean paid = false;

    @Column(name = "dtPaid")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date dtPaid;

    @Column(name = "packaged", nullable = false)
    private boolean packaged = false;

    @Column(name = "dtPackaged")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date dtPackaged;

    @Column(name = "sent", nullable = false)
    private boolean sent = false;

    @Column(name = "dtSent")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date dtSent;

    @Column(name = "shippingCode")
    private String shippingCode;

    @Column(name = "delivered", nullable = false)
    private boolean delivered = false;

    @Column(name = "dtDelivered")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date dtDelivered;

    @Column(name = "dtPaymentForecast")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date dtPaymentForecast;

    @Column(name = "canceled", nullable = false)
    private boolean canceled = false;

    @Column(name = "dtCanceled")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date dtCanceled;

    @Column(name = "dtLastEvent")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date dtLastEvent;

    @Column(name = "pagSeguroStatus")
    @Enumerated(EnumType.STRING)
    private TransactionStatus pagSeguroStatus = TransactionStatus.UNKNOWN_STATUS;

    @OneToOne(cascade = CascadeType.REFRESH, optional = false, fetch = FetchType.EAGER, orphanRemoval = false)
    @JoinColumn(name = "cart", nullable = false)
    private Cart cart;

    public PurchaseOrder() {
    }

    public OrderJson toJson() throws Exception {
        OrderJson orderJson = new OrderJson();
        orderJson.setStoreId(getStoreId());
        orderJson.setCart(getCart().toJson());
        orderJson.setCustomerId(getCustomerId());
        orderJson.setDeliveryAddress(Json.toObject(getDeliveryAddress(), AddressJson.class));
        orderJson.setId(getId());
        orderJson.setPaymentType(getPaymentType().getName());
        orderJson.setSellerId(getSellerId());
        orderJson.setTransaction(getTransaction());
        orderJson.setPaid(isPaid());
        orderJson.setPackaged(isPackaged());
        orderJson.setDelivered(isDelivered());
        orderJson.setDtDelivered(getDtDelivered());
        orderJson.setDtOrder(getDtOrder());
        orderJson.setDtPackaged(getDtPackaged());
        orderJson.setDtPaid(getDtPaid());
        orderJson.setSent(isSent());
        orderJson.setDtSent(getDtSent());
        orderJson.setShippingCode(getShippingCode());
        orderJson.setDtPaymentForecast(getDtPaymentForecast());
        orderJson.setCanceled(isCanceled());
        orderJson.setDtCanceled(getDtCanceled());
        orderJson.setPagSeguroStatus(getPagSeguroStatus().toString());
        orderJson.setPagSeguroCode(getPagSeguroCode());

        return orderJson;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public Cart getCart() {
        return cart;
    }

    public void setCart(Cart cart) {
        this.cart = cart;
    }

    public AddressJson getObjectDeliveryAddress() {
        return Json.toObject(this.deliveryAddress, AddressJson.class);
    }

    public String getDeliveryAddress() {
        return this.deliveryAddress;
    }

    public void setDeliveryAddressJson(AddressJson addressJson) {
        this.deliveryAddress = Json.toJson(addressJson);
    }

    public void setDeliveryAddress(String deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public boolean isPaid() {
        return paid;
    }

    public void setPaid(boolean paid) {
        this.paid = paid;
    }

    public PaymentTypeEnum getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(PaymentTypeEnum paymentType) {
        this.paymentType = paymentType;
    }

    public String getTransaction() {
        return transaction;
    }

    public void setTransaction(String transaction) {
        this.transaction = transaction;
    }

    public boolean isPackaged() {
        return packaged;
    }

    public void setPackaged(boolean packaged) {
        this.packaged = packaged;
    }

    public boolean isSent() {
        return sent;
    }

    public void setSent(boolean sent) {
        this.sent = sent;
    }

    public boolean isDelivered() {
        return delivered;
    }

    public void setDelivered(boolean delivered) {
        this.delivered = delivered;
    }

    public Date getDtOrder() {
        return dtOrder;
    }

    public void setDtOrder(Date dtOrder) {
        this.dtOrder = dtOrder;
    }

    public Date getDtPaid() {
        return dtPaid;
    }

    public void setDtPaid(Date dtPaid) {
        this.dtPaid = dtPaid;
    }

    public Date getDtPackaged() {
        return dtPackaged;
    }

    public void setDtPackaged(Date dtPackaged) {
        this.dtPackaged = dtPackaged;
    }

    public Date getDtSent() {
        return dtSent;
    }

    public void setDtSent(Date dtSent) {
        this.dtSent = dtSent;
    }

    public Date getDtDelivered() {
        return dtDelivered;
    }

    public void setDtDelivered(Date dtDelivered) {
        this.dtDelivered = dtDelivered;
    }

    public String getShippingCode() {
        return shippingCode;
    }

    public void setShippingCode(String shippingCode) {
        this.shippingCode = shippingCode;
    }

    public String getPagSeguroCode() {
        return pagSeguroCode;
    }

    public void setPagSeguroCode(String pagSeguroCode) {
        this.pagSeguroCode = pagSeguroCode;
    }

    public Date getDtPaymentForecast() {
        return dtPaymentForecast;
    }

    public void setDtPaymentForecast(Date dtPaymentForecast) {
        this.dtPaymentForecast = dtPaymentForecast;
    }

    public Date getDtLastEvent() {
        return dtLastEvent;
    }

    public void setDtLastEvent(Date dtLastEvent) {
        this.dtLastEvent = dtLastEvent;
    }

    public boolean isCanceled() {
        return canceled;
    }

    public void setCanceled(boolean canceled) {
        this.canceled = canceled;
    }

    public Date getDtCanceled() {
        return dtCanceled;
    }

    public void setDtCanceled(Date dtCanceled) {
        this.dtCanceled = dtCanceled;
    }

    public TransactionStatus getPagSeguroStatus() {
        if (pagSeguroStatus == null) {
            return TransactionStatus.UNKNOWN_STATUS;
        }
        return pagSeguroStatus;
    }

    public void setPagSeguroStatus(TransactionStatus pagSeguroStatus) {
        this.pagSeguroStatus = pagSeguroStatus;
    }

    public Long getStoreId() {
        return storeId;
    }

    public void setStoreId(Long storeId) {
        this.storeId = storeId;
    }
}
