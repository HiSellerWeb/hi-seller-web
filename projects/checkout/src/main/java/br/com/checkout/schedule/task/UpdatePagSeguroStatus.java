package br.com.checkout.schedule.task;

import br.com.checkout.service.PurchaseOrderService;
import br.com.uol.pagseguro.domain.Credentials;
import java.util.TimerTask;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class UpdatePagSeguroStatus extends TimerTask {

    private static final Log LOG = LogFactory.getLog(UpdatePagSeguroStatus.class);

    private final PurchaseOrderService orderService;
    private final Credentials credentials;

    public UpdatePagSeguroStatus(PurchaseOrderService orderService, Credentials credentials) {
        this.orderService = orderService;
        this.credentials = credentials;
    }

    @Override
    public void run() {
        try {
            orderService.updatePagSeguroStatus(credentials, 16);
        } catch (Exception e) {
            LOG.error("Ocorreu um erro ao atualizar os status do PagSeguro", e);
        }
    }
}
