package br.com.checkout.dao;

import br.com.checkout.domain.PurchaseOrder;
import br.com.commons.dao.AbstractDao;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.joda.time.DateTime;

public class PurchaseOrderDao extends AbstractDao<PurchaseOrder> {

    @Override
    public PurchaseOrder save(PurchaseOrder purchaseOrder) {
        purchaseOrder.setId(null);
        purchaseOrder.setDtLastEvent(DateTime.now().toDate());
        return super.save(purchaseOrder);
    }

    public PurchaseOrder findById(Long id) {
        return super.findById(PurchaseOrder.class, id);
    }

    @Override
    public PurchaseOrder update(PurchaseOrder purchaseOrder) {
        purchaseOrder.setDtLastEvent(DateTime.now().toDate());
        return super.update(purchaseOrder);
    }

    public List<PurchaseOrder> findAllByCustomerId(Long storeId, Long id) {
        List<Criterion> restrictions = new ArrayList<>();
        restrictions.add(Restrictions.eq("storeId", storeId));
        restrictions.add(Restrictions.eq("customerId", id));

        List<PurchaseOrder> orders = super.findByRestrictions(PurchaseOrder.class, restrictions);

        return orders;
    }

    public List<PurchaseOrder> findAllBySellerId(Long storeId, Long id) {
        List<Criterion> restrictions = new ArrayList<>();
        restrictions.add(Restrictions.eq("storeId", storeId));
        restrictions.add(Restrictions.eq("sellerId", id));

        List<PurchaseOrder> orders = super.findByRestrictions(PurchaseOrder.class, restrictions, org.hibernate.criterion.Order.asc("dtPaid"));

        return orders;
    }

    public List<PurchaseOrder> findAllBySellerId(Long storeId, Long id, Date startDate, Date endDate) {
        List<Criterion> restrictions = new ArrayList<>();
        restrictions.add(Restrictions.eq("sellerId", id));
        restrictions.add(Restrictions.between("dtPaid", startDate, endDate));

        List<PurchaseOrder> orders = super.findByRestrictions(PurchaseOrder.class, restrictions, org.hibernate.criterion.Order.asc("dtPaid"));

        return orders;
    }

    public List<PurchaseOrder> findAllByStatus(Long storeId, boolean paid, boolean packaged, boolean sent, boolean delivered, String orderBy, String direction) {
        List<Criterion> restrictions = new ArrayList<>();
        restrictions.add(Restrictions.eq("storeId", storeId));
        restrictions.add(Restrictions.eq("paid", paid));
        restrictions.add(Restrictions.eq("packaged", packaged));
        restrictions.add(Restrictions.eq("sent", sent));
        restrictions.add(Restrictions.eq("delivered", delivered));

        org.hibernate.criterion.Order order = super.getOrder(orderBy, direction);

        return super.findByRestrictions(PurchaseOrder.class, restrictions, order);
    }

    @SuppressWarnings("unchecked")
    public List<Long> findAllGrupBySeller(Long storeId, Date startDate, Date endDate) {
        return session().createCriteria(PurchaseOrder.class)
                .add(Restrictions.between("dtPaid", startDate, endDate))
                .add(Restrictions.eq("storeId", storeId))
                .setProjection(Projections.projectionList()
                        .add(Projections.groupProperty("sellerId"))
                ).list();
    }
}
