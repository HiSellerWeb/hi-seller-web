package br.com.checkout.domain;

import br.com.commons.json.CartJson;
import br.com.commons.json.Json;
import br.com.commons.json.ProductCartListJson;
import br.com.commons.json.ProductMovJson;
import br.com.commons.json.SkuCartJson;
import br.com.commons.security.Encrypter;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Index;
import org.joda.time.DateTime;

@Entity
@Table(name = "cart")
@Cache(usage = CacheConcurrencyStrategy.NONE)
public class Cart implements Serializable {

    private static final long serialVersionUID = 30812093810L;

    @Id
    @SequenceGenerator(name = "cart_id", sequenceName = "cart_id_seg")
    @GeneratedValue(generator = "cart_id", strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "storeId", nullable = false, updatable = false)
    private Long storeId;

    @Column(name = "customerId")
    @Index(name = "cart_customerid")
    private Long customerId;

    @Column(name = "sellerId")
    @Index(name = "cart_sellerid")
    private Long sellerId;

    @Column(name = "products", columnDefinition = "TEXT")
    private String products = "";

    @Column(name = "freightCost", precision = 5, scale = 2)
    private BigDecimal freightCost = BigDecimal.ZERO;

    @Column(name = "dtStart", nullable = false)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date dtStart = DateTime.now().toDate();

    @Column(name = "dtToExpire", nullable = false)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date dtToExpire;

    @Column(name = "dtExpired")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date dtExpired;

    @Column(name = "finished", nullable = false)
    private boolean finished = false;

    @Column(name = "expired", nullable = false)
    private boolean expired = false;

    public Cart() {
    }

    public Cart(CartJson cartJson) {
        if (cartJson != null) {
            this.id = cartJson.getId();
            this.storeId = cartJson.getStoreId();
            this.customerId = cartJson.getCostumerId();
            this.sellerId = cartJson.getSellerId();
        }
    }

    public CartJson toJson() throws Exception {
        CartJson cartJson = new CartJson();
        cartJson.setStoreId(getStoreId());
        cartJson.setEncryptId(getId());
        cartJson.setCustomerId(getCustomerId());
        cartJson.setProducts(getObjectProducts());
        cartJson.setFinished(isFinished());
        cartJson.setExpired(isExpired());
        cartJson.setSellerId(getSellerId());
        cartJson.setFreightCost(Double.valueOf(getFreightCost().toString()));

        return cartJson;
    }

    public SkuCartJson getProduct(Long id) throws Exception {
        ProductCartListJson productsJson = Json.toObject(getProducts(), ProductCartListJson.class);
        if (productsJson == null) {
            return null;
        }
        for (SkuCartJson product : productsJson) {
            if (product.getId().equals(id)) {
                return product;
            }
        }
        return null;
    }

    public void addProduct(ProductMovJson productMovJson) throws Exception {
        ProductCartListJson productsJson = Json.toObject(getProducts(), ProductCartListJson.class);
        if (productsJson == null) {
            productsJson = new ProductCartListJson();
        }
        boolean containProduct = false;
        ProductCartListJson newProductList = new ProductCartListJson();
        for (SkuCartJson product : productsJson) {
            if (product.getId().equals(productMovJson.getId())) {
                product.setQuantity(product.getQuantity() + productMovJson.getQuantity());
                containProduct = true;
                if (product.getQuantity() == 0) {
                    continue;
                }
            }
            newProductList.add(product);
        }
        if (!containProduct && productMovJson.getQuantity() > 0) {
            SkuCartJson product = new SkuCartJson(productMovJson);
            newProductList.add(product);
        }

        setProducts(Json.toJson(newProductList));
    }

    public BigDecimal getProductsValue() throws Exception {
        BigDecimal value = new BigDecimal(0.00);
        for (SkuCartJson item : getObjectProducts()) {
            try {
                value = value.add(item.getPrice().multiply(BigDecimal.valueOf(item.getQuantity())));
            } catch (Exception e) {
            }
        }
        return value;
    }

    public BigDecimal getDiscountTicket() {
        return BigDecimal.ZERO;
//        return getProductsValue().multiply(BigDecimal.valueOf(10)).divide(BigDecimal.valueOf(100));
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public Date getDtStart() {
        return dtStart;
    }

    public void setDtStart(Date dtStart) {
        this.dtStart = dtStart;
    }

    public Date getDtExpired() {
        return dtExpired;
    }

    public void setDtExpired(Date dtExpired) {
        this.dtExpired = dtExpired;
    }

    public boolean isFinished() {
        return finished;
    }

    public void setFinished(boolean finished) {
        this.finished = finished;
    }

    public boolean isExpired() {
        return expired;
    }

    public void setExpired(boolean expired) {
        this.expired = expired;
    }

    public Date getDtToExpire() {
        return dtToExpire;
    }

    public void setDtToExpire(Date dtToExpire) {
        this.dtToExpire = dtToExpire;
    }

    public String getProducts() throws Exception {
        return Encrypter.decrypt(products);
    }

    public ProductCartListJson getObjectProducts() throws Exception {
        return Json.toObject(getProducts(), ProductCartListJson.class);
    }

    public void setProducts(String products) throws Exception {
        this.products = Encrypter.encrypt(products);
    }

    public void setProductsFromObjetc(ProductCartListJson productCartListJson) throws Exception {
        setProducts(Json.toJson(productCartListJson));
    }

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public BigDecimal getFreightCost() {
        return freightCost;
    }

    public void setFreightCost(BigDecimal freightCost) {
        this.freightCost = freightCost;
    }

    public Long getStoreId() {
        return storeId;
    }

    public void setStoreId(Long storeId) {
        this.storeId = storeId;
    }
}
