package br.com.checkout.service;

import br.com.checkout.config.ConfigEnum;
import br.com.commons.json.FreighJson;
import br.com.commons.json.SkuCartJson;
import br.com.commons.service.AbstractService;
import java.math.BigDecimal;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.http.ResponseEntity;

public class FreightService extends AbstractService {

    static final Logger LOG = Logger.getLogger(FreightService.class);

    public FreighJson freightCost(Long storeId, String authorization, String zipCode, Double value, Long weight) {
        FreighJson freighJson = new FreighJson();
//        if (weight.equals(0L)) {
//            return freighJson;
//        }
        String kg = String.valueOf(Double.valueOf(weight) / 1000).replace('.', ',');

        ResponseEntity<String> getForEntity = getForEntity("https://pagseguro.uol.com.br/desenvolvedor/simulador_de_frete_calcular.jhtml?postalCodeFrom="
                + appConfig.getValue(storeId, ConfigEnum.SHOP_ZIP_CODE) + "&weight=" + kg + "&value=" + value + "&postalCodeTo=" + zipCode, authorization, String.class);
        String response = getForEntity.getBody();
        String[] splitedResponse = response.split("\\|");
        if (splitedResponse != null
                && splitedResponse.length >= 5) {
            freighJson.setPac(Double.valueOf(splitedResponse[4].trim()));
            return freighJson;
        }
        return freighJson;
    }

    public FreighJson freightCost(Long storeId, String authorization, String zipCode, List<SkuCartJson> products) {
        BigDecimal totalPrice = BigDecimal.ZERO;
        Long totalWeight = 0L;
        FreighJson freighJson = new FreighJson();
        if (products != null) {

            for (SkuCartJson product : products) {
//                totalPrice.add(product.getPrice()  * product.getQuantity()); // No momento não será informado o valor do produto, pois está aumentando em muito o custo do frete
                totalWeight += product.getWeight() * product.getQuantity();
            }
            freighJson = freightCost(storeId, authorization, zipCode, Double.valueOf(totalPrice.toString()), totalWeight);
        }

        return freighJson;
    }

}
