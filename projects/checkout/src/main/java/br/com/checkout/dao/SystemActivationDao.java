package br.com.checkout.dao;

import br.com.checkout.domain.SystemActivation;
import br.com.commons.dao.AbstractDao;
import br.com.commons.exception.ForbidenException;
import org.joda.time.DateTime;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public class SystemActivationDao extends AbstractDao<SystemActivation> {

    public SystemActivation getSystemActivation(Long storeId) throws ForbidenException {
        SystemActivation systemActivation = super.findById(SystemActivation.class, storeId);

        if (systemActivation == null) {
            throw new ForbidenException("Licenciamento do sistema não encontrado");
        }

        return systemActivation;
    }

    @Transactional(propagation = Propagation.NEVER)
    public DateTime renewSystemActivation(Long storeId) throws ForbidenException, Exception {
        SystemActivation systemActivation = super.findById(SystemActivation.class, storeId);

        if (systemActivation == null) {
            systemActivation = new SystemActivation();
            systemActivation.setId(storeId);
            DateTime expirationDate = new DateTime();
            expirationDate.plusMonths(1);
            expirationDate.plusWeeks(2);
            systemActivation.setDateTimeExpireDate(expirationDate);
            super.save(systemActivation);
        }

        systemActivation = super.findById(SystemActivation.class, storeId);

        if (systemActivation == null) {
            throw new ForbidenException("Licenciamento do sistema alterado indevidamente");
        }

        DateTime expirationDate = new DateTime();
        expirationDate = expirationDate.plusMonths(1);
        expirationDate = expirationDate.plusWeeks(2);
        systemActivation.setDateTimeExpireDate(expirationDate);

        systemActivation = super.update(systemActivation);
        return systemActivation.expireDateToDateTime();
    }
}
