package br.com.checkout.service;

import br.com.checkout.config.ConfigEnum;
import br.com.checkout.dao.PurchaseOrderDao;
import br.com.checkout.domain.PurchaseOrder;
import br.com.commons.exception.ServiceUnavailableException;
import br.com.commons.json.OrderJson;
import br.com.commons.json.SellerJson;
import br.com.commons.security.Authentication;
import br.com.commons.security.AuthenticationRole;
import br.com.commons.service.AbstractService;
import br.com.uol.pagseguro.domain.Credentials;
import br.com.uol.pagseguro.domain.TransactionSearchResult;
import br.com.uol.pagseguro.domain.TransactionSummary;
import br.com.uol.pagseguro.enums.TransactionStatus;
import br.com.uol.pagseguro.exception.PagSeguroServiceException;
import br.com.uol.pagseguro.properties.PagSeguroConfig;
import br.com.uol.pagseguro.service.TransactionSearchService;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;

public class PurchaseOrderService extends AbstractService {

    static final Logger LOG = Logger.getLogger(PurchaseOrderService.class);

    @Autowired
    private PurchaseOrderDao orderDao;

    public PurchaseOrder create(PurchaseOrder purchaseOrder) throws Exception {
        purchaseOrder.setId(null);
        return orderDao.save(purchaseOrder);
    }

    public PurchaseOrder update(PurchaseOrder purchaseOrder) {
        return orderDao.update(purchaseOrder);
    }

    public void delete(PurchaseOrder order) {
        orderDao.delete(order);
    }

    public PurchaseOrder findById(Long id) {
        return orderDao.findById(id);
    }

    public List<PurchaseOrder> findAllByCustomerId(Long storeId, Long id) {
        return orderDao.findAllByCustomerId(storeId, id);
    }

    public List<PurchaseOrder> findAllBySellerId(Long storeId, Long id) {
        return orderDao.findAllBySellerId(storeId, id);
    }

    public List<PurchaseOrder> findAllBySellerId(Long storeId, Long id, Date dtStart, Date dtEnd) {
        return orderDao.findAllBySellerId(storeId, id, dtStart, dtEnd);
    }

    public List<PurchaseOrder> findAllByStatus(Long storeId, boolean paid, boolean packaged, boolean sent, boolean delivered, String orderBy, String direction) {
        return orderDao.findAllByStatus(storeId, paid, packaged, sent, delivered, orderBy, direction);
    }

    public PurchaseOrder setPrepared(Long id) {
        PurchaseOrder order = orderDao.findById(id);
        order.setPackaged(true);
        order.setDtPackaged(DateTime.now().toDate());

        return orderDao.update(order);
    }

    public PurchaseOrder setShipped(Long id, String shippingCode) {
        PurchaseOrder order = orderDao.findById(id);
        order.setShippingCode(shippingCode);
        order.setDtSent(DateTime.now().toDate());
        order.setSent(true);

        return orderDao.update(order);
    }

    public List<Long> findAllGrupBySeller(Long storeId, Date dtStart, Date dtEnd) {
        return orderDao.findAllGrupBySeller(storeId, dtStart, dtEnd);
    }

    public List<SellerJson> findSellersOrders(String authorization, Long storeId, Date dtStart, Date dtEnd) throws Exception {
        List<Long> sellersIds = findAllGrupBySeller(storeId, dtStart, dtEnd);
        List<SellerJson> sellersJson = null;
        if (sellersIds != null) {
            sellersJson = new ArrayList<>(sellersIds.size());
            for (Long sellerId : sellersIds) {
                Authentication authentication = new Authentication(sellerId, AuthenticationRole.SELLER);
                ResponseEntity<SellerJson> entity = getForEntity(appConfig.getValue(0L, ConfigEnum.ACCOUNT_ENDPOINT) + "seller/token/" + authentication.getAuthenticationCode(),
                        authorization, SellerJson.class);
                SellerJson sellerJson = entity.getBody();
                List<PurchaseOrder> ordersBySeller = findAllBySellerId(storeId, sellerJson.getId());
                List<OrderJson> ordersJson = new ArrayList<>(ordersBySeller.size());
                for (PurchaseOrder orderBySeller : ordersBySeller) {
                    ordersJson.add(orderBySeller.toJson());
                }
                sellerJson.setOrdersJson(ordersJson);
                sellersJson.add(sellerJson);
            }
        }
        return sellersJson;
    }

    public SellerJson findBySeller(Long storeId, SellerJson sellerJson, Date startDate, Date endDate) throws Exception {
        List<PurchaseOrder> ordersBySeller = findAllBySellerId(storeId, sellerJson.getId(), startDate, endDate);
        List<OrderJson> ordersJson = new ArrayList<>(ordersBySeller.size());
        for (PurchaseOrder orderBySeller : ordersBySeller) {
            ordersJson.add(orderBySeller.toJson());
        }
        sellerJson.setOrdersJson(ordersJson);
        return sellerJson;
    }

    public void updatePagSeguroStatus(Credentials credentials, int days) throws ServiceUnavailableException {
        updatePagSeguroStatus(credentials, DateTime.now().plusDays(days * -1).toDate(), DateTime.now().toDate());
    }

    public void updatePagSeguroStatus(Credentials credentials, Date startDate, Date endDate) throws ServiceUnavailableException {
        try {
            TransactionSearchResult transactions = TransactionSearchService.searchByDate(PagSeguroConfig.getAccountCredentials(), startDate, endDate, 1, 1000);
            processPagSeguroTransaction(transactions);

            if (transactions.getTotalPages() > 1) {
                for (int i = 1; i < transactions.getTotalPages(); i++) {
                    transactions = TransactionSearchService.searchByDate(PagSeguroConfig.getAccountCredentials(), startDate, endDate, i, 1000);
                    processPagSeguroTransaction(transactions);
                }
            }
        } catch (PagSeguroServiceException ex) {
            throw new ServiceUnavailableException("Consulta ao PagSeguro indisponível no momento.");
        }
    }

    private void processPagSeguroTransaction(TransactionSearchResult transactions) throws NumberFormatException {
        if (transactions != null && transactions.getTransactionSummaries() != null && transactions.getTransactionSummaries().size() > 0) {
            for (TransactionSummary transaction : transactions.getTransactionSummaries()) {

                PurchaseOrder order = findById(Long.valueOf(transaction.getReference()));

                if (order == null || (order.getPagSeguroStatus() != null && order.getPagSeguroStatus().equals(transaction.getStatus()))) {
                    continue;
                }

                order.setPagSeguroCode(transaction.getCode());
                order.setPagSeguroStatus(transaction.getStatus());

                final TransactionStatus status = transaction.getStatus();
                if (null != status) {
                    switch (status) {
                        case AVAILABLE://A transação foi paga e chegou ao final de seu prazo de liberação sem ter sido retornada e sem que haja nenhuma disputa aberta.
                            break;
                        case CANCELLED://A transação foi cancelada sem ter sido finalizada
                            if (!order.isCanceled()) {
                                order.setCanceled(true);
                                order.setDtCanceled(DateTime.now().toDate());
                            }
                            break;
                        case CONTESTATION://O comprador, dentro do prazo de liberação da transação, abriu uma disputa.
                            break;
                        case INITIATED://Sem informação na API
                            break;
                        case IN_ANALYSIS://O comprador optou por pagar com um cartão de crédito e o PagSeguro está analisando o risco da transação.
                            break;
                        case IN_DISPUTE:
                            break;
                        case PAID: //A transação foi paga pelo comprador e o PagSeguro já recebeu uma confirmação da instituição financeira responsável pelo processamento.
                            if (!order.isPaid()) {
                                order.setPaid(true);
                                order.setDtPaid(DateTime.now().toDate());
                                order.setDtPaymentForecast(DateTime.now().plusDays(14).toDate());//Adicionado 14 dias, pois esse é o prazo minimo para a liberação do pagamento pelo PagSeguro. Se o cliente optar por um prazo maior para pagar uma menor taxa de comissão o problema é dele ;)
                            }
                            break;
                        case REFUNDED://O valor da transação foi devolvido para o comprador.
                            if (!order.isCanceled()) {
                                order.setCanceled(true);
                                order.setDtCanceled(DateTime.now().toDate());
                            }
                            break;
                        case SELLER_CHARGEBACK://Sem informação na API
                            break;
                        case UNKNOWN_STATUS:
                            break;
                        case WAITING_PAYMENT: //O comprador iniciou a transação, mas até o momento o PagSeguro não recebeu nenhuma informação sobre o pagamento.
                            break;
                        default:
                            break;
                    }
                    update(order);
                }
            }
        }
    }
}
