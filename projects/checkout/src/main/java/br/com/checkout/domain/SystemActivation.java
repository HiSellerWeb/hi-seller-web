package br.com.checkout.domain;

import br.com.commons.security.Encrypter;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.joda.time.DateTime;

@Entity
@Table(name = "systemActivation")
@Cache(usage = CacheConcurrencyStrategy.NONE)
public class SystemActivation implements Serializable {

    private static final long serialVersionUID = 30812093810L;

    @Id
    @SequenceGenerator(name = "system_activation_id", sequenceName = "system_activation_seg")
    @Column(name = "id")
    private Long id;

    @Column(name = "expireDate")
    private String expireDate = "";

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public DateTime expireDateToDateTime() throws Exception {
        return DateTime.parse(Encrypter.decrypt(expireDate));
    }

    public void setDateTimeExpireDate(DateTime expireDate) throws Exception {
        this.expireDate = Encrypter.encrypt(expireDate.toString());
    }
}
