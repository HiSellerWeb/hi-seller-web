package br.com.checkout.validator;

import br.com.commons.json.CartJson;
import br.com.commons.validator.AbstractValidator;
import br.com.commons.validator.RestrictionType;

public class CartValidator extends AbstractValidator<CartJson> {

    @Override
    protected void apply(CartJson cartJson) {
        if (cartJson == null) {
            setInvalid(true).setError("cartJson", RestrictionType.REQUIRED, "Objeto nulo");
            return;
        }

        isNull(cartJson.getCostumerId()).setError("cartJson.userId", RestrictionType.REQUIRED, "Obrigatório informar o usuário do carrinho");
    }
}
