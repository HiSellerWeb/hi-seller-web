package br.com.checkout.service;

import br.com.checkout.config.ConfigEnum;
import br.com.checkout.dao.PurchaseOrderDao;
import br.com.checkout.dao.SystemActivationDao;
import br.com.checkout.domain.PurchaseOrder;
import br.com.commons.exception.NotFoundException;
import br.com.commons.exception.UnprocessableEntityException;
import br.com.commons.helper.StringUtils;
import br.com.commons.json.AddressJson;
import br.com.commons.json.CustomerJson;
import br.com.commons.json.ProductCartListJson;
import br.com.commons.json.ProductMovJson;
import br.com.commons.json.SkuCartJson;
import br.com.commons.json.SystemInfoJson;
import br.com.commons.security.Encrypter;
import br.com.commons.service.AbstractService;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Calendar;
import org.joda.time.DateTime;
import org.jrimum.bopepo.BancosSuportados;
import org.jrimum.bopepo.Boleto;
import org.jrimum.bopepo.view.BoletoViewer;
import org.jrimum.domkee.comum.pessoa.endereco.CEP;
import org.jrimum.domkee.comum.pessoa.endereco.Endereco;
import org.jrimum.domkee.comum.pessoa.endereco.UnidadeFederativa;
import org.jrimum.domkee.comum.pessoa.id.cprf.CNPJ;
import org.jrimum.domkee.financeiro.banco.febraban.Agencia;
import org.jrimum.domkee.financeiro.banco.febraban.Carteira;
import org.jrimum.domkee.financeiro.banco.febraban.Cedente;
import org.jrimum.domkee.financeiro.banco.febraban.ContaBancaria;
import org.jrimum.domkee.financeiro.banco.febraban.NumeroDaConta;
import org.jrimum.domkee.financeiro.banco.febraban.Sacado;
import org.jrimum.domkee.financeiro.banco.febraban.TipoDeTitulo;
import org.jrimum.domkee.financeiro.banco.febraban.Titulo;
import org.jrimum.domkee.financeiro.banco.febraban.Titulo.Aceite;
import org.springframework.beans.factory.annotation.Autowired;

public class PaymentService extends AbstractService {

    @Autowired
    private PurchaseOrderDao orderDao;

    @Autowired
    private SystemActivationDao systemActivationDao;

    public byte[] getTicket(Long storeId, PurchaseOrder purchaseOrder) throws Exception {
        String ticket = Encrypter.encrypt(String.valueOf(purchaseOrder.getId())) + ".pdf";
        String ticketPath = appConfig.getValue(storeId, ConfigEnum.TICKET_PATH) + ticket;
        if (Files.isReadable(Paths.get(ticketPath))) {
            return Files.readAllBytes(Paths.get(ticketPath));
        }
        throw new NotFoundException();
    }

    public String generateTicket(Long storeId, PurchaseOrder purchaseOrder, CustomerJson customerJson) throws Exception {
        String ticket = Encrypter.encrypt(String.valueOf(purchaseOrder.getId())) + ".pdf";
        String ticketPath = appConfig.getValue(storeId, ConfigEnum.TICKET_PATH) + ticket;
        if (Files.isReadable(Paths.get(ticketPath))) {
            return ticketPath;
        }

        Cedente cedente = getCedente(storeId);

        Sacado sacado = new Sacado(customerJson.getName(), customerJson.getCpf());

        AddressJson deliveryAddress = purchaseOrder.getObjectDeliveryAddress();

        Endereco enderecoSac = new Endereco();
        enderecoSac.setUF(UnidadeFederativa.valueOfSigla(deliveryAddress.getUf()));
        enderecoSac.setLocalidade(deliveryAddress.getCity());
        enderecoSac.setCep(new CEP(deliveryAddress.getZipCode()));
        enderecoSac.setBairro(deliveryAddress.getNeighborhood());
        enderecoSac.setLogradouro(deliveryAddress.getStreet()
                .concat(", ")
                .concat(deliveryAddress.getNumber())
                .concat(!StringUtils.isEmpty(deliveryAddress.getComplement()) ? "" : " - " + deliveryAddress.getComplement()));

        sacado.addEndereco(enderecoSac);

        ContaBancaria contaBancaria = getContaCedente(storeId);

        Titulo titulo = new Titulo(contaBancaria, sacado, cedente);
        titulo.setNumeroDoDocumento(purchaseOrder.getId().toString());
        titulo.setNossoNumero(purchaseOrder.getId().toString());
        titulo.setDigitoDoNossoNumero("8");
        titulo.setValor(purchaseOrder.getCart().getProductsValue().add(purchaseOrder.getCart().getFreightCost()).subtract(purchaseOrder.getCart().getDiscountTicket()));
        titulo.setTipoDeDocumento(TipoDeTitulo.OUTROS);
        titulo.setAceite(Aceite.N);
        titulo.setDesconto(BigDecimal.ZERO);
        titulo.setDeducao(BigDecimal.ZERO);
        titulo.setMora(BigDecimal.ZERO);
        titulo.setAcrecimo(BigDecimal.ZERO);
        titulo.setValorCobrado(purchaseOrder.getCart().getProductsValue().add(purchaseOrder.getCart().getFreightCost()).subtract(purchaseOrder.getCart().getDiscountTicket()));

        Calendar today = Calendar.getInstance();
        titulo.setDataDoDocumento(today.getTime());

        Calendar dateLimitToPay = today;
        dateLimitToPay.add(Calendar.DATE, +5);
        titulo.setDataDoVencimento(dateLimitToPay.getTime());

        Boleto boleto = new Boleto(titulo);

        boleto.setLocalPagamento("ATÉ O VENCIMENTO, PREFERENCIALMENTE NO ITAÚ");

        boleto.setInstrucao1("==================================================================================");
        boleto.setInstrucao2("                                              OBRIGADO POR COMPRAR COM A ALABASTRUM");
        boleto.setInstrucao3("==================================================================================");
        boleto.setInstrucao4("");
        boleto.setInstrucao5("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
        boleto.setInstrucao6("                                                  FAVOR NÃO RECEBER APÓS O VENCIMENTO");
        boleto.setInstrucao7("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");

        BoletoViewer boletoViewer = new BoletoViewer(boleto);

        File arquivoPdf = boletoViewer.getPdfAsFile(ticketPath);

        fileToByte(arquivoPdf);
        return ticket;
    }

    private static Cedente getCedente(Long storeId) {
        Cedente cedente = new Cedente("ALABASTRUM COSM BR LTDA ME");
        cedente.setCPRF(new CNPJ("03.424.700/0001-00"));
        Endereco enderecoCed = new Endereco();
        enderecoCed.setUF(UnidadeFederativa.RJ);
        enderecoCed.setLocalidade("Rio de Janeiro");
        enderecoCed.setCep(new CEP("21351-021"));
        enderecoCed.setBairro("Madureira");
        enderecoCed.setLogradouro("Rua Carolina Machado");
        enderecoCed.setNumero("380, cobertura 1");
        cedente.addEndereco(enderecoCed);
        return cedente;
    }

    private static ContaBancaria getContaCedente(Long storeId) {
        ContaBancaria contaBancaria = new ContaBancaria(BancosSuportados.BANCO_ITAU.create());
        contaBancaria.setNumeroDaConta(new NumeroDaConta(16602, "0"));
        contaBancaria.setCarteira(new Carteira(175));
        contaBancaria.setAgencia(new Agencia(9162));
        return contaBancaria;
    }

    private static byte[] fileToByte(File file) throws Exception {
        @SuppressWarnings("resource")
        FileInputStream fis = new FileInputStream(file);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] buffer = new byte[8192];
        int bytesRead = 0;
        while ((bytesRead = fis.read(buffer, 0, 8192)) != -1) {
            baos.write(buffer, 0, bytesRead);
        }
        return baos.toByteArray();
    }

    public PurchaseOrder confirmPayment(String authorization, Long storeId, String ticket) throws UnprocessableEntityException, NotFoundException, Exception {
        if (ticket == null || ticket.isEmpty() || !ticket.contains("-")) {
            throw new UnprocessableEntityException("Número de boleto inválido.", null);
        }
        String[] split = ticket.split("-");
        String stringOrderId = split[0];
        if (!StringUtils.isNumeric(stringOrderId)) {
            throw new UnprocessableEntityException("Número de boleto inválido.", null);
        }

        PurchaseOrder order = orderDao.findById(Long.valueOf(stringOrderId));

        if (order == null) {
            throw new NotFoundException("Boleto não encontrado");
        }

        //Chama o serviço do stock para movimentar os produtos
        ProductCartListJson products = order.getCart().getObjectProducts();
        for (SkuCartJson product : products) {
            postForEntity(appConfig.getValue(storeId, ConfigEnum.STOCK_ENDPOINT) + "sku/orderToSold",
                    authorization, new ProductMovJson(product.getId(), product.getQuantity()), ProductMovJson.class);
        }

        order.setDtPaid(DateTime.now().toDate());
        order.setPaid(true);
        order.setDtPaymentForecast(DateTime.now().toDate());

        return orderDao.update(order);
    }

    public SystemInfoJson getExpirationDate(Long storeId) throws Exception {
        DateTime expireDate = systemActivationDao.getSystemActivation(storeId).expireDateToDateTime();
        SystemInfoJson systemInfoJson = new SystemInfoJson();
        systemInfoJson.setExpirationDate(expireDate.toString("dd/MM/YYYY H:m"));
        systemInfoJson.setIsValid(expireDate.isAfterNow() || expireDate.isEqualNow());
        return systemInfoJson;
    }

    public DateTime renewExpirationDate(Long storeId) throws Exception {
        return systemActivationDao.renewSystemActivation(storeId);
    }
}
