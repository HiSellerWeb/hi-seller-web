package br.com.checkout.config;

import br.com.commons.appConfig.ConfigEnumInterface;
import br.com.commons.model.ConfigTypeEnum;

public enum ConfigEnum implements ConfigEnumInterface<ConfigEnum> {

    ENDPOINT("ENDPOINT", "http://localhost:8080/checkout/"),
    STOCK_ENDPOINT("STOCK_ENDPOINT", "http://localhost:8080/stock/"),
    FRONT_ENDPOINT("FRONT_ENDPOINT", "http://localhost:8080/front/"),
    ACCOUNT_ENDPOINT("ACCOUNT_ENDPOINT", "http://localhost:8080/account/"),
    TIME_TO_EXPIRE_CART("TIME_TO_EXPIRE_CART", "30"),
    TIME_TO_EXECUTE_EXPIRE_CART("TIME_TO_EXECUTE_EXPIRE_CART", "15"),
    TICKET_PATH("TICKET_PATH", "C:\\boletos\\"),
    PAGSEGURO_NOTIFICATION_URL("PAGSEGURO_NOTIFICATION_URL", "payment/pagSeguro/notification"),
    ///////////////////////////////////////////////////////
    SHOP_ZIP_CODE("SHOP_ZIP_CODE", "89012510", 0L, ConfigTypeEnum.INDIVIDUAL);

    private final String name;
    private final String defaultValue;
    private final Long storeId;
    private final ConfigTypeEnum configType;

    private ConfigEnum(String key, String defaultValue) {
        this.name = key;
        this.defaultValue = defaultValue;
        this.storeId = 0L;
        this.configType = ConfigTypeEnum.GLOBAL;
    }

    private ConfigEnum(String key, String defaultValue, Long storeId, ConfigTypeEnum configType) {
        this.name = key;
        this.defaultValue = defaultValue;
        this.storeId = storeId;
        this.configType = configType;
    }

    @Override
    public ConfigEnum[] getValues() {
        return values();
    }

    @Override
    public Long getStoreId() {
        return storeId;
    }

    @Override
    public ConfigTypeEnum getConfigType() {
        return configType;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDefaultValue() {
        return defaultValue;
    }
}
