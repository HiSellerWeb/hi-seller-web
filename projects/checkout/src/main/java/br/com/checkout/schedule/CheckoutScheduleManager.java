package br.com.checkout.schedule;

import br.com.checkout.config.ConfigEnum;
import br.com.checkout.schedule.task.ExpireCartTask;
import br.com.checkout.schedule.task.UpdatePagSeguroStatus;
import br.com.checkout.service.CartService;
import br.com.checkout.service.PurchaseOrderService;
import br.com.commons.appConfig.AppConfig;
import br.com.commons.schedule.AbstractScheduleManager;
import br.com.uol.pagseguro.domain.Credentials;
import br.com.uol.pagseguro.properties.PagSeguroConfig;
import javax.annotation.PostConstruct;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public final class CheckoutScheduleManager extends AbstractScheduleManager {

    private static final Log LOG = LogFactory.getLog(CheckoutScheduleManager.class);
    private ExpireCartTask expireCartTask;
    private UpdatePagSeguroStatus updatePagSeguroStatus;

    private final CartService cartService;
    private final PurchaseOrderService orderService;
    private final AppConfig appConfig;

    private final Credentials credentials;

    public CheckoutScheduleManager(CartService cartService, PurchaseOrderService orderService, AppConfig appConfig) throws Exception {
        this.credentials = PagSeguroConfig.getAccountCredentials();
        this.cartService = cartService;
        this.orderService = orderService;
        this.appConfig = appConfig;

        schedule();
    }

    @Override
    @PostConstruct
    public void schedule() {
        scheduleExpireCart();
        scheduleUpdatePagSeguroStatus();
    }

    private void scheduleExpireCart() {
        cancelSchedule(expireCartTask);
        expireCartTask = new ExpireCartTask(cartService);
        scheduleTask(expireCartTask, appConfig.getValueLongDefault(0L, ConfigEnum.TIME_TO_EXECUTE_EXPIRE_CART, 1L), appConfig.getValueLongDefault(0L, ConfigEnum.TIME_TO_EXPIRE_CART, 30L));
    }

    private void scheduleUpdatePagSeguroStatus() {
        cancelSchedule(updatePagSeguroStatus);
        updatePagSeguroStatus = new UpdatePagSeguroStatus(orderService, credentials);
        scheduleTask(updatePagSeguroStatus, 3L, 15L);
    }
}
