package br.com.checkout.model;

public enum PaymentTypeEnum {

    TICKET("TICKET"),
    PAG_SEGURO("PAG_SEGURO");

    private String name;

    PaymentTypeEnum(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
