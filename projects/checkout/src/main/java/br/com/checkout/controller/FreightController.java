package br.com.checkout.controller;

import br.com.checkout.config.ConfigEnum;
import br.com.checkout.domain.Cart;
import br.com.checkout.service.CartService;
import br.com.checkout.service.FreightService;
import br.com.commons.appConfig.AppConfig;
import br.com.commons.controller.AbstractController;
import br.com.commons.json.FreighJson;
import br.com.commons.json.ProductJson;
import br.com.commons.security.Encrypter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "/freight")
public class FreightController extends AbstractController {

    private static final Log LOG = LogFactory.getLog(FreightController.class);

    @Autowired
    private FreightService freightService;

    @Autowired
    private CartService cartService;

    @Autowired
    private AppConfig appConfig;

    @RequestMapping(value = "{zipCode}/product/{productId}", method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<FreighJson> getCart(@RequestHeader(value = "Authorization") String authorization, @PathVariable("zipCode") String zipCode, @PathVariable("productId") String productId, HttpServletRequest request, HttpServletResponse response) throws Exception {
        Long storeId = validateAuthorization(authorization);
        ResponseEntity<ProductJson> getForEntity = getForEntity(appConfig.getValue(storeId, ConfigEnum.STOCK_ENDPOINT) + "product/" + productId, authorization, ProductJson.class);
        ProductJson productJson = getForEntity.getBody();

        FreighJson freighJson = freightService.freightCost(storeId, authorization, zipCode, Double.valueOf(productJson.getPrice().toString()), productJson.getWeight());
        return new ResponseEntity<>(freighJson, HttpStatus.OK);
    }

    @RequestMapping(value = "/{zipCode}/cart/{cartId}", method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<FreighJson> getFullCart(@RequestHeader(value = "Authorization") String authorization, @PathVariable("zipCode") String zipCode, @PathVariable("cartId") String cartId, HttpServletRequest request, HttpServletResponse response) throws Exception {
        Long storeId = validateAuthorization(authorization);
        Cart cart = cartService.verify(Long.valueOf(Encrypter.decrypt(cartId)));

        FreighJson freighJson = freightService.freightCost(storeId, authorization, zipCode, cart.getObjectProducts());

        return new ResponseEntity<>(freighJson, HttpStatus.OK);
    }
}
