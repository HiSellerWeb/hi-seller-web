package br.com.base_api;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@ContextConfiguration({"/resources/spring/testDataSource.xml", "/resources/spring/testApplicationContext.xml"})
@Transactional(propagation = Propagation.REQUIRED)
public class AbstractTest extends AbstractTransactionalJUnit4SpringContextTests {

    @Before
    public void setUp() {
    }

    //Para não gerar erro de classe sem teste
    @Test
    public void byPass() {
        Assert.assertTrue(Boolean.TRUE);
    }
}
