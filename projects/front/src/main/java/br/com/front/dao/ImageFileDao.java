package br.com.front.dao;

import br.com.commons.dao.AbstractDao;
import br.com.front.domain.ImageFile;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public class ImageFileDao extends AbstractDao<ImageFile> {

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public ImageFile save(ImageFile imageFile) {
        imageFile.setId(null);
        return super.save(imageFile);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public ImageFile update(ImageFile imageFile) {
        return super.update(imageFile);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void delete(ImageFile imageFile) {
        super.delete(imageFile);
    }

    public List<ImageFile> findAll(Long storeId) {
        List<Criterion> restrictions = new ArrayList<>();
        restrictions.add(Restrictions.eq("storeId", storeId));
        return super.findByRestrictions(ImageFile.class, restrictions, Order.asc("name"));
    }

    public ImageFile findById(Long id) {
        return super.findById(ImageFile.class, id);
    }

    public ImageFile findByName(String imageFile) {
        List<Criterion> restrictions = new ArrayList<>();
        restrictions.add(Restrictions.like("name", imageFile).ignoreCase());
        List<ImageFile> imageFiles = super.findByRestrictions(ImageFile.class, restrictions);

        if (imageFiles != null && imageFiles.size() >= 1) {
            return imageFiles.get(0);
        }

        return null;
    }

}
