package br.com.front.config;

import br.com.commons.appConfig.ConfigEnumInterface;
import br.com.commons.model.ConfigTypeEnum;

public enum ConfigEnum implements ConfigEnumInterface<ConfigEnum> {

    ENDPOINT("ENDPOINT", "http://localhost:8080/front/"),
    ACCOUNT_ENDPOINT("ACCOUNT_ENDPOINT", "http://localhost:8080/account/"),
    STOCK_ENDPOINT("ACCOUNT_ENDPOINT", "http://localhost:8080/stock/");

    private final String name;
    private final String defaultValue;
    private final Long storeId;
    private final ConfigTypeEnum configType;

    private ConfigEnum(String key, String defaultValue) {
        this.name = key;
        this.defaultValue = defaultValue;
        this.storeId = 0L;
        this.configType = ConfigTypeEnum.GLOBAL;
    }

    private ConfigEnum(String key, String defaultValue, Long storeId, ConfigTypeEnum configType) {
        this.name = key;
        this.defaultValue = defaultValue;
        this.storeId = storeId;
        this.configType = configType;
    }

    @Override
    public ConfigEnum[] getValues() {
        return values();
    }

    @Override
    public Long getStoreId() {
        return storeId;
    }

    @Override
    public ConfigTypeEnum getConfigType() {
        return configType;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDefaultValue() {
        return defaultValue;
    }
}