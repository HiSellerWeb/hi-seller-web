package br.com.front.service;

import br.com.commons.exception.ConflictException;
import br.com.commons.exception.NotFoundException;
import br.com.front.dao.ImageFileDao;
import br.com.front.domain.ImageFile;
import java.io.File;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class ImageFileService {

    static final Logger LOG = Logger.getLogger(ImageFileService.class);

    @Autowired
    private ImageFileDao imageFileDao;

    public ImageFile create(ImageFile imageFile) throws Exception {
        ImageFile imgFile = imageFileDao.findByName(imageFile.getName());
        if (imgFile != null) {
            //return imgFile;
            throw new ConflictException();
        } else {
            return imageFileDao.save(imageFile);
        }
    }

    public List<ImageFile> findAll(Long storeId) {
        return imageFileDao.findAll(storeId);
    }

    public String remove(Long imageFileId) throws Exception {
        final ImageFile imageFile = imageFileDao.findById(imageFileId);
        if (imageFile == null) {
            throw new NotFoundException();
        }
        new File(imageFile.getPath()).delete();
        imageFileDao.delete(imageFile);

        return imageFile.getPath();
    }

    public void update(ImageFile imageFile) throws NotFoundException {
        imageFileDao.update(imageFile);
    }

    public ImageFile findById(Long imageFileId) throws NotFoundException {
        ImageFile imageFile = imageFileDao.findById(imageFileId);

        if (imageFile == null) {
            throw new NotFoundException();
        }
        return imageFile;
    }

}
