package br.com.front.domain;

import br.com.commons.json.ImageJson;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Table(name = "ImageFile")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ImageFile implements Serializable {

    private static final long serialVersionUID = 34938240928304L;

    @Id
    @SequenceGenerator(name = "imageFile_id", sequenceName = "imageFile_id_seg")
    @GeneratedValue(generator = "imageFile_id", strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "storeId", nullable = false, updatable = false)
    private Long storeId;

    @Column(name = "name", nullable = false, unique = true, length = 255)
    private String name;

    @Column(name = "url")
    private String url;

    @Column(name = "path", nullable = false)
    private String path;

    @Column(name = "fileName", columnDefinition = "VARCHAR(500)")
    private String fileName;

    @Column(name = "size", nullable = false)
    private Long size = 0L;

    @Column(name = "type", nullable = false)
    private String type;

    public ImageFile() {
    }

    public ImageJson toJson() {
        ImageJson json = new ImageJson();
        json.setId(getId());
        json.setName(getName());
        json.setUrl(getUrl());
        json.setPath(getPath());
        json.setFileName(getFileName());
        json.setSize(getSize());
        json.setType(getType());
        json.setStoreId(getStoreId());
        return json;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getStoreId() {
        return storeId;
    }

    public void setStoreId(Long storeId) {
        this.storeId = storeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
