package br.com.front.controller;

import br.com.commons.json.CategoryJson;
import br.com.commons.json.ProductJson;
import br.com.front.config.ConfigEnum;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/categoria")
public class CategoryController extends AbstractFrontController {

    @RequestMapping(value = "/{categoryName}")
    public String category(@PathVariable("categoryName") String categoryName, HttpServletRequest req, ModelMap model, HttpServletResponse resp) {

        String pre = preHandler(req, model, resp);
        if (pre != null) {
            return pre;
        }

        ResponseEntity<CategoryJson> categoryEntity = getForEntity(appConfig.getValue(getStoreId(), ConfigEnum.STOCK_ENDPOINT) + "category/?categoryName=" + categoryName, getAuthorization(), CategoryJson.class);
        if (categoryEntity == null || categoryEntity.getBody() == null) {
            return "404";
        }

        CategoryJson category = categoryEntity.getBody();
        model.put("category", category);

        ResponseEntity<ProductJson[]> productEntity = getForEntity(appConfig.getValue(getStoreId(), ConfigEnum.STOCK_ENDPOINT) + "product/category/" + category.getId(), getAuthorization(), ProductJson[].class);
        if (productEntity == null || productEntity.getBody() == null) {
            return "404";
        }

        ProductJson[] products = productEntity.getBody();
        model.put("products", products);

        String pos = posHandler(category.getName(), req, model, resp);
        if (pos != null) {
            return pos;
        }

        return "layouts/" + category.getLayoutFile();
    }
}
