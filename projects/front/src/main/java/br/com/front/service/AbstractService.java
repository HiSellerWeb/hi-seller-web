package br.com.fileRepository.service;

import br.com.commons.appConfig.AppConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestTemplate;

class AbstractService {

    @Autowired
    protected AppConfig appConfig;

    @Autowired
    protected RestTemplate restTemplate;

}
