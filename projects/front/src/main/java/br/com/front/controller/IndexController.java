package br.com.front.controller;

import br.com.commons.json.CategoryJson;
import br.com.front.config.ConfigEnum;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class IndexController extends AbstractFrontController {

    @RequestMapping(value = "/")
    public String index(HttpServletRequest req, ModelMap model, HttpServletResponse resp) {

        String pre = preHandler(req, model, resp);
        if (pre != null) {
            return pre;
        }

        ResponseEntity<CategoryJson[]> entity = getForEntity(appConfig.getValue(getStoreId(), ConfigEnum.STOCK_ENDPOINT) + "category", getAuthorization(), CategoryJson[].class);
        if (entity != null && entity.getBody() != null) {
            model.put("categories", entity.getBody());
        }

        String pos = posHandler("HI Seller Web", req, model, resp);
        if (pos != null) {
            return pos;
        }

        return "home";
    }
}
