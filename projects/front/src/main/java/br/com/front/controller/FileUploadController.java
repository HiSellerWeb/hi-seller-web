package br.com.front.controller;

import br.com.commons.appConfig.AppConfig;
import br.com.commons.json.ImageJson;
import br.com.commons.json.ImageListJson;
import br.com.front.domain.ImageFile;
import br.com.front.service.ImageFileService;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.List;
import java.util.regex.Pattern;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

@Controller
public class FileUploadController extends AbstractFrontController {

    @Autowired
    private ImageFileService imageFileService;

    @Autowired
    private AppConfig appConfig;

    private static final Logger logger = LoggerFactory
            .getLogger(FileUploadController.class);

    @RequestMapping(value = "/uploadFile", method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity<ImageJson> uploadFileHandler(@RequestParam("name") String name, @RequestParam("file") MultipartFile file, HttpServletRequest request) throws Exception {
        Long storeId = getStoreId();
        String fileName = file.getOriginalFilename();
        String url = "http://localhost:8080/img/";
        String path = "C:\\projetos\\hi-seller-web\\apache-tomcat-8.0.23\\webapps\\img\\";
        String realName = storeId.toString() + "_" + name + "_" + fileName;

        //Salvando dados da imagem no banco
        ImageFile imageFile = new ImageFile();
        imageFile.setName(name);
        imageFile.setFileName(fileName);
        imageFile.setUrl(url + realName);
        imageFile.setPath(path + realName);
        imageFile.setStoreId(storeId);
        imageFile.setSize(file.getSize());
        imageFile.setType(file.getContentType());
        imageFile = imageFileService.create(imageFile);
        ImageJson imageJson = new ImageJson();
        imageJson.setUrl(imageFile.getUrl());

        if (file.isEmpty()) {
            return null;
        }
        byte[] bytes = file.getBytes();

        File dir = new File(path + File.separator + "");
        if (!dir.exists()) {
            dir.mkdirs();
        }

        // Create the file on server
        File serverFile = new File(dir.getAbsolutePath()
                + File.separator + realName);

        try (BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile))) {
            stream.write(bytes);
            stream.close();
        }

        //no nome colocar um sequencial storeID + ID
        return new ResponseEntity<>(imageJson, HttpStatus.OK);
    }

    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/{imageId}", method = RequestMethod.DELETE)
    public @ResponseBody
    ResponseEntity<ImageJson> delete(@PathVariable("imageId") String imageId, HttpServletRequest request, HttpServletResponse response) throws Exception {
        //validateManagerToken(storeId, token);
        if (imageId.indexOf(",") > -1) {
            String imageIds[] = imageId.split(Pattern.quote(","));
            for (String id : imageIds) {
                imageFileService.remove(Long.parseLong(id));
            }
        } else {
            imageFileService.remove(Long.parseLong(imageId));

        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

//    @RequestMapping(value = "/uploadMultipleFile", method = RequestMethod.POST)
//    public @ResponseBody
//    String uploadMultipleFileHandler(@RequestParam("name") String[] names,
//            @RequestParam("file") MultipartFile[] files) {
//
//        if (files.length != names.length) {
//            return "Mandatory information missing";
//        }
//
//        String message = "";
//        for (int i = 0; i < files.length; i++) {
//            MultipartFile file = files[i];
//            String name = names[i];
//            try {
//                byte[] bytes = file.getBytes();
//
//                // Creating the directory to store file
//                String rootPath = System.getProperty("catalina.home");
//                File dir = new File(rootPath + File.separator + "tmpFiles");
//                if (!dir.exists()) {
//                    dir.mkdirs();
//                }
//
//                // Create the file on server
//                File serverFile = new File(dir.getAbsolutePath()
//                        + File.separator + name);
//                BufferedOutputStream stream = new BufferedOutputStream(
//                        new FileOutputStream(serverFile));
//                stream.write(bytes);
//                stream.close();
//
//                logger.info("Server File Location="
//                        + serverFile.getAbsolutePath());
//
//                message = message + "You successfully uploaded file=" + name + "";
//            } catch (Exception e) {
//                return "You failed to upload " + name + " => " + e.getMessage();
//            }
//        }
//        return message;
//    }
    @RequestMapping(method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<ImageListJson> findAll(HttpServletRequest request, HttpServletResponse response) throws Exception {
        Long storeId = getStoreId();
        final List<ImageFile> images = imageFileService.findAll(storeId);
        final ImageListJson imageListJson = new ImageListJson();

        if (images != null && !images.isEmpty()) {
            for (ImageFile file : images) {
                imageListJson.add(file.toJson());
            }
        }

        return new ResponseEntity<>(imageListJson, HttpStatus.OK);
    }
}
