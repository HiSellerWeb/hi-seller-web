package br.com.front.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/minha-conta")
public class MyAccountController extends AbstractFrontController {

    @RequestMapping(value = "")
    public String myAccountHome(HttpServletRequest req, ModelMap model, HttpServletResponse resp) {
        String pre = preHandler(req, model, resp);
        if (pre != null) {
            return pre;
        }

        String pos = posHandler("Minha Conta", req, model, resp);
        if (pos != null) {
            return pos;
        }

        return "customer/myAccount";
    }
}
