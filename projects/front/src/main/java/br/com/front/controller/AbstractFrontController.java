package br.com.front.controller;

import br.com.commons.appConfig.AppConfig;
import br.com.commons.controller.AbstractController;
import br.com.commons.helper.StringUtils;
import br.com.commons.json.CartJson;
import br.com.commons.json.Json;
import br.com.front.config.ConfigEnum;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;

class AbstractFrontController extends AbstractController {

    @Autowired
    public AppConfig appConfig;

    public String preHandler(HttpServletRequest req, ModelMap model, HttpServletResponse resp) {
        Map cookies = getCookies(req);

        String cart = (String) cookies.get("cart");
        if (!StringUtils.isEmpty(cart)) {
            model.addAttribute("cart", Json.toObject(cart, CartJson.class));
        }

        String seller = (String) cookies.get("seller");
        if (!StringUtils.isEmpty(seller)) {
            model.addAttribute("seller", seller);
        }

        String customerToken = (String) cookies.get("customerToken");
        if (!StringUtils.isEmpty(customerToken)) {
            model.addAttribute("customerToken", customerToken);
        }

        return null;
    }

    public String posHandler(String title, HttpServletRequest req, ModelMap model, HttpServletResponse resp) {
        model.addAttribute("title", title);
        model.addAttribute("ENDPOINT", appConfig.getValue(getStoreId(), ConfigEnum.ENDPOINT));

        return null;
    }

    public String getCookie(HttpServletRequest req, String name) {
        for (Cookie c : req.getCookies()) {
            if (c.getName().equals(name)) {
                return c.getValue();
            }
        }
        return null;
    }

    public Map<String, String> getCookies(HttpServletRequest req) {
        Map cookies = new HashMap();
        if (req.getCookies() != null) {
            for (Cookie c : req.getCookies()) {
                cookies.put(c.getName(), c.getValue());
            }
        }
        return cookies;
    }

    protected String getAuthorization() {
        return "0";
    }

    protected Long getStoreId() {
        return 0L;
    }
}
