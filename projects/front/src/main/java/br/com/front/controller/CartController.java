package br.com.front.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/carrinho")
public class CartController extends AbstractFrontController {

    @RequestMapping(value = "")
    public String cart(HttpServletRequest req, ModelMap model, HttpServletResponse resp) {
        String pre = preHandler(req, model, resp);
        if (pre != null) {
            return pre;
        }

//        ResponseEntity<CategoryJson> categoryEntity = getForEntity(appConfig.getValue(getStoreId(), ConfigEnum.STOCK_ENDPOINT) + "category/?categoryName=" + categoryName, getAuthorization(), CategoryJson.class);
//        if (categoryEntity == null || categoryEntity.getBody() == null) {
//            return "404";
//        }
//
//        CategoryJson category = categoryEntity.getBody();
//        model.put("category", category);
//
//        ResponseEntity<ProductJson[]> productEntity = getForEntity(appConfig.getValue(getStoreId(), ConfigEnum.STOCK_ENDPOINT) + "product/category/" + category.getId(), getAuthorization(), ProductJson[].class);
//        if (productEntity == null || productEntity.getBody() == null) {
//            return "404";
//        }
//
//        ProductJson[] products = productEntity.getBody();
//        model.put("products", products);
        String pos = posHandler("Carrinho", req, model, resp);
        if (pos != null) {
            return pos;
        }

        return "cart";
    }
}
