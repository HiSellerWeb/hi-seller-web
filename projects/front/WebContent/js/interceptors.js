angular.factory('httpRequestInterceptor', function ($q, $location) {
    return {
        response: function (response) {
            return promise.then(
                    function success(response) {
                        window.alert('ok');
                        return response;
                    },
                    function error(response) {
                        if (response.status === 404) {
                            window.alert('404');
                            $location.path('/404');
                            return $q.reject(response);
                        }
                        else {
                            return $q.reject(response);
                        }
                    }
            );
        }
    };
});