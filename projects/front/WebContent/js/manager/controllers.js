function preLoad($scope, $state, $http, $cookies) {
    if ($scope.config === undefined) {
        $scope.config = getConfig();
        $scope.title = $scope.config.title;
    }

    // validando login usuário
    var customerToken = $cookies.get('managerToken');
    if (customerToken && $scope.config.accountUrl) {

        $http.get($scope.config.accountUrl + 'manager/' + customerToken)
                .then(function successCallback(response) {
                    $cookies.put('managerToken', response.data.authenticationToken);
                    $scope.customer = response.data;

                }, function errorCallback(response) {
                    console.log(JSON.stringify(response.data));
                    if (response.status === 410) {
                        $cookies.remove("managerToken");
                        console.log("Autenticação expirada e removida");
                    } else {
                        $cookies.remove("managerToken");
                        console.log("Autenticação inválida e removida");
                    }
                    window.location.reload();
                });
    } else {
        $state.go('loginManager');
    }
}

function getAllCategories($scope, $http) {
    //Busca as categorias cadastradas
    $http.get($scope.config.stockUrl + 'category')
            .then(function successCallback(response) {
                $scope.categories = response.data;
            }, function errorCallback(response) {
                $scope.error = response.data;
            });

    //Busca as imagens cadastradas
    $http.get($scope.config.fileRepositoryUrl)
            .then(function successCallback(response) {
                $scope.galleryImages = response.data;
            }, function errorCallback(response) {
                $scope.error = response.data;
            });
}

function getAllTypes($scope, $http) {
    //Busca as categorias cadastradas
    $http.get($scope.config.stockUrl + 'type')
            .then(function successCallback(response) {
                $scope.types = response.data;
            }, function errorCallback(response) {
                $scope.error = response.data;
            });
}


function getAllImages($scope, $state, $stateParams, $http, $cookies) {
    //Busca as categorias cadastradas
    $http.get($scope.config.fileRepositoryUrl)
            .then(function successCallback(response) {
                $scope.images = response.data;
            }, function errorCallback(response) {
                $scope.error = response.data;
            });
}


function getTypeByName($scope, $http, name) {
    $http.get($scope.config.stockUrl + 'type')
            .then(function successCallback(response) {
                var types = response.data;
                for (i = 0; types.length; i++) {
                    if (types[i].name === name) {
                        $scope.type = types[i];
                        break;
                    }
                }
            }, function errorCallback(response) {
                $scope.error = response.data;
            });
}

function refreshProducts($scope, $state, $stateParams, $http, $cookies) {
    //Busca os produtos cadastrados
    $http.get($scope.config.stockUrl + 'product/category/' + $scope.product.category.id)
            .then(function successCallback(response) {
                $scope.products = response.data;
            }, function errorCallback(response) {
                $scope.error = response.data;
            });
}

angular.module('managerApp.controllers', []).controller('MainContoller', function ($scope, $state, $http, $stateParams, popupService, $window, $cookies) {
    preLoad($scope, $state, $http, $cookies);

}).controller('HomeController', function ($scope, $state, $cookies, $http) {
    preLoad($scope, $state, $http, $cookies);
    $scope.config.title = $scope.title + ' : Home';

//Obtem a data de expiração da licença do sistema
    $http.get($scope.config.checkoutUrl + 'payment/expirationDate')
            .then(function successCallback(response) {
                $scope.expirationDate = response.data;

            }, function errorCallback(response) {
                $scope.error = response.data.message;
                console.log(response.data.message);

            });

}).controller('LoginManagerController', function ($scope, $state, $stateParams) {

    $scope.formData = {};

    $scope.config.title = $scope.title + ' : Administrador';

}).controller('authenticateManager', function ($scope, $state, $http, $cookies) {

    $scope.submitForm = function () {
        $scope.error = undefined;
        $scope.sucess = undefined;

        $http.post($scope.config.accountUrl + 'authenticate/manager', JSON.stringify($scope.formData))
                .then(function successCallback(response) {
                    $cookies.putObject('managerToken', response.data.authenticationToken);
                    $state.go('home');

                }, function errorCallback(response) {
                    $scope.error = response.data;
                    console.log(JSON.stringify(response.data));

                });

    };

}).controller('logoutManagerController', function ($state, $scope, $http, $cookies, $rootScope) {
    $cookies.remove('managerToken');
    $state.go('login');
    window.location.reload();

}).controller('productController', function ($scope, $state, $stateParams, $http, $cookies) {
    $scope.product = {};
    $scope.product.id = 0;
    $scope.product.category = {};
    $scope.product.category.id = 1;
    $scope.category = {};

    //Título da página
    $scope.config.title = $scope.title + ' : Cadastro : Produto';

    getAllCategories($scope, $http);

    refreshProducts($scope, $state, $stateParams, $http, $cookies);

//    refreshTypes($scope, $state, $stateParams, $http, $cookies);

    var managerToken = $cookies.get('managerToken');
    $scope.saveProduct = function () {
        $scope.error = undefined;
        $scope.sucess = undefined;
//        product.price = product.price.toString().replace(",", ".");
        if ($scope.product.id === undefined) {
            $http.post($scope.config.stockUrl + 'product/' + managerToken, JSON.stringify($scope.product))
                    .then(function successCallback(response) {
                        $scope.product = response.data;
                        refreshProducts($scope, $state, $stateParams, $http, $cookies);
                    }, function errorCallback(response) {
                        $scope.error = {};
                        if (response.data.errors === undefined) {
                            $scope.error.message = response.data.message;
                        } else {
                            $scope.error.message = response.data.errors[0].message;
                        }
                    });
        } else {
            $http.put($scope.config.stockUrl + 'product/' + managerToken, JSON.stringify(product))
                    .then(function successCallback(response) {
                        $scope.products = response.data;
                        refreshProducts($scope, $state, $stateParams, $http, $cookies);
                        $scope.sucess = 'Produto alterado com êxito!';
                    }, function errorCallback(response) {
                        $scope.error = {};
                        if (response.data.errors === undefined) {
                            $scope.error.message = response.data.message;
                        } else {
                            $scope.error.message = response.data.errors[0].message;
                        }
                    });
        }
    };

    $scope.deleteProduct = function (product) {
        $scope.sucess = undefined;
        $scope.error = undefined;
        var category = $scope.product.category;
        if ($scope.product.id === undefined) {
            return;
        }
        $http.delete($scope.config.stockUrl + 'product/' + managerToken + '/' + product.id)
                .then(function successCallback(response) {
                    $scope.products = response.data;
                    $scope.sucess = 'Produto excluído com êxito!';
                    refreshProducts($scope, $state, $stateParams, $http, $cookies);
                }, function errorCallback(response) {
                    $scope.error = response.data;
                });
    };

    $scope.clearProduct = function (product) {
        $scope.sucess = undefined;
        $scope.error = undefined;
        $scope.product = {};
        $scope.product.categoryId = product.categoryId;
        $scope.product.category = product.category;
    };

    $scope.editProduct = function (product) {
        $scope.sucess = undefined;
        $scope.error = undefined;

        $scope.product = product;
        $scope.product.category.id = product.category.id.toString();

        getTypeByName($scope, $http, product.category.type);

    };

    $scope.loadProductsByCategory = function (categoryId) {
        $scope.sucess = undefined;
        $scope.error = undefined;
        $scope.product = {};
        $scope.product.category = {};
        $scope.product.category.id = categoryId;
        $http.get($scope.config.stockUrl + 'category/' + categoryId)
                .then(function successCallback(response) {
                    $scope.product.category = response.data;
                    $scope.product.category.id = $scope.product.category.id.toString();
                    refreshProducts($scope, $state, $stateParams, $http, $cookies);
                }, function errorCallback(response) {
                    $scope.error = response.data;
                });
    };

    $scope.newSku = function () {
        $scope.product.skus.push({'rows': {}});
    };

    $scope.newImage = function (sku) {
        for (i = 0; i < $scope.product.skus.length; i++) {
            if ($scope.product.skus[i].code === sku.code) {
                if ($scope.product.skus[i].images === undefined) {
                    $scope.product.skus[i].images = [];
                }
                $scope.product.skus[i].images.push('http://');
            }
        }
    };

    $scope.saveSku = function (sku) {
        $scope.error = undefined;
        $scope.sucess = undefined;
        sku.price = sku.price.toString().replace(",", ".");
        sku.product = {};
        sku.product.id = $scope.product.id;
        if (sku.id === undefined) {
            $http.post($scope.config.stockUrl + 'sku/' + managerToken, JSON.stringify(sku))
                    .then(function successCallback(response) {
                        $scope.product = response.data;
                        refreshProducts($scope, $state, $stateParams, $http, $cookies);
                        $scope.sucess = 'Produto incluído com êxito!';
                    }, function errorCallback(response) {
                        $scope.error = {};
                        if (response.data.errors === undefined) {
                            $scope.error.message = response.data.message;
                        } else {
                            $scope.error = {};
                            if (response.data.errors === undefined) {
                                $scope.error.message = response.data.message;
                            } else {
                                $scope.error.message = response.data.errors[0].message;
                            }
                        }
                    });
        } else {
            $http.put($scope.config.stockUrl + 'sku/' + managerToken, JSON.stringify(sku))
                    .then(function successCallback(response) {
                    }, function errorCallback(response) {
                        $scope.error = {};
                        if (response.data.errors === undefined) {
                            $scope.error.message = response.data.message;
                        } else {
                            $scope.error.message = response.data.errors[0].message;
                        }
                    });
        }

    };

}).controller('categoryController', function ($scope, $state, $stateParams, $http, $cookies) {
    //Título da página
    $scope.config.title = $scope.title + ' : Cadastro : Categoria';
    $scope.category = {};
    $scope.galleryImages = {};
    $scope.category.image = '';


    getAllCategories($scope, $http);
    getAllTypes($scope, $http);

    var managerToken = $cookies.get('managerToken');
//    var caminhoImagem;
    $scope.uploadFile = function (files) {
        var fd = new FormData();
        //Take the first selected file
        fd.append("file", files[0]);
        $http.post($scope.config.fileRepositoryUrl + 'uploadFile', fd, {
            headers: {'Content-Type': undefined},
            transformRequest: angular.identity})
                .then(function successCallback(response) {
                    $scope.category.image = response.data.url;
                }, function errorCallback(response) {
                    console.log('FALHA categoryController' + JSON.stringify(response.data));
                });
    };

    $scope.saveCategory = function () {
        var category = $scope.category;
        $scope.error = undefined;
        $scope.sucess = undefined;

        if (category.id === undefined) {

            $http.post($scope.config.stockUrl + 'category/' + managerToken, JSON.stringify(category))
                    .then(function successCallback(response) {
                        $scope.categories = response.data;
                        getAllCategories($scope, $http);
                        $scope.sucess = 'Categoria incluída com êxito!';
                    }, function errorCallback(response) {
                        $scope.error = response.data;
                    });
        } else {
            $http.put($scope.config.stockUrl + 'category/' + managerToken, JSON.stringify(category))
                    .then(function successCallback(response) {
                        $scope.categories = response.data;
                        getAllCategories($scope, $http);
                        $scope.sucess = 'Categoria alterada com êxito!';
                    }, function errorCallback(response) {
                        $scope.error = response.data;
                    });
        }

    };

    $scope.deleteCategory = function () {
        $scope.error = undefined;
        $scope.sucess = undefined;
        if ($scope.category.id === undefined) {
            return;
        }
        $http.delete($scope.config.stockUrl + 'category/' + customerToken + '/' + $scope.category.id)
                .then(function successCallback(response) {
                    $scope.categories = response.data;
                    $scope.sucess = 'Categoria excluída com êxito!';
                    getAllCategories($scope, $http);
                    $scope.category = {};
                }, function errorCallback(response) {
                    $scope.error = response.data;
                });
    };

    $scope.editCategory = function (category) {
        $scope.error = undefined;
        $scope.sucess = undefined;

        var layoutFile = category.layoutFile;

        $scope.category = category;
        for (i = 0; i < $scope.types.length; i++) {
            if ($scope.category.type === $scope.types[i].name) {
                $scope.layouts = $scope.types[i].layouts;
            }
        }
        $scope.category.layoutFile = layoutFile;
        //Recarrega para remover as alterações não salvas de outra categoria
        getAllCategories($scope, $http);
    };

    $scope.clearCategory = function () {
        $scope.category = {};
        $scope.category.image = '';
    };

    $scope.getLayouts = function () {
        for (i = 0; i < $scope.types.length; i++) {
            if ($scope.category.type === $scope.types[i].name) {
                $scope.layouts = $scope.types[i].layouts;
            }
        }
    };

    $scope.selectImage = function (image) {
        $scope.category.image = image.url;
    };

}).controller('galleryController', function ($scope, $state, $stateParams, $http, $cookies) {
    //Título da página
    $scope.config.title = $scope.title + ' : Galeria';
    $scope.image = {};
    $scope.selectedImages = [];

    getAllImages($scope, $state, $stateParams, $http, $cookies);

    $scope.saveImage = function () {
        $scope.error = undefined;
        $scope.sucess = undefined;
        var fd = new FormData();
        var image = $scope.image;

        if ((image.name === undefined)
                || image.name === '') {
            $scope.error = 'Favor preencher o nome da imagem!';
            return;
        } else if ((document.getElementById("imageFile").files[0] === undefined)) {
            $scope.error = 'Favor selecionar uma imagem!';
            return;
        }

        //Take the first selected file
        fd.append("file", document.getElementById("imageFile").files[0]);
        fd.append("name", image.name)
        $http.post($scope.config.fileRepositoryUrl + 'uploadFile', fd, {
            headers: {'Content-Type': undefined},
            transformRequest: angular.identity})
                .then(function successCallback(response) {
                    $scope.error = undefined;
                    $scope.sucess = 'Imagem incluída com êxito!';
                    $scope.image = {};
                    getAllImages($scope, $state, $stateParams, $http, $cookies);
                }, function errorCallback(response) {
                    $scope.sucess = undefined;
                    $scope.error = response.data.message;
                });
    };

    $scope.selectImage = function (image) {
        var teste = true;
        var teste2;
        for (i = 0; i < $scope.selectedImages.length; i++) {
            if ($scope.selectedImages[i].id === image.id) {
                teste = false;
                teste2 = $scope.selectedImages.indexOf(image);
            }
        }
        if (teste) {
            $scope.selectedImages.push(image);
        } else {
            $scope.selectedImages.splice(teste2, 1);
        }
    };

    $scope.deleteImage = function () {
        if (($scope.selectedImages === undefined)
                || ($scope.selectedImages.length === 0)) {
            return;
        }
        var idImages;
        for (i = 0; i < $scope.selectedImages.length; i++) {
            if (idImages === undefined) {
                idImages = $scope.selectedImages[i].id;
            } else {
                idImages = idImages + ',' + $scope.selectedImages[i].id;
            }
        }

        $http.delete($scope.config.fileRepositoryUrl + idImages)
                .then(function successCallback(response) {
                    $scope.categories = response.data;
                    $scope.error = undefined;
                    $scope.sucess = 'Imagem excluída com êxito!';
                    getAllImages($scope, $state, $stateParams, $http, $cookies);
                    $scope.selectedImages = [];
                }, function errorCallback(response) {
                    $scope.sucess = undefined;
                    $scope.error = response.data;
                });
    };


}).controller('popupGalleryController', function ($scope, $state, $stateParams, $http, $cookies) {
    //Título da página
    $scope.config.title = $scope.title + ' : Galeria';

    var MyCtrlDialog = function ($scope) {
        $scope.open = function () {
            $scope.showModal = true;
        };
        $scope.ok = function () {
            $scope.showModal = false;
        };
        $scope.cancel = function () {
            $scope.showModal = false;
        };
    };

}
).controller('financialController', function ($state, $scope, $http, $cookies, $rootScope) {
    var date = new Date();
    $scope.filterFormData = {};
    $scope.filterFormData.dtStart = new Date(date.getFullYear(), date.getMonth(), 1);
    $scope.filterFormData.dtEnd = new Date(date.getFullYear(), date.getMonth() + 1, 0);

    $scope.confirmPayment = function (formData) {
        $scope.error = undefined;
        $scope.sucess = undefined;
        var managerToken = $cookies.get('managerToken');
        $http.post($scope.config.checkoutUrl + "payment/" + managerToken + "/confirmPayment", formData)
                .then(function successCallback(response) {
                    $scope.order = response.data;
                    $scope.sucess = 'Pagamento confirmado com sucesso!';
                }, function errorCallback(response) {
                    $scope.error = response.data;
                    JSON.stringify(response.data);


                });
    };

    $scope.filter = function (formData) {
        $scope.error = undefined;
        $scope.sucess = undefined;
        JSON.stringify(formData);
        var managerToken = $cookies.get('managerToken');
        $http.get($scope.config.checkoutUrl + "order/" + managerToken + "/findSellersOrders?dtStart=" + formData.dtStart.toISOString().slice(0, 10) + "&dtEnd=" + formData.dtEnd.toISOString().slice(0, 10))
                .then(function successCallback(response) {
                    $scope.sellers = response.data;
                    $scope.stotalProducs = 0;
                    $scope.totalFreightCost = 0;
                    $scope.totalPriceProducts = 0;
                    for (i = 0; i < $scope.sellers.length; i++) {
                        $scope.sellers[i].sellerTotalFreightCost = 0;
                        $scope.sellers[i].sellerTotalPriceProducts = 0;
                        $scope.sellers[i].sellerTotalProducs = 0;
                        for (h = 0; h < $scope.sellers[i].ordersJson.length; h++) {
                            $scope.sellers[i].sellerTotalProducs += $scope.sellers[i].ordersJson[h].cart.totalQuantityProducts;
                            $scope.sellers[i].sellerTotalFreightCost += $scope.sellers[i].ordersJson[h].cart.freightCost;
                            $scope.sellers[i].sellerTotalPriceProducts += $scope.sellers[i].ordersJson[h].cart.totalPriceProducts;
                        }
                        $scope.stotalProducs += $scope.sellers[i].sellerTotalProducs;
                        $scope.totalFreightCost += $scope.sellers[i].sellerTotalFreightCost;
                        $scope.totalPriceProducts += $scope.sellers[i].sellerTotalPriceProducts;
                    }
                }, function errorCallback(response) {
                    $scope.error = response.data;
                    JSON.stringify(response.data);
                });
    };

}).controller('preparationController', function ($state, $scope, $http, $cookies, $rootScope) {
    var managerToken = $cookies.get('managerToken');
    $http.get($scope.config.checkoutUrl + "order/" + managerToken + "/findOrder?paid=true&orderBy=dtPaid")
            .then(function successCallback(response) {
                $scope.orders = response.data;
            }, function errorCallback(response) {
                $scope.error = response.data;
            });

    $scope.prepared = function (order) {
        $scope.error = undefined;
        $scope.sucess = undefined;
        var managerToken = $cookies.get('managerToken');
        var orderJson = {};
        orderJson.id = order.id;
        $http.post($scope.config.checkoutUrl + "order/" + managerToken + "/prepared", orderJson)
                .then(function successCallback(response) {
                    $scope.sucess = 'Pedido n° ' + orderJson.id + ' preparado com sucesso!';

                    $http.get($scope.config.checkoutUrl + "order/" + managerToken + "/findOrder?paid=true&orderBy=dtPaid")
                            .then(function successCallback(response) {
                                $scope.orders = response.data;
                            }, function errorCallback(response) {
                                $scope.error = response.data;
                            });

                }, function errorCallback(response) {
                    $scope.error = response.data;
                    JSON.stringify(response.data);
                });
    };

}).controller('sentController', function ($state, $scope, $http, $cookies, $rootScope) {
    var managerToken = $cookies.get('managerToken');
    $http.get($scope.config.checkoutUrl + "order/" + managerToken + "/findOrder?paid=true&packaged=true&orderBy=dtPackaged")
            .then(function successCallback(response) {
                $scope.orders = response.data;
            }, function errorCallback(response) {
                $scope.error = response.data;
            });

    $scope.sent = function (order) {
        $scope.error = undefined;
        $scope.sucess = undefined;
        var managerToken = $cookies.get('managerToken');
        var orderJson = {};
        orderJson.id = order.id;
        orderJson.shippingCode = order.shippingCode;
        $http.post($scope.config.checkoutUrl + "order/" + managerToken + "/sent", orderJson)
                .then(function successCallback(response) {
                    $scope.sucess = 'Pedido n° ' + orderJson.id + ' enviado com sucesso!';

                    $http.get($scope.config.checkoutUrl + "order/" + managerToken + "/findOrder?paid=true&packaged=true&orderBy=dtPackaged")
                            .then(function successCallback(response) {
                                $scope.orders = response.data;
                            }, function errorCallback(response) {
                                $scope.error = response.data;
                            });

                }, function errorCallback(response) {
                    $scope.error = response.data;
                    JSON.stringify(response.data);
                });
    };

});
