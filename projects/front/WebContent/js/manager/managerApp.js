angular.module('managerApp', ['ui.router', 'ngResource', 'ngCookies', 'managerApp.controllers', 'managerApp.services']);

angular.module('managerApp').config(function ($stateProvider, $locationProvider) {

    if (getConfig() === undefined || getConfig().dev !== true) {
        $locationProvider.html5Mode(true);
    }

    $stateProvider.state('home', {
        url: '/home',
        templateUrl: 'administrador/partials/home.html',
        controller: 'HomeController'

    }).state('loginManager', {
        url: '/login',
        templateUrl: 'administrador/partials/login.html',
        controller: 'LoginManagerController'

    }).state('category', {
        url: '/cadastro/categoria',
        templateUrl: 'administrador/partials/category.html',
        controller: 'categoryController'

    }).state('product', {
        url: '/cadastro/produto',
        templateUrl: 'administrador/partials/product.html',
        controller: 'productController'

    }).state('logoutManager', {
        url: '/logout',
        templateUrl: 'administrador/partials/login.html',
        controller: 'logoutManagerController'

    }).state('financial', {
        url: '/financeiro',
        templateUrl: 'administrador/partials/financial.html',
        controller: 'financialController'

    }).state('preparation', {
        url: '/preparacao',
        templateUrl: 'administrador/partials/preparation.html',
        controller: 'preparationController'

    }).state('sent', {
        url: '/envio',
        templateUrl: 'administrador/partials/sent.html',
        controller: 'sentController'
    }).state('gallery', {
        url: '/galeria',
        templateUrl: 'administrador/partials/gallery.html',
        controller: 'galleryController'
    });

}).run(function ($state, $http) {
    $state.go('loginManager');
    $http.defaults.headers.common['Authorization'] = '0'; //TODO: Ver de onde vai vir essa chave
});
