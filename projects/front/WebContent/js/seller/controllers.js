function preLoad($scope, $state, $http, $cookies) {
    if ($scope.config === undefined) {
        $scope.config = getConfig();
        $scope.title = $scope.config.title;
    }

    // validando login do vendedor
    var sellerToken = $cookies.getObject('sellerToken');
    if (sellerToken && $scope.config.accountUrl) {
        $http.get($scope.config.accountUrl + 'seller/token/' + sellerToken)
                .then(function successCallback(response) {
                    $scope.authenticatedSeller = response.data;
                }, function errorCallback(response) {
                    if (response.status === 410) {
                        $cookies.remove("sellerToken");
                        console.log("Autenticação expirada e removida");
                    } else {
                        $cookies.remove("sellerToken");
                        console.log("Autenticação inválida e removida");
                    }
                    window.location.reload();
                });
    }

    if (!$cookies.get('sellerToken')) {
        $state.go('loginSeller');
        return;
    }
}

angular.module('sellerApp.controllers', []).controller('MainContoller', function ($scope, $state, $http, $stateParams, popupService, $window, $cookies) {
    preLoad($scope, $state, $http, $cookies);

}).controller('HomeController', function ($scope, $state, $cookies, $http) {
    $scope.config.title = $scope.title + ' : Home';
    preLoad($scope, $state, $http, $cookies);

}).controller('LoginSellerController', function ($scope, $state, $stateParams) {
    $scope.formData = {};
    $scope.config.title = $scope.title + ' : Vendedor';

}).controller('authenticateSeller', function ($scope, $state, $http, $cookies) {
    $scope.submitForm = function () {
        console.log(JSON.stringify($scope.formData));

        $http.post($scope.config.accountUrl + 'authenticate/seller', JSON.stringify($scope.formData))
                .then(function successCallback(response) {
                    $cookies.putObject('sellerToken', response.data.authenticationToken);
                    console.log(response.data);
                    $state.go('mySalles');

                }, function errorCallback(response) {
                    $scope.error = response.data;
                    console.log(response.data);

                });

    };

}).controller('alabastrumFirstAccessController', function ($scope, $cookies, $http, $state) {
    $scope.formData = {};
    $scope.config.title = $scope.title + ' : Primeiro acesso';

    $scope.submitForm = function () {
        $http.post($scope.config.accountUrl + 'seller/importFromAlabstrum', JSON.stringify($scope.formData))
                .then(function successCallback(response) {
                    $cookies.putObject('sellerToken', response.data.authenticationToken);
                    $state.go('myAccout');

                }, function errorCallback(response) {
                    $scope.error = response.data;

                });

    };

}).controller('myAccountController', function ($scope, $cookies, $http, $state) {
    preLoad($scope, $state, $http, $cookies);
// validando login do vendedor
    var sellerToken = $cookies.getObject('sellerToken');
    if (sellerToken && $scope.config.accountUrl) {
        $http.get($scope.config.accountUrl + 'seller/token/' + sellerToken)
                .then(function successCallback(response) {
                    $scope.account = response.data;
                }, function errorCallback(response) {
                    $cookies.remove("sellerToken");
                    window.location.reload();
                });
    }

    $scope.submitForm = function (account) {
        var sellerToken = $cookies.getObject('sellerToken');
        $http.put($scope.config.accountUrl + 'seller/' + sellerToken, account)
                .then(function successCallback(response) {
                    $scope.sellerToken = response.data.authenticationToken;
                    $cookies.putObject('sellerToken', response.data.authenticationToken);
                    $scope.sucess = 'Cadastro alterado com sucesso!';

                }, function errorCallback(response) {
                    $scope.error = response.data;
                });
    };

}).controller('mySallesController', function ($state, $scope, $http, $cookies, $rootScope) {
    preLoad($scope, $state, $http, $cookies);
    var date = new Date();
    $scope.filterFormData = {};
    $scope.filterFormData.dtStart = new Date(date.getFullYear(), date.getMonth(), 1);
    $scope.filterFormData.dtEnd = new Date(date.getFullYear(), date.getMonth() + 1, 0);

    $scope.filter = function (formData) {
        // validando login do vendedor
        var sellerToken = $cookies.getObject('sellerToken');
        if (sellerToken && $scope.config.accountUrl) {
            $http.get($scope.config.checkoutUrl + "order/" + sellerToken + "/findSellerOrders?dtStart=" + formData.dtStart.toISOString().slice(0, 10) + "&dtEnd=" + formData.dtEnd.toISOString().slice(0, 10))
                    .then(function successCallback(response) {
                        $scope.seller = response.data;
                        $scope.seller.sellerTotalFreightCost = 0;                         $scope.seller.sellerTotalPriceProducts = 0;                         $scope.seller.sellerTotalProducs = 0;                         for (h = 0; h < $scope.seller.ordersJson.length; h++) {
                            $scope.seller.sellerTotalProducs += $scope.seller.ordersJson[h].cart.totalQuantityProducts;
                            $scope.seller.sellerTotalFreightCost += $scope.seller.ordersJson[h].cart.freightCost;
                            $scope.seller.sellerTotalPriceProducts += $scope.seller.ordersJson[h].cart.totalPriceProducts;
                        }
                    }, function errorCallback(response) {
                        $scope.error = response.data;
                    });
        }
    };

}).controller('logoutSellerController', function ($state, $scope, $http, $cookies, $rootScope) {
    $cookies.remove('sellerToken');
    window.location.reload();

});
