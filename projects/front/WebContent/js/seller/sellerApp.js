angular.module('sellerApp', ['ui.router', 'ngResource', 'ngCookies', 'sellerApp.controllers', 'sellerApp.services']);

angular.module('sellerApp').config(function ($stateProvider, $locationProvider) {

    if (getConfig() === undefined || getConfig().dev !== true) {
        $locationProvider.html5Mode(true);
    }

    $stateProvider.state('mySalles', {
        url: '/minhas-vendas',
        templateUrl: 'vendedor/partials/mySalles.html',
        controller: 'mySallesController'

    }).state('loginSeller', {
        url: '/login',
        templateUrl: 'vendedor/partials/login.html',
        controller: 'LoginSellerController'

    }).state('logoutSeller', {
        url: '/logout',
        templateUrl: 'partials/login.html',
        controller: 'logoutSellerController'

    }).state('alabastrumSellerFirtAccess', {
        url: '/primeiro-acesso',
        templateUrl: 'vendedor/partials/loginEV.html',
        controller: 'alabastrumFirstAccessController'

    }).state('myAccount', {
        url: '/minha-conta',
        templateUrl: 'vendedor/partials/myAccount.html',
        controller: 'LoginSellerController'
    });

}).run(function ($state, $http) {
    $state.go('mySalles');
    $http.defaults.headers.common['Authorization'] = '0'; //TODO: Ver de onde vai vir essa chave
});
