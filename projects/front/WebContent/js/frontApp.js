angular.module('frontApp', ['ui.router', 'ngResource', 'ngCookies', 'frontApp.controllers', 'frontApp.services']);

angular.module('frontApp').config(function ($stateProvider, $locationProvider, $httpProvider) {
//    $httpProvider.interceptors.push('httpRequestInterceptor');
    if (getConfig() === undefined || getConfig().dev !== true) {
        $locationProvider.html5Mode(true);
    }

    $stateProvider.state('home', {
        url: '/home',
        templateUrl: 'partials/home.html',
        controller: 'HomeController'

    }).state('catalog', {
        url: '/catalogo',
        templateUrl: 'partials/catalog.html',
        controller: 'CatalogController'

    }).state('viewCategory', {
        url: '/categoria/:id',
        templateUrl: 'partials/category.html',
        controller: 'CategoryController'

    }).state('viewProduct', {
        url: '/produto/:id',
        templateUrl: 'partials/product.html',
        controller: 'productViewController'

    }).state('createCustomer', {
        url: '/cadastre-se',
        templateUrl: 'partials/customer/createCustomer.html',
        controller: 'createCustomerController'

    }).state('forgotPassword', {
        url: '/esqueciasenha',
        templateUrl: 'partials/customer/forgotPassword.html',
        controller: 'forgotPasswordController'

    }).state('logoutCustomer', {
        url: '/logout',
        templateUrl: 'partials/home.html',
        controller: 'logoutCustomerController'

    }).state('cart', {
        url: '/carrinho',
        templateUrl: 'partials/cart.html',
        controller: 'CartViewController'

    }).state('myAccount', {
        url: '/minhaConta',
        templateUrl: 'partials/customer/myAccount.html',
        controller: 'myAccountController'

    }).state('myOrders', {
        url: '/meusPedidos',
        templateUrl: 'partials/customer/myOrders.html',
        controller: 'myOrdersController'

    }).state('order', {
        url: '/pedido/:id',
        templateUrl: 'partials/customer/order.html',
        controller: 'orderController'

                //sempre manter este mapeamento por ultimo
    }).state('findSeller', {
        url: '/:codOrNick',
        templateUrl: 'partials/catalog.html',
        controller: 'findSellerFromUrl'

    });

}).run(function ($state, $http) {
    $state.go('home');
    $http.defaults.headers.common['Authorization'] = '0'; //TODO: Ver de onde vai vir essa chave
});
