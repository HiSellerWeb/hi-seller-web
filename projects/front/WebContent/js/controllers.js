angular.module('frontApp.controllers', ['ngSanitize']).controller('MainContoller', function ($scope, $state, $http, $cookies, $stateParams, popupService, $window) {
    preLoad($scope, $state, $http, $cookies);

}).controller('addToCartController', function ($scope, $state, $http, $cookies, $rootScope) {
    $scope.submitForm = function (skuId) {
        //Só cria um carrinho se ja não tem um criado no cookie
        var formData = {};
        formData.customerId = 0;
        formData.sellerId = 0;

        if ($scope.customer) {
            formData.customerId = $scope.customer.id;
        }

        var seller = $cookies.getObject('seller');
        if (seller) {
            formData.sellerId = seller.id;
        }

        if ($cookies.getObject('cart') === undefined) {

            //Cria o carrinho para o usuário
            $http.post($scope.config.checkoutUrl + 'cart', JSON.stringify(formData))
                    .then(function successCallback(response) {
                        $cookies.putObject('cart', response.data, {'path': '/'});
                        addProductToCart(skuId, $http, $cookies, $scope, $rootScope);
                    }, function errorCallback(response) {
                        showMsg($rootScope, 'Falha ao criar o carrinho. Se o erro persistir, contate o administrador.');
                        console.log(JSON.stringify(response.data));
                    });
        } else {
            addProductToCart(skuId, $http, $cookies, $scope, $rootScope);
        }

    };


}).controller('findSeller', function ($scope, $state, $http, $cookies) {

    $scope.submitForm = function () {
        $http.get($scope.config.accountUrl + 'seller/' + $scope.form.id)
                .then(function successCallback(response) {
                    $cookies.putObject('seller', response.data);
                    $state.go('catalog'); //State
                }, function errorCallback(response) {
                    if (response.status === 404) {
                        $scope.error = response.data;
                    }
                });

    };

}).controller('findSellerFromUrl', function ($scope, $state, $http, $cookies, $stateParams) {
    var codeOrNick = '';
    if ($stateParams !== undefined && $stateParams.codOrNick !== undefined) {
        codeOrNick = $stateParams.codOrNick;
        if (codeOrNick === '') {
            $state.go('home');
        }
    } else {
        $state.go('home');
    }

    $http.get($scope.config.accountUrl + 'seller/' + codeOrNick)
            .then(function successCallback(response) {
                $cookies.putObject('seller', response.data);
                console.log('sucesso: ' + JSON.stringify(response.data));
                $state.go('catalog'); //State
            }, function errorCallback(response) {
                console.log('error: ' + JSON.stringify(response.data));
                if (response.status === 404) {
                    $scope.error = response.data;
                }
            });

}).controller('CatalogController', function ($scope, $cookies, $state, $http) {
    //Catálogo
    preLoad($scope, $state, $http, $cookies);

    $scope.config.title = $scope.title + ' : Catálogo';
    $scope.seller = $cookies.getObject('seller');

    $http.get($scope.config.stockUrl + 'category')
            .then(function (response) {
                $scope.categories = response.data;
            });

}).controller('CategoryController', function ($scope, $state, $http, $cookies, $stateParams) {
    preLoad($scope, $state, $http, $cookies);
    $http.get($scope.config.stockUrl + 'category/' + $stateParams.id)
            .then(function (response) {
                $scope.category = response.data;
                $scope.config.title = $scope.title + ' : ' + $scope.category.name;
            });

    $http.get($scope.config.stockUrl + 'product/category/' + $stateParams.id)
            .then(function successCallback(response) {
                $scope.products = response.data;
                console.log('Produtos: ' + JSON.stringify($scope.products));

            }, function errorCallback(response) {
                $scope.error = response.data;
                console.log('Erro ao carregar os produtos: ' + JSON.stringify($scope.error));
            });

}).controller('productViewController', function ($scope, $state, $http, $cookies, $stateParams) {
    preLoad($scope, $state, $http, $cookies);
    $http.get($scope.config.stockUrl + 'product' + $stateParams.id).then(function successCallback(response) {
        $scope.product = response.data;
        $scope.config.title = $scope.title + ' : ' + $scope.product.name;

    }, function errorCallback(response) {
        $scope.error = response.data;
    });

}).controller('updateCartController', function ($scope, $state, $http, $cookies, $rootScope) {

    $scope.submitForm = function (product) {

        //Só cria um carrinho se ja não tem um criado no cookie
        formData = {};
        formData.customerId = 0;

        if ($scope.customer) {
            formData.customerId = $scope.customer.id;
        }

        if ($cookies.getObject('cart') === undefined) {

            //Cria o carrinho para o usuário
            $http.post($scope.config.checkoutUrl + 'cart', JSON.stringify(formData))
                    .then(function successCallback(response) {
                        $cookies.putObject('cart', response.data);
                        changeProductQuantity(product, $http, $cookies, $scope, $rootScope);
                    }, function errorCallback(response) {
                        $scope.error = response.data;
                        console.log(response.data);
                    });
        } else {
            changeProductQuantity(product, $http, $cookies, $scope, $rootScope);
        }

    };

}).controller('LoginCustomerController', function ($scope) {
    $scope.formData = {};

}).controller('authenticateCustomer', function ($scope, $http, $cookies, $rootScope, $state) {
    $scope.submitForm = function () {
        $http.post($scope.config.accountUrl + 'authenticate/customer', JSON.stringify($scope.formData))
                .then(function successCallback(response) {
                    $cookies.put('customerToken', response.data.authenticationToken);

                    var cart = $cookies.getObject('cart');

                    if (cart !== undefined) {
                        var formData = {};
                        formData.authenticationToken = response.data.authenticationToken;
                        $http.put($scope.config.checkoutUrl + 'cart/' + cart.encryptId + '/customer/', JSON.stringify(formData));
                    }
                    window.location.reload();

                }, function errorCallback(response) {
                    $scope.error = response.data;
                    console.log(JSON.stringify(response.data));

                });

    };

}).controller('logoutCustomerController', function ($state, $scope, $http, $cookies, $rootScope) {
    $cookies.remove('customerToken');
    $state.go('home');
    window.location.reload();

}).controller('forgotPasswordController', function ($state, $scope, $http, $cookies, $rootScope) {

    $scope.submitForm = function (account) {
        $('#sendButton').attr("disabled", true);
        $scope.error = undefined;
        $scope.sucess = undefined;

        if (account.nickName === undefined) {
            $scope.error = 'Favor preencher o nickname!';
            $('#sendButton').attr("disabled", false);
            return;
        }

        $http.get($scope.config.accountUrl + 'customer/forgotPassword/' + account.nickName)
                .then(function successCallback(response) {
                    $scope.account = response.data;
                    $scope.sucess = 'Uma senha provisória foi enviada ao e-mail viculado a conta!';
                    console.log('SUCESSO forgotPasswordController: ' + JSON.stringify(response.data));
                }, function errorCallback(response) {
                    $scope.error = response.data.message;
                    $scope.account.password = undefined;
                    console.log('FALHA forgotPasswordController' + JSON.stringify(response.data));
                    $('#sendButton').attr("disabled", false);
                });

    };

}).controller('CartViewController', function ($scope, $state, $http, $cookies, $stateParams, $rootScope) {
    preLoad($scope, $state, $http, $cookies);
    $scope.address = {};

    $scope.config.title = $scope.title + ' : Carrinho';

    var cart = $cookies.getObject('cart');

    if (cart === undefined) {
        cart = {};
    } else {
        $http.get($scope.config.checkoutUrl + 'cart/' + cart.encryptId + '/fullCart')
                .then(function successCallback(response) {
                    $scope.cart = response.data;

                    var customerToken = $cookies.get('customerToken');
                    if (customerToken && $scope.config.accountUrl) {

                        $http.get($scope.config.accountUrl + 'address/customer/' + customerToken)
                                .then(function successCallback(response) {
                                    $scope.addresses = response.data;
                                    $scope.address = response.data[0];
                                    if ($scope.address !== undefined) {
                                        $http.get($scope.config.checkoutUrl + 'freight/' + $scope.address.zipCode + '/cart/' + cart.encryptId)
                                                .then(function successCallback(response) {
                                                    $scope.cart.freightCost = response.data.pac;

                                                }, function errorCallback(response) {
                                                    $scope.error = response.data;

                                                });
                                    }


                                }, function errorCallback(response) {
                                    console.log(JSON.stringify(response.data));
                                });
                    }

                }, function errorCallback(response) {

                    $scope.error = response.data;
                    console.log(JSON.stringify(response.data));

                });
    }

    $scope.completeAddress = function () {

        //Nova variável "cep" somente com dígitos.
        var cep = $scope.address.zipCode.replace(/\D/g, '');
        //Verifica se campo cep possui valor informado.
        if (cep !== "") {

            //Expressão regular para validar o CEP.
            var validacep = /^[0-9]{8}$/;
            //Valida o formato do CEP.
            if (validacep.test(cep)) {

                //Preenche os campos com "..." enquanto consulta webservice.
                $scope.address.street = 'buscando...';
                $scope.address.neighborhood = 'buscando...';
                $scope.address.city = 'buscando...';
                $scope.address.uf = '';

                //Consulta o webservice viacep.com.br/
                $.getJSON("//viacep.com.br/ws/" + cep + "/json/?callback=?", function (dados) {

                    if (!("erro" in dados)) {
                        //Atualiza os campos com os valores da consulta.
                        $scope.address.street = dados.logradouro;
                        $scope.address.neighborhood = dados.bairro;
                        $scope.address.city = dados.localidade;
                        $scope.address.uf = dados.uf.toUpperCase();
                    } else {
                        //CEP pesquisado não foi encontrado.
                        limpa_formulário_cep($scope);
                        alert("CEP não encontrado.");
                    }
                });

                $http.get($scope.config.checkoutUrl + 'freight/' + cep + '/cart/' + cart.encryptId)
                        .then(function successCallback(response) {
                            $scope.cart.freightCost = response.data.pac;
                        }, function errorCallback(response) {
                            $scope.error = response.data;
                        });

            } else if (cep.length < 8) {
                return;
            } else {
                //cep é inválido.
                limpa_formulário_cep($scope);
                alert("Formato de CEP inválido.");
            }
        } else {
            //cep sem valor, limpa formulário.
            limpa_formulário_cep($scope);
            $scope.address.city = 'RJ';
        }
    };

    $scope.updateCart = function (sku) {
        changeSkuQuantity(sku, $http, $cookies, $scope, $rootScope);
        var cep = $scope.address.zipCode.replace(/\D/g, '');
        $http.get($scope.config.checkoutUrl + 'freight/' + cep + '/cart/' + cart.encryptId)
                .then(function successCallback(response) {
                    $scope.cart.freightCost = response.data.pac;
                }, function errorCallback(response) {
                    $scope.error = response.data;
                });
    };

    $scope.removeToCart = function (product) {
        product.productQuantity = 0;
        changeProductQuantity(product, $http, $cookies, $scope, $rootScope);
        var cep = $scope.address.zipCode.replace(/\D/g, '');
        $http.get($scope.config.checkoutUrl + 'freight/' + cep + '/cart/' + cart.encryptId)
                .then(function successCallback(response) {
                    $scope.cart.freightCost = response.data.pac;
                }, function errorCallback(response) {
                    $scope.error = response.data;
                });
    };

}).controller('paymentController', function ($scope, $http, $cookies, $state) {
    $scope.tiketPayment = function (address) {
        var cart = $cookies.getObject('cart');
        var customerToken = $cookies.get('customerToken');
        var formData = {};
        formData.address = address;
        formData.authenticationToken = customerToken;
        $http.post($scope.config.checkoutUrl + 'cart/' + cart.encryptId + '/tiketPayment', JSON.stringify(formData))
                .then(function successCallback(response) {
                    $state.go('order', {id: response.data.orderToken});

                }, function errorCallback(response) {
                    $scope.error = response.data;
                });
    };

    $scope.pagSeguroPayment = function (address) {
        var cart = $cookies.getObject('cart');
        var customerToken = $cookies.get('customerToken');
        var formData = {};
        formData.address = address;
        formData.authenticationToken = customerToken;
        $http.post(getConfig().checkoutUrl + 'cart/' + cart.encryptId + '/pagSeguroPayment', JSON.stringify(formData))
                .then(function successCallback(response) {
                    $state.go('order', {id: response.data.orderToken});
                }, function errorCallback(response) {
                    $scope.error = response.data;
                });
    };


}).controller('myAccountController', function ($scope, $http, $cookies, $sce) {
    var customerToken = $cookies.get('customerToken');

    //Título da página
    $scope.config.title = $scope.title + ' : Meu cadaaaaaastro';
    $scope.account = {};

    //Busca os dados usuário
    $http.get($scope.config.accountUrl + 'customer/' + customerToken)
            .then(function successCallback(response) {
                $scope.account = response.data;
                console.log('SUCESSO myAccountController: ' + JSON.stringify(response.data));
            }, function errorCallback(response) {
                $scope.error = response.data;
                console.log('FALHA myAccountController' + JSON.stringify(response.data));
            });

    $scope.submitForm = function (account) {
        $scope.error = "";
        $scope.sucess = "";

        if (account.newPassword !== undefined) {
            if (account.confirmPassword === undefined) {
                $scope.error = 'Favor preencher todos os campos para alterar a senha.';
                return;
            }
            if (account.newPassword !== account.confirmPassword) {
                $scope.error = 'Nova senha não é igual a confirmação de senha!';
                $scope.account.newPassword = undefined;
                $scope.account.confirmPassword = undefined;
                return;
            }
        }

        $http.put($scope.config.accountUrl + 'customer/' + customerToken + '/' + account.newPassword, JSON.stringify(account))
                .then(function successCallback(response) {
                    $scope.account = response.data;
                    $scope.sucess = 'Dados alterados com êxito!';
                    console.log('SUCESSO myAccountController: ' + JSON.stringify(response.data));
                }, function errorCallback(response) {
                    if (response.data.httpStatusCode === 422) {
                        var mensage = '';
                        for (i = 0; i < response.data.errors.length; i++) {
                            mensage += response.data.errors[i].message + ";<br>";
                        }
                        $scope.error = $sce.trustAsHtml(mensage);
                    } else {

                        $scope.error = response.data.message;
                    }
                    $scope.account.password = undefined;
                    console.log('FALHA myAccountController' + JSON.stringify(response.data));
                }
                );
    };
}
).controller('myOrdersController', function ($state, $scope, $http, $cookies) {
    preLoad($scope, $state, $http, $cookies);
    $scope.config.title = $scope.title + ' : Meus Pedidos';

    var customerToken = $cookies.get('customerToken');

    if (customerToken !== undefined) {
        $http.get($scope.config.checkoutUrl + 'order/' + customerToken)
                .then(function successCallback(response) {
                    var orders = response.data;
                    JSON.stringify(orders);
                    for (i = 0; i < orders.length; i++) {
                        orders[i].status = 'Aguardando Pagamento';
                        orders[i].class = 'warning';
                        if (orders[i].paid) {
                            orders[i].status = 'Em preparação';
                            orders[i].class = 'active';
                        }
                        if (orders[i].packaged) {
                            orders[i].status = 'Aguardando envio';
                            orders[i].class = 'active';
                        }
                        if (orders[i].sent) {
                            orders[i].status = 'Enviado';
                            orders[i].class = 'active';
                        }
                        if (orders[i].delivered) {
                            orders[i].status = 'Entregue';
                            orders[i].class = 'success';
                        }
                        if (orders[i].canceled) {
                            orders[i].status = 'Cancelado';
                            orders[i].class = 'danger';
                        }
                        $scope.orders = orders;
                    }
                }, function errorCallback(response) {
                    $scope.error = response.data.message;
                });
    }

}).controller('orderController', function ($state, $scope, $http, $cookies, $stateParams) {
    preLoad($scope, $state, $http, $cookies);
    $scope.config.title = $scope.title + ' : Meu Pedido';
    var customerToken = $cookies.get('customerToken');

    if ($stateParams !== undefined && $stateParams.id !== undefined && customerToken !== undefined) {
        var orderId = $stateParams.id;

        $http.get($scope.config.checkoutUrl + 'order/' + customerToken + '/' + orderId)
                .then(function successCallback(response) {
                    $scope.order = response.data;
                    $scope.address = $scope.order.deliveryAddress;
                    if ($scope.order.paymentType === 'TICKET') {
                        $scope.boleto = $scope.config.checkoutUrl + 'order/' + $scope.config.storeId + '/' + customerToken + '/' + orderId + '/viewTiket';
                    }
                    if ($scope.order.paymentType === 'PAG_SEGURO') {
                        $scope.pagSeguro_checkout = $scope.config.pagSeguro_checkout + $scope.order.transaction;
                    }
                }, function errorCallback(response) {
                    $scope.error = response.data;
                });
    }

}).controller('createCustomerController', function ($scope, $cookies, $http, $state, $sce) {
    preLoad($scope, $state, $http, $cookies);
    $scope.config.title = $scope.title + ' : Criar Conta';

    $scope.submitForm = function (account) {
        $http.post($scope.config.accountUrl + 'customer', account)
                .then(function successCallback(response) {
                    $cookies.put('customerToken', response.data.authenticationToken);
                    window.location.reload();
                    $state.go('home');

                    var cart = $cookies.getObject('cart');
                    if (cart !== undefined) {
                        var formData = {};
                        formData.authenticationToken = response.data.authenticationToken;
                        $http.put($scope.config.checkoutUrl + 'cart/' + cart.encryptId + '/customer/', JSON.stringify(formData))
                                .then(function successCallback(response) {
                                    console.log('Carrinho atualizado: ' + JSON.stringify(response.data));
                                }, function errorCallback(response) {
                                    console.log('Erro ao atualizar o carrinho: ' + JSON.stringify(response));

                                    $cookies.remove('customerToken');
                                    $state.go('home');
                                    window.location.reload();
                                });
                    }
                }, function errorCallback(response) {
                    if (response.data.httpStatusCode === 422) {
                        var mensage = '';
                        for (i = 0; i < response.data.errors.length; i++) {
                            mensage += response.data.errors[i].message + ";<br>";
                        }
                        $scope.error = $sce.trustAsHtml(mensage);
                    } else {
                        $scope.error = response.data.message;
                    }

                });
    };
});