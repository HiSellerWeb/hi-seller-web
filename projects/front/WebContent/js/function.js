function preLoad($scope, $state, $http, $cookies) {
    if ($scope.config === undefined) {
        $scope.config = getConfig();
        $scope.title = $scope.config.title;
    }
    //Obtendo cookies
    var cart = $cookies.getObject('cart');
    var customerToken = $cookies.get('customerToken');

    //Validando carrinho
    if (cart && cart.encryptId && $scope.config.checkoutUrl) {
        $http.get($scope.config.checkoutUrl + 'cart/' + cart.encryptId)
                .then(function successCallback(response) {
                    $cookies.putObject('cart', response.data);
                    if ((response.data.costumerId === undefined || response.data.costumerId === 0) && customerToken) {
                        var formData = {};
                        formData.authenticationToken = customerToken;
                        $http.put($scope.config.checkoutUrl + 'cart/' + cart.encryptId + '/customer/', JSON.stringify(formData));
                    }
                }, function errorCallback(response) {
                    if (response.status === 410) {
                        $cookies.remove("cart");
                    } else if (response.status === 404) {
                        $cookies.remove("cart");
                    }
                });
    }

    // validando login usuário
    if (customerToken && $scope.config.accountUrl) {
        $http.get($scope.config.accountUrl + 'customer/' + customerToken)
                .then(function successCallback(response) {
                    $cookies.put('customerToken', response.data.authenticationToken);
                    $scope.customer = response.data;
                }, function errorCallback(response) {
                    console.log(JSON.stringify(response.data));
                    if (response.status === 410) {
                        $cookies.remove("customerToken");
                        console.log("Autenticação expirada e removida");
                    } else {
                        $cookies.remove("customerToken");
                        console.log("Autenticação inválida e removida");
                    }
                    window.location.reload();
                });
    }

    $scope.seller = $cookies.get('seller');
    if (!$scope.seller && !isBot()) {
        $state.go('home');
        return;
    }

}

function limpa_formulário_cep($scope) {
    $scope.address.street = '';
    $scope.address.neighborhood = '';
    $scope.address.city = '';
    $scope.address.state = '';
}

function setCustomerToCart($http, $scope, $rootScope, cart, authenticationToken) {
    var formData = {};
    formData.authenticationToken = authenticationToken;
    $http.put($scope.config.checkoutUrl + 'cart/' + cart.encryptId + '/customer/', JSON.stringify(formData))
            .then(function successCallback(response) {
                console.log('Carrinho atualizado: ' + JSON.stringify(response.data));
            }, function errorCallback(response) {
                console.log('Erro ao atualizar o carrinho: ' + JSON.stringify(response));
            });
}

function showMsg($rootScope, title, body) {
    $rootScope.msg = {};
    $rootScope.msg.title = title;
    $rootScope.msg.body = body;
    $('#msgModal').modal('show');
}

function addProductToCart(skuId, $http, $cookies, $scope, $rootScope) {
    var formData = {};
    formData.id = skuId;
    formData.quantity = 1;
    $http.post($scope.config.checkoutUrl + 'cart/' + $cookies.getObject('cart').encryptId, JSON.stringify(formData))
            .then(function successCallback(response) {
                showMsg($rootScope, 'Produto adicionado!', 'O produto foi adicionado com sucesso ao seu carrinho de compras');
            }, function errorCallback(response) {
                if (response.status === 404) {
                    showMsg($rootScope, 'Carrinho expirado!', 'Seu carrinho expirou devido ao tempo de inatividade no site...');
                    $cookies.remove('cart');
                } else if (response.status === 409) {
                    showMsg($rootScope, 'Produto já adicionado!', 'Este produto já foi adicionado ao carrinho, parar alterar a quantidade vá ao seu Carrinho de compras');
                } else if (response.status === 412) {
                    showMsg($rootScope, 'Produto sem estoque!', 'Este produto está sem estoque no momento, tente novamente em alguns minutos, se alguém desitir da compra ele ainda pode ser seu :).');
                } else if (response.status === 403) {
                    showMsg($rootScope, 'Sistema em manutenção!', 'O sistema está em manutenção no momento, por favor tente novamente mais tarde.');
                } else {
                    showMsg($rootScope, 'Item não adicionado!', 'Não foi possível adicionar o produto ao carrinho, por favor tente novamente.');
                }
            });
}

function changeSkuQuantity(sku, $http, $cookies, $scope, $rootScope) {
    $http.put($scope.config.checkoutUrl + 'cart/' + $cookies.getObject('cart').encryptId, JSON.stringify(sku))
            .then(function successCallback(response) {
                showMsg($rootScope, 'Produto atualizado com sucesso!', 'A quantidade do produto foi alterada com sucesso!');
                window.location.reload();
            }, function errorCallback(response) {
                if (response.status === 404) {
                    showMsg($rootScope, 'Carrinho expirado!', 'Seu carrinho expirou devido ao tempo de inatividade no site.');
                } else if (response.status === 409) {
                    showMsg($rootScope, 'Produto já adicionado!', 'Este produto já foi adicionado ao carrinho, parar alterar a quantidade vá ao seu Carrinho de compras');
                } else {
                    showMsg($rootScope, 'Item não adicionado!', 'Não foi possível adicionar o produto ao carrinho, por favor tente novamente.');
                    console.log(JSON.stringify(response.data));
                }
                window.location.reload();
            });
}

function isBot() {
    return /bot|googlebot|crawler|spider|robot|crawling/i.test(navigator.userAgent);
}