angular.module('frontApp.services', []).factory('Seller', function ($resource) {
    return $resource('http://localhost:8082/Account/seller/:id', {id: '@_id'}, {
        update: {
            method: 'PUT'
        }
    });
}).service('popupService', function ($window) {
    this.showPopup = function (message) {
        return $window.confirm(message);
    }
});
