function getConfig() {
    var config = {};
    config.title = "HI Seler Web";
    config.accountUrl = "http://localhost:8080/account/";
    config.checkoutUrl = "http://localhost:8080/checkout/";
    config.stockUrl = "http://localhost:8080/stock/";
    config.fileRepositoryUrl = "http://localhost:8080/fileRepository/";
    config.pagSeguro_checkout = "https://sandbox.pagseguro.uol.com.br/v2/checkout/payment.html?code=";
    config.storeId = 0;
    config.dev = true;

    return config;
}