<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator"%>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt_br" data-ng-app="frontApp"  ng-controller="MainContoller">
    <head>
        <link rel="icon" type="image/png" href="<c:url value="/img/favicon.ico"/>"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <meta name="description" content="Conheça a Alabastrum, a empresa de Marketing Multinível que vai mudar a sua vida!"/>
        <meta name="keywords" content="Alabastrum, costmeticos, rio de janeiro, qualidade, marketing, Multinivel"/>
        <meta name="author" content="HI Seller Web"/>
        <meta name="google-site-verification" content="wIuWB2Gvm-Jtann2cKTl0SB7kpTTGYWA3KziHz5tBQ0"/>
        <meta name="revisit-after" content="1"/>
        <meta name="fragment" content="!"/>

        <title>${title}</title>

        <link rel="stylesheet" href="<c:url value="/css/bootstrap.min.css"/>"/>
        <link rel="stylesheet" href="<c:url value="/css/font-awesome.min.css"/>"/>
        <link rel="stylesheet" href="<c:url value="/css/custom.css"/>"/>

        <sitemesh:write property='head'/>
    </head>
    <body>

        <!--Google Analytcs para a Loja Alabastrum-->
        <script>
            (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                    m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
            ga('create', 'UA-75340522-1', 'auto');
            ga('send', 'pageview');
        </script>

        <!--login flutuante Modal-->
        <div id="loginModal" class="fade modal" role="dialog" ng-controller="LoginCustomerController">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Autenticação</h4>
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal" ng-submit="submitForm()" ng-controller="authenticateCustomer">
                            <div class="form-group">
                                <div ng-if="error.message" class="alert alert-danger" role="alert" style="width: 90%; margin: auto; margin-bottom: 5px; padding: 6px 10px">
                                    <p id="error">{{error.message}}</p>
                                </div>
                                <input type="text" class="form-control header_login" placeholder="E-mail" required="" autofocus="" ng-model="formData.email" style="width: 90%; margin: auto; margin-bottom: 2px;"/>
                                <input type="password" class="form-control header_login" placeholder="Senha" required="" ng-model="formData.password" style="width: 90%; margin: auto"/>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" class="btn btn-default" id="header_form_ok">Acessar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <p class="text-left">
                            <a ui-sref="forgotPassword()" data-dismiss="modal">Esqueci minha senha</a>
                        </p>
                        <p></p>
                        <p class="text-left">
                            <a ui-sref="createCustomer()" data-dismiss="modal">Criar Conta</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <!--MSG Modal-->
        <div id="msgModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">{{msg.title}}</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <p class="text-center">{{msg.body}}</p>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-default" id="header_form_ok" data-dismiss="modal">OK</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Fixed navbar -->
        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="${ENDPOINT}"><img src="<c:url value="/loja/logo_horizontal.png"/>" height="200%"></a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <!--<li><a href="#/contact"><i class="fa fa-comments"></i> Contato</a></li>-->
                        <li>
                            <a ui-sref="catalog()"><i class="fa fa-barcode"></i> Categorias</a>
                        </li>
                        <li>
                            <a href="${ENDPOINT}carrinho"><i class="fa fa-shopping-cart"></i> Carrinho</a>
                        </li>
                        <li ng-if="!customer">
                            <a href="#" data-toggle="modal" data-target="#loginModal"><i class="fa fa-sign-in"></i> Login</a>
                        </li>
                        <li ng-if="customer">
                            <a ui-sref="myAccount()"><i class="fa fa-user"></i> Minha Conta ({{customer.nickName}})</a>
                        </li>
                        <li ng-if="customer">
                            <a ui-sref="logoutCustomer()"><i class="fa fa-sign-out"></i> Sair</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="container theme-showcase" role="main" id="main">
            <sitemesh:write property='body'/>
        </div>
        <footer class="text-center">
            <a href="/vendedor" target="vendedor">Login vendedor</a>||
            <a href="/administrador" target="administrador">Login administrador</a>
        </footer>
        <script type="text/javascript" src="<c:url value="/js/jquery-1.12.0.min.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/js/config/config.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/js/function.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/js/angular.min.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/js/angular-ui-router.min.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/js/angular-sanitize.min.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/js/angular-resource.min.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/js/angular-cookies.min.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/js/bootstrap.min.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/js/frontApp.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/js/controllers.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/js/services.js"/>"></script>
    </body></html>