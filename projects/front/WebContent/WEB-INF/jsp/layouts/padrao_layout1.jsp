<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="jumbotron" style="overflow: hidden;">
    <div class="row">
        <div class="col-md-4">
            <img src="${category.image}" width="90%" class="img-rounded">
        </div>
        <div class="col-md-8">
            <h2>${category.name}</h2>
            <p>${category.description}</p>
        </div>
    </div>
    <hr>
    <ol class="list-unstyled">
        <c:forEach items="${products}" var="product">
            <ol class="list-unstyled">
                <c:forEach items="${product.skus}" var="sku">
                    <li>
                        <div class="row">
                            <div class="col-md-3">
                                <div id="carousel-${sku.id}" data-interval="false" class="carousel slide" data-ride="carousel">
                                    <div class="carousel-inner">
                                        <c:forEach items="${sku.images}" var="image">
                                            <div class='item ${sku.images[0] == image ? 'active' : ''}'>
                                                <img src=${image} class="img-rounded">
                                            </div>
                                        </c:forEach>
                                    </div>
                                    <a class="left carousel-control" href="#carousel-${sku.id}" data-slide="prev"><i class="icon-prev  fa fa-angle-left"></i></a>
                                    <a class="right carousel-control" href="#carousel-${sku.id}" data-slide="next"><i class="icon-next fa fa-angle-right"></i></a>
                                </div>
                            </div>
                            <div class="col-md-9">
                                <h2>${product.name} ${sku.name}</h2>
                                <dl>
                                    <dt>Código:</dt>
                                    <dd>${sku.code}</dd>
                                    <dt>Descrição:</dt>
                                    <dd>${product.shotDescription}</dd>
                                    <dt>Valor unitário:</dt>
                                    <dd>R$ ${sku.price}</dd>
                                </dl>
                                <form class="form-signin" name="${sku.id}" ng-submit="submitForm(${sku.id})" ng-controller="addToCartController">
                                    <button class="btn btn-sm btn-success" type="submit">Comprar</button>
                                </form>
                            </div>
                        </div>
                        <hr>
                    </li>
                </c:forEach>
            </ol>
        </c:forEach>
        </li>
    </ol>
</div>