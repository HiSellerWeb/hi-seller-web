<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="jumbotron" style="overflow: hidden;">
    <div class="row">
        <div class="col-md-4">
            <img src="${category.image}" width="90%" class="img-rounded">
        </div>
        <div class="col-md-8">
            <h2>${category.name}</h2>
            <p>${category.description}</p>
        </div>
    </div>
    <hr>
    <table class="table table-responsive">
        <thead>
            <tr>
                <th>Código</th>
                <th>Sexo</th>
                <th>Inspiração</th>
                <th>Tipo</th>
                <th>Intencidade</th>
                <th>Ml</th>
                <th>Valor Unit.</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <c:forEach items="${products}" var="product">
                <c:forEach items="${product.skus}" var="sku">

                    <tr>
                        <td>${sku.code}</td>
                        <td>${sku.rows.gender}</td>
                        <td>${sku.rows.inspiration}</td>
                        <td>${sku.rows.type}</td>
                        <td>${sku.rows.intensity}</td>
                        <td>${sku.rows.size}</td>
                        <td>${sku.price}</td>
                        <td>
                            <form class="form-signin" name="${sku.id}" ng-submit="submitForm(${sku.id})" ng-controller="addToCartController">
                                <button class="btn btn-sm btn-success" type="submit">Comprar</button>
                            </form>
                        </td>
                    </tr>

                </c:forEach>
            </c:forEach>
        </tbody>
    </table>
</div>