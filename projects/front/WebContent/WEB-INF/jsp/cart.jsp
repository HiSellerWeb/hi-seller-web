<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="container-fluid" ng-controller="CartViewController">
    <c:choose>
        <c:when test="${cart != null}">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        <div class="row">
                                            <div class="col-xs-8">
                                                <h5><span class="glyphicon glyphicon-shopping-cart"></span> Carrinho de Compras</h5>
                                            </div>
                                            <div class="col-xs-4">
                                                <a ui-sref="catalog()">
                                                    <button type="button" class="btn btn-primary btn-sm btn-block">
                                                        <span class="glyphicon glyphicon-share-alt"></span> Continuar compra
                                                    </button>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th class="col-xs-6">Produto</th>
                                                <th class="col-xs-2">Valor unitário</th>
                                                <th class="col-xs-2">Quantidade</th>
                                                <th class="col-xs-2">Subtotal Produto</th>
                                                <th class="col-xs-1"></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr ng-repeat="sku in cart.products">
                                                <th scope="row"><h5 class="product-name"><strong>{{sku.name}}</strong></h5></th>

                                                <td><h6><strong>{{sku.price| currency:"R$ "}}<span class="text-muted  text-right"></span></strong></h6></td>

                                                <td>
                                                    <div class="col-xs-8">
                                                        <input ng-blur="updateCart(sku)" type="number" class="form-control input-sm" min="0" max="20" size="8" ng-model="sku.quantity" value="{{sku.quantity}}" >
                                                    </div>
                                                </td>

                                                <td><h6><strong>{{sku.price * sku.quantity| currency:"R$ "}}</strong></h6></td>

                                                <td>
                                                    <form class="form-signin" name="{{sku.id}}" ng-submit="removeToCart(sku)">
                                                        <button type="submit" class="btn btn-link btn-xs">
                                                            <span class="glyphicon glyphicon-trash"> </span>
                                                        </button>
                                                    </form>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="panel-footer">
                                    <div class="row text-center">
                                        <div class="col-xs-9">
                                            <h5 class="text-right">Frete <strong>{{cart.freightCost| currency:"R$ "}}</strong></h5>
                                        </div>
                                        <div class="col-xs-9">
                                            <h4 class="text-right">Total <strong>{{cart.totalPriceProducts + cart.freightCost| currency:"R$ "}}</strong></h4>
                                        </div>
                                        <div ng-if="!customer" class="col-xs-3">
                                            <button type="button" data-toggle="modal" data-target="#loginModal" class="btn btn-success btn-block">
                                                Pagar
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div ng-if="customer" class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-info">
                                    <div class="panel-heading">
                                        <div class="panel-title">
                                            <h5>Endereço de entrega</h5>
                                        </div>
                                    </div>
                                    <div class="panel-body">
                                        <form class="form-horizontal">
                                            <fieldset>

                                                <div class="form-group">

                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label" for="recipient">DestinatÃ¡rio</label>
                                                        <div class="col-md-6">
                                                            <input id="recipient" type="text" name="recipient"  placeholder="DestinatÃ¡rio" ng-model="address.recipient" value="{{address.recipient}}" class="form-control input-md" required="">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label" for="zipCode">CEP</label>
                                                        <div class="col-md-6">
                                                            <input id="zipCode" name="zipCode" type="text" placeholder="00000000" maxlength="8" ng-model="address.zipCode" value="{{address.zipCode}}" ng-change="completeAddress();" class="form-control input-md" required="">
                                                            <span class="help-block">Somente nÃºmeros</span>
                                                        </div>
                                                    </div>

                                                    <label class="col-md-4 control-label" for="uf">Estado</label>
                                                    <div class="col-md-6">
                                                        <select id="uf" name="uf" ng-model="address.uf" class="form-control">
                                                            <option value="">Selecione o estado   </option>
                                                            <option value="AC">Acre                 </option>
                                                            <option value="AL">Alagoas              </option>
                                                            <option value="AP">Amapá                </option>
                                                            <option value="AM">Amazonas             </option>
                                                            <option value="BA">Bahia                </option>
                                                            <option value="CE">Ceará                </option>
                                                            <option value="DF">Distrito Federal     </option>
                                                            <option value="ES">Espírito Santo       </option>
                                                            <option value="GO">Goiás                </option>
                                                            <option value="MA">Maranhão             </option>
                                                            <option value="MT">Mato Grosso          </option>
                                                            <option value="MS">Mato Grosso do Sul   </option>
                                                            <option value="MG">Minas Gerais         </option>
                                                            <option value="PA">Pará                 </option>
                                                            <option value="PB">Paraiba              </option>
                                                            <option value="PR">Paraná               </option>
                                                            <option value="PE">Pernambuco           </option>
                                                            <option value="PI">Piauí                </option>
                                                            <option value="RJ">Rio de Janeiro       </option>
                                                            <option value="RN">Rio Grande do Norte  </option>
                                                            <option value="RS">Rio Grande do Sul    </option>
                                                            <option value="RO">Rondônia             </option>
                                                            <option value="RR">Roraima              </option>
                                                            <option value="SC">Santa Catarina       </option>
                                                            <option value="SP">São Paulo            </option>
                                                            <option value="SE">Sergipe              </option>
                                                            <option value="TO">Tocantins            </option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-4 control-label" for="city">Cidade</label>
                                                    <div class="col-md-6">
                                                        <input id="city" name="city" type="text" placeholder="Cidade" ng-model="address.city" value="{{address.city}}" class="form-control input-md" required="">

                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-4 control-label" for="neighborhood">Bairro</label>
                                                    <div class="col-md-6">
                                                        <input id="neighborhood" name="neighborhood" type="text" placeholder="Bairro" ng-model="address.neighborhood" value="{{address.neighborhood}}" class="form-control input-md" required="">

                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-4 control-label" for="street">Rua</label>
                                                    <div class="col-md-6">
                                                        <input id="street" name="street" type="text" placeholder="Rua" ng-model="address.street" value="{{address.street}}" class="form-control input-md" required="">

                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-4 control-label" for="number">NÃºmero</label>
                                                    <div class="col-md-6">
                                                        <input id="number" name="number" type="text" placeholder="NÃºmero" ng-model="address.number" value="{{address.number}}" class="form-control input-md" required="">

                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-4 control-label" for="complement">Complemento</label>
                                                    <div class="col-md-6">
                                                        <input id="complement" name="complement" type="text" placeholder="Complemento" ng-model="address.complement" value="{{address.complement}}" class="form-control input-md">

                                                    </div>
                                                </div>

                                            </fieldset>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div ng-if="customer" class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-info">
                                    <div class="panel-heading">
                                        <div class="panel-title">
                                            <h5><span><i class="glyphicon glyphicon-lock"></i></span> Pagamento</h5>
                                        </div>
                                    </div>
                                    <div class="panel-body" ng-controller="paymentController">
                                        <div ng-if="error.message" class="alert alert-danger" role="alert">
                                            <p id="error">{{error.message}}</p>
                                        </div>
                                        <button type="submit" ng-click="tiketPayment(address)" class="btn btn-default"><img src="img/carrinho/botaoBoleto.jpg" height="74px" /></button>
                                        <button type="submit" ng-click="pagSeguroPayment(address)" class="btn btn-default"><img src="img/carrinho/botaoPagSeguro.jpg" height="74px" /></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </c:when>
        <c:otherwise>
            <div class="alert alert-danger" role="alert">
                <p id="error">Carrinho vazio!</p>
            </div>
        </c:otherwise>
    </c:choose>

</div>