<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="jumbotron">
    <div class="row">
        <c:forEach items="${categories}" var="category">
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <a href="${ENDPOINT}categoria/${category.urlName}"><img src="${category.image}" height="400px" width="300px"></a>
                    <div class="caption">
                        <h3><a href="${ENDPOINT}categoria/${category.urlName}">${category.name}</a></h3>
                    </div>
                </div>
            </div>
        </c:forEach>
    </div>
