package br.com.commons.json;

import java.io.Serializable;

public class PagSeguroNotificationJson implements Serializable {

    private static final long serialVersionUID = 4765783085195940950L;

    private String notificationCode;
    private String notificationType;

    public String getNotificationCode() {
        return notificationCode;
    }

    public void setNotificationCode(String notificationCode) {
        this.notificationCode = notificationCode;
    }

    public String getNotificationType() {
        return notificationType;
    }

    public void setNotificationType(String notificationType) {
        this.notificationType = notificationType;
    }

}
