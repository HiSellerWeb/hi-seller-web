package br.com.commons.json;

import com.google.gson.Gson;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class Json {

    private static final Gson GSON = new Gson();

    @SuppressWarnings("unchecked")
    public static final <T> T toObject(String json, Class<T> type) {
        if (json == null) {
            return null;
        }
        try {
            return GSON.fromJson(URLDecoder.decode(json, "UTF-8"), type);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Json.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static String toJson(Object object) {
        return GSON.toJson(object);
    }

}
