package br.com.commons.json;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@JsonSerialize
@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class CustomerJson extends AccountJson implements Serializable {

    private static final long serialVersionUID = 4220359298807145518L;

    private List<Long> addresses = new ArrayList<>();

    public CustomerJson() {
    }

    public CustomerJson(AccountJson accountJson) {
        super(accountJson);
    }

    public void addAddress(Long addressId) {
        getAddresses().add(addressId);
    }

    public List<Long> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<Long> addresses) {
        this.addresses = addresses;
    }
}
