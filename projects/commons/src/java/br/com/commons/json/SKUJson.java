package br.com.commons.json;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonSerialize
@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class SKUJson extends SkuCartJson implements Serializable {

    private static final long serialVersionUID = -3736809087595835867L;

    private Long id;
    private Long storeId;
    private ProductJson product;
    private BigDecimal price = BigDecimal.ZERO;
    private String code;
    private Long weight = 0L;
    private Integer available = 0;
    private Integer sold = 0;
    private Integer stock = 0;
    private List<String> images = new ArrayList<>();
    private Integer sessionReserve = 0;
    private Integer orderReserve = 0;
    private Map<String, String> rows = new HashMap<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ProductJson getProduct() {
        return product;
    }

    public void setProduct(ProductJson product) {
        this.product = product;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getWeight() {
        return weight;
    }

    public void setWeight(Long weight) {
        this.weight = weight;
    }

    public Integer getAvailable() {
        return available;
    }

    public void setAvailable(Integer available) {
        this.available = available;
    }

    public Integer getSold() {
        return sold;
    }

    public void setSold(Integer sold) {
        this.sold = sold;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public Long getStoreId() {
        return storeId;
    }

    public void setStoreId(Long storeId) {
        this.storeId = storeId;
    }

    public List<String> getImages() {
        return images;
    }

    public void addImage(String image) {
        if (this.images == null) {
            this.images = new ArrayList<>();
        }
        this.images.add(image);
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public Integer getSessionReserve() {
        return sessionReserve;
    }

    public void setSessionReserve(Integer sessionReserve) {
        this.sessionReserve = sessionReserve;
    }

    public Integer getOrderReserve() {
        return orderReserve;
    }

    public void setOrderReserve(Integer orderReserve) {
        this.orderReserve = orderReserve;
    }

    public Map<String, String> getRows() {
        return rows;
    }

    public void setRows(Map<String, String> rows) {
        this.rows = rows;
    }

    public void putRow(String key, String value) {
        if (this.rows == null) {
            this.rows = new HashMap<>();
        }
        this.rows.put(key, value);
    }

}
