package br.com.commons.json;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.io.Serializable;

@JsonSerialize
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PaymentJson implements Serializable {

    private static final long serialVersionUID = -5120717198301509031L;
    private String authenticationToken;
    private String orderToken;
    private AddressJson address;
    private String pagSeguroCheckoutURL;

    public PaymentJson() {
    }

    public PaymentJson(String authenticationToken) {
        this.authenticationToken = authenticationToken;
    }

    public String getAuthenticationToken() {
        return authenticationToken;
    }

    public void setAuthenticationToken(String authenticationToken) {
        this.authenticationToken = authenticationToken;
    }

    public AddressJson getAddress() {
        return address;
    }

    public void setAddress(AddressJson address) {
        this.address = address;
    }

    public String getOrderToken() {
        return orderToken;
    }

    public void setOrderToken(String orderToken) {
        this.orderToken = orderToken;
    }

    public String getPagSeguroCheckoutURL() {
        return pagSeguroCheckoutURL;
    }

    public void setPagSeguroCheckoutURL(String pagSeguroCheckoutURL) {
        this.pagSeguroCheckoutURL = pagSeguroCheckoutURL;
    }
}
