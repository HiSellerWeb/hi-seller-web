package br.com.commons.json;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.io.Serializable;
import java.math.BigDecimal;

@JsonSerialize
@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class SkuCartJson implements Serializable {

    private static final long serialVersionUID = 1460376634058574499L;

    private Long id;
    private String name;
    private BigDecimal price = BigDecimal.ZERO;
    private Long weight;
    private Integer quantity = 0;
    private String imgUrl = "";
    private String code = "";

    public SkuCartJson() {
    }

    public SkuCartJson(ProductMovJson productMovJson) {
        this.id = productMovJson.getId();
        this.quantity = productMovJson.getQuantity();
    }

    public ProductMovJson toProductMovJson() {
        return new ProductMovJson(this.id, this.quantity);
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public Long getWeight() {
        return weight;
    }

    public void setWeight(Long weight) {
        this.weight = weight;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
