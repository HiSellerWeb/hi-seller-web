package br.com.commons.json;

import java.io.Serializable;

public class AlabastrumAccountJson implements Serializable {

    private static final long serialVersionUID = 7494854745004687365L;

    private AlabastrumUsuarioJson usuario;

    public AlabastrumAccountJson() {
    }

    public AlabastrumUsuarioJson getUsuario() {
        return usuario;
    }

    public void setUsuario(AlabastrumUsuarioJson usuario) {
        this.usuario = usuario;
    }
}
