package br.com.commons.json;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.io.Serializable;

@JsonSerialize
@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ManagerJson extends AccountJson implements Serializable {

    private static final long serialVersionUID = 6613047743813807477L;

    public ManagerJson() {
    }

    public ManagerJson(AccountJson accountJson) {
        super(accountJson);
    }

}
