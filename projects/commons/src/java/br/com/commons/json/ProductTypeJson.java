package br.com.commons.json;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.io.Serializable;

@JsonSerialize
@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductTypeJson implements Serializable {

    private static final long serialVersionUID = -2178037486418651218L;

    private String frontEndName;
    private String fieldName;
    private boolean request;
    private int min;
    private int max;

    public ProductTypeJson(String frontEndName, String fieldName, boolean request, int min, int max) {
        this.frontEndName = frontEndName;
        this.fieldName = fieldName;
        this.request = request;
        this.min = min;
        this.max = max;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public boolean isRequest() {
        return request;
    }

    public void setRequest(boolean request) {
        this.request = request;
    }

    public Integer getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public String getFrontEndName() {
        return frontEndName;
    }

    public void setFrontEndName(String frontEndName) {
        this.frontEndName = frontEndName;
    }
}
