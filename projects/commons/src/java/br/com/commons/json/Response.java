package br.com.commons.json;

import br.com.commons.validator.ValidationError;
import java.util.List;

public class Response {

    private int httpStatusCode;
    private String message;
    private List<ValidationError> errors;

    public Response(int httpStatusCode, String message, List<ValidationError> errors) {
        this.httpStatusCode = httpStatusCode;
        this.message = message;
        this.errors = errors;
    }

    public String toJson() {
        return Json.toJson(this);
    }
}
