package br.com.commons.json;

import java.io.Serializable;

public class AlabastrumUsuarioJson implements Serializable {

    private static final long serialVersionUID = 7494854745004687365L;

    private String CPF;
    private String vNome;
    private String eMail;

    public String getCPF() {
        return CPF;
    }

    public void setCPF(String CPF) {
        this.CPF = CPF;
    }

    public String getvNome() {
        return vNome;
    }

    public void setvNome(String vNome) {
        this.vNome = vNome;
    }

    public String geteMail() {
        return eMail;
    }

    public void seteMail(String eMail) {
        this.eMail = eMail;
    }
}
