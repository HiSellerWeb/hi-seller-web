package br.com.commons.model;

public enum ConfigTypeEnum {

    INDIVIDUAL("INDIVIDUAL"),
    GLOBAL("GLOBAL");

    private String name;

    ConfigTypeEnum(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
