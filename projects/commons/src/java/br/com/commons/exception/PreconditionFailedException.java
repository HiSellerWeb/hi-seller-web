package br.com.commons.exception;

import org.springframework.http.HttpStatus;

public class PreconditionFailedException extends RestHttpException {

    private static final long serialVersionUID = -718217387115543523L;

    public PreconditionFailedException(String message) {
        super(message, HttpStatus.PRECONDITION_FAILED);
    }
}
