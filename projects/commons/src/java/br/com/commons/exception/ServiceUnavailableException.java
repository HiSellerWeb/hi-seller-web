package br.com.commons.exception;

import org.springframework.http.HttpStatus;

public class ServiceUnavailableException extends RestHttpException {

    private static final long serialVersionUID = -1898559451816289518L;

    public ServiceUnavailableException(String message) {
        super(message, HttpStatus.SERVICE_UNAVAILABLE);
    }
}
