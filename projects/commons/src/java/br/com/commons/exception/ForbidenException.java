package br.com.commons.exception;

import org.springframework.http.HttpStatus;

public class ForbidenException extends RestHttpException {

    private static final long serialVersionUID = -8237844461730375549L;

    public ForbidenException() {
        this("Acesso negado");
    }

    public ForbidenException(String message) {
        super(message, HttpStatus.FORBIDDEN);
    }
}
