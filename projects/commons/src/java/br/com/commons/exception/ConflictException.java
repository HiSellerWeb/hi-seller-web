package br.com.commons.exception;

import org.springframework.http.HttpStatus;

public class ConflictException extends RestHttpException {

    private static final long serialVersionUID = 134536647464345L;

    public ConflictException() {
        this("Este recurso ja existe");
    }

    public ConflictException(String message) {
        super(message, HttpStatus.CONFLICT);
    }
}
