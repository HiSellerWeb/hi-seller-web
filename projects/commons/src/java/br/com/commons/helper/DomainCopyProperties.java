package br.com.commons.helper;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;

public class DomainCopyProperties {

    /**
     *
     * @param source: objeto a ser copiado
     * @param target: objeto a ser alterado
     * @param ignoredPropertiesNames: array de strings com os nomes dos atributos que NÃO devem ser copiados
     */
    public static void copyPropertiesIgnored(Object source, Object target, String... ignoredPropertiesNames) {
        if (source == null) {
            return;
        }

        BeanUtils.copyProperties(source, target, ignoredPropertiesNames);
    }

    /**
     *
     * @param source: objeto a ser copiado
     * @param target: objeto a ser alterado
     * @param allowedPropertiesNames: array de strings com os nomes dos atributos que podem devem ser copiados
     */
    public static void copyPropertiesAllowed(Object source, Object target, String... allowedPropertiesNames) {
        if (source == null) {
            return;
        }

        BeanUtils.copyProperties(source, target, getIgnoredPropertiesNames(source, Arrays.asList(allowedPropertiesNames)));
    }

    private static String[] getIgnoredPropertiesNames(Object source, List<String> propertiesNamesAllowed) {
        final BeanWrapper src = new BeanWrapperImpl(source);
        java.beans.PropertyDescriptor[] descriptors = src.getPropertyDescriptors();

        Set<String> ignoredPropertyNames = new HashSet<>();

        for (java.beans.PropertyDescriptor propertyDescriptor : descriptors) {
            Boolean isWritableProperty = src.isWritableProperty(propertyDescriptor.getName());

            if (isWritableProperty) {
                Object srcValue = src.getPropertyValue(propertyDescriptor.getName());

                if (srcValue == null || !propertiesNamesAllowed.contains(propertyDescriptor.getName())) {
                    ignoredPropertyNames.add(propertyDescriptor.getName());
                }
            }
        }

        return ignoredPropertyNames.toArray(new String[ignoredPropertyNames.size()]);
    }
}
