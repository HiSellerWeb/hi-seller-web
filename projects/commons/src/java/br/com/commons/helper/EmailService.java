package br.com.commons.helper;

import br.com.commons.json.ContactEmailJson;
import java.util.Arrays;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;

public class EmailService {

    static final Logger LOG = Logger.getLogger(EmailService.class);

    private HtmlEmail getEmailInstance() throws EmailException {
        HtmlEmail email = new HtmlEmail();
        email.setHostName("smtp.googlemail.com");
        email.setSmtpPort(465);
        email.setAuthenticator(new DefaultAuthenticator("contato.alabastrum@gmail.com", "contato.alabastrum@123"));
        email.setSSLOnConnect(true);
        email.setFrom("contato.alabastrum@gmail.com");
        return email;
    }

    public void sendRecoveryPasswordEmail(String email, String nickName, String password) throws Exception {
        HtmlEmail emailToSend = getEmailInstance();

        emailToSend.setSubject("Alteraçoã de senha");
        emailToSend.addTo("hebertsouza87@gmail.com");

        StringBuilder sb = new StringBuilder();
        sb.append("<h2>Loja Alabastrum</h2>");
        sb.append("<h3>Recuperação de Senha</h3>");

        sb.append("<div>Olá, ##NICKNAME##!</div>");
        sb.append("<br>");
        sb.append("<div>Foi solicitado em nosso sistema uma nova senha de acesso para a loja da <a href=\"http://www.loja.alabastrum.com.br\">Alabastrum</a>.</div>");
        sb.append("<br>");
        sb.append("<div>Segue abaixo os novos dados para acesso ao sistema:</div>");
        sb.append("<div><strong>E-mail: </strong>##EMAIL##</div>");
        sb.append("<div><strong>Senha: </strong>##PASSWORD##</div>");
        sb.append("<br>");
        sb.append("<div>Lembrando que é possível alterar a senha através do menu minha conta.</div>");
        sb.append("<br>");
        sb.append("<br>");
        sb.append("<div>Este é um e-mail automático, favor não responde-lo. Para entrar e contato, utilize o menu Contato no site da loja.</div>");
        sb.append("<br>");
        sb.append("<br>");
        sb.append("<div>Atenciosamente,</div>");
        sb.append("<div>Alabastrum</div>");

        String msg = sb.toString().replaceAll("##NICKNAME##", nickName).replaceAll("##EMAIL##", email).replaceAll("##PASSWORD##", password);
        emailToSend.setHtmlMsg(msg);
        emailToSend.send();
    }

    public void sendContactEmail(ContactEmailJson contactEmail) throws EmailException, AddressException {
        try {
            HtmlEmail emailToSend = getEmailInstance();

            emailToSend.setReplyTo(Arrays.asList(new InternetAddress(contactEmail.getEmail())));
            emailToSend.setSubject(contactEmail.getAssunto());
            emailToSend.addTo("contato.alabastrum@gmail.com");
//            emailToSend.addTo("atendimento@alabastrum.com.br");

            emailToSend.setHtmlMsg("<html>"
                    + "<center><h2>E-mail de contato do site</h2></center>"
                    + "<strong>Nome:</strong> " + Jsoup.parse(contactEmail.getNome()).text() + "<br>"
                    + "<strong>E-mail:</strong> " + Jsoup.parse(contactEmail.getEmail()).text() + "<br>"
                    + "<strong>Telefone:</strong> " + Jsoup.parse(contactEmail.getTelefone()).text() + "<br>"
                    + "<strong>Cidade:</strong>  " + Jsoup.parse(contactEmail.getCidade()).text() + "<br>"
                    + "<strong>Assinar Newsletter?</strong> " + Jsoup.parse(contactEmail.getNewsletter()).text() + "<br><br>"
                    + "<strong>Mensagem:</strong> " + Jsoup.parse(contactEmail.getMensagem()).text() + "<br>"
                    + "</html>");
            emailToSend.send();
        } catch (Exception e) {
            LOG.error("No foi possvel enviar o e-mail", e);
        }

    }
}
