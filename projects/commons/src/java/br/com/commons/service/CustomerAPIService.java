package br.com.commons.service;

import br.com.commons.controller.AbstractController;
import br.com.commons.json.CustomerJson;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
@Deprecated
public class CustomerAPIService extends AbstractController {

    public CustomerJson findByToken(String accountUrl, String authentication, String token) throws Exception {
        final ResponseEntity<CustomerJson> entity = getForEntity(accountUrl + "/customer/" + token, authentication, CustomerJson.class);
        if (HttpStatus.OK.equals(entity.getStatusCode())) {
            return entity.getBody();
        }
        return null;
    }
}
