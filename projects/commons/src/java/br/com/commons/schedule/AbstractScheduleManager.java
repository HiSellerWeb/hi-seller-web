package br.com.commons.schedule;

import br.com.commons.utils.TimeUtil;
import java.util.Timer;
import java.util.TimerTask;
import javax.annotation.PostConstruct;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public abstract class AbstractScheduleManager {

    private static final Log LOG = LogFactory.getLog(AbstractScheduleManager.class);

    private final Timer timer;

    public AbstractScheduleManager() {
        this.timer = new Timer();
    }

    @PostConstruct
    public abstract void schedule();

    protected void scheduleTask(TimerTask task, Long delayInMinutes, Long periodicityInMinutes) {
        LOG.info("Agendando [" + task.getClass().getSimpleName() + "] para rodar a cada [" + periodicityInMinutes + "] minutos."
                + " A primeira execucao acontecera daqui ha [" + delayInMinutes + "] minutos");

        timer.schedule(task, toMilliseconds(delayInMinutes), toMilliseconds(periodicityInMinutes));
    }

    protected Long toMilliseconds(Long minutes) {
        return TimeUtil.minutesToMilliseconds(minutes);
    }

    protected void cancelSchedule(TimerTask task) {
        if (task != null) {
            LOG.info("Suspendendo agendamentos anteriores para [" + task.getClass().getSimpleName() + "]");
            task.cancel();
        }
    }
}
