package br.com.commons.domain;

import br.com.commons.json.Json;
import br.com.commons.json.ProductTypeJson;
import com.google.common.collect.ImmutableMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public enum TypeEnum {

    DEFAULT("Padrão", Arrays.asList(),
            ImmutableMap.<String, String>builder()
                    .put("Padrão 1", "padrao_layout1")
                    .build()),
    PERFUME("Perfume", Arrays.asList(
            new ProductTypeJson("Genero", "gender", true, 1, 1),
            new ProductTypeJson("Inspiração", "inspiration", true, 3, 20),
            new ProductTypeJson("Tipo", "type", true, 3, 20),
            new ProductTypeJson("Intencidade", "intensity", true, 3, 20),
            new ProductTypeJson("mL", "size", true, 1, 5)),
            ImmutableMap.<String, String>builder()
                    .put("Perfume 1", "perfume_layout1")
                    .build());

    private String frontName;
    private List<ProductTypeJson> fields;

    //Mapa com os possíveis layots para o tipo de produto.
    //Chave = Nome do layout (como será exibido no front)
    //Valor = Nome do arquivo .html que será usado para este layout
    private Map<String, String> layouts;

    TypeEnum(String frontName, List<ProductTypeJson> fields, Map<String, String> layouts) {
        this.frontName = frontName;
        this.fields = fields;
        this.layouts = layouts;
    }

    public List<ProductTypeJson> getFields() {
        return fields;
    }

    public void setFields(List<ProductTypeJson> fields) {
        this.fields = fields;
    }

    public Map<String, String> getLayouts() {
        return layouts;
    }

    public void setLayouts(Map<String, String> layouts) {
        this.layouts = layouts;
    }

    public String toJson() {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        sb.append("\"name\":");
        sb.append("\"").append(this).append("\",");
        sb.append("\"frontName\":");
        sb.append("\"").append(frontName).append("\",");
        sb.append("\"fields\":");
        sb.append(Json.toJson(fields)).append(",");
        sb.append("\"layouts\":");
        sb.append(Json.toJson(layouts));
        sb.append("}");
        return sb.toString();
    }

    public static String allToJson() {
//        StringBuilder sb = new StringBuilder();
        List<String> values = new ArrayList<>();
//        sb.append("{");
        for (int i = 0; i < values().length; i++) {
            values.add(values()[i].toJson());
        }
//        sb.append(values.toString());
//        sb.append("}");
        return values.toString();
    }
}
