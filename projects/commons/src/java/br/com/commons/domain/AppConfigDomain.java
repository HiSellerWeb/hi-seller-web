package br.com.commons.domain;

import br.com.commons.appConfig.ConfigEnumInterface;
import br.com.commons.model.ConfigTypeEnum;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Table(name = "app_config")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class AppConfigDomain implements Serializable {

    private static final long serialVersionUID = 8746387L;

    @Id
    @SequenceGenerator(name = "app_config_id", sequenceName = "app_config_id_seg")
    @GeneratedValue(generator = "app_config_id", strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "name", nullable = false, unique = true, length = 255)
    private String name;

    @Column(name = "value", nullable = false, length = 255)
    private String value;

    @Column(name = "storeId", nullable = false, updatable = false)
    private Long storeId;

    @Column(name = "configType", nullable = false)
    @Enumerated(EnumType.STRING)
    private ConfigTypeEnum configType;

    public AppConfigDomain() {
    }

    public AppConfigDomain(ConfigEnumInterface configEnum) {
        this.name = configEnum.getName();
        this.value = configEnum.getDefaultValue();
        this.storeId = configEnum.getStoreId();
        this.configType = configEnum.getConfigType();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Long getStoreId() {
        return storeId;
    }

    public void setStoreId(Long storeId) {
        this.storeId = storeId;
    }

    public ConfigTypeEnum getConfigType() {
        return configType;
    }

    public void setConfigType(ConfigTypeEnum configType) {
        this.configType = configType;
    }

}
