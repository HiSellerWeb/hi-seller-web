package br.com.commons.dao;

import br.com.commons.domain.AppConfigDomain;
import br.com.commons.model.ConfigTypeEnum;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Transactional(propagation = Propagation.REQUIRED)
public class AppConfigDao extends AbstractDao<AppConfigDomain> {

    @Override
    public AppConfigDomain save(AppConfigDomain appConfigDomain) {
        return super.save(appConfigDomain);
    }

    public AppConfigDomain findByName(Long storeId, String name) {
        List<Criterion> restrictions = new ArrayList<>();
        restrictions.add(Restrictions.or(Restrictions.eq("storeId", storeId), Restrictions.eq("configType", ConfigTypeEnum.GLOBAL)));
        restrictions.add(Restrictions.eq("name", name));

        List<AppConfigDomain> orders = super.findByRestrictions(AppConfigDomain.class, restrictions);
        if (orders == null || orders.isEmpty()) {
            return null;
        }

        return orders.get(0);

    }

    public List<AppConfigDomain> findAll() {
        return super.findAll(AppConfigDomain.class);
    }

}
