package br.com.commons.appConfig;

import br.com.commons.model.ConfigTypeEnum;

public interface ConfigEnumInterface<T> {

    public String getName();

    public String getDefaultValue();

    public Long getStoreId();

    public ConfigTypeEnum getConfigType();

    public T[] getValues();
}
