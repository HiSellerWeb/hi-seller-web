package br.com.commons.appConfig;

import br.com.commons.dao.AppConfigDao;
import br.com.commons.domain.AppConfigDomain;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class AppConfig {

    private final static Log LOG = LogFactory.getLog(AppConfig.class);

    private static final Map<String, String> PROPERTIES = new HashMap<>();

    @Autowired
    private AppConfigDao appConfigDao;

//    @PostConstruct
    public String getValue(Long storeId, ConfigEnumInterface configEnum) {
        try {
            if (!PROPERTIES.containsKey(configEnum.getName())) {
                findAndSet(storeId, configEnum);
            }
        } catch (Exception e) {
            LOG.error("Error ao carregar o valor do appConfig para o item: " + configEnum.getName());
        }

        return PROPERTIES.get(configEnum.getName());
    }

//    @PostConstruct
    public Long getValueLongDefault(Long storeId, ConfigEnumInterface configEnum, Long def) {
        try {
            if (!PROPERTIES.containsKey(configEnum.getName())) {
                findAndSet(storeId, configEnum);
            }
        } catch (Exception e) {
            LOG.error("Error ao carregar o valor do appConfig para o item: " + configEnum.getName());
        }

        String get = PROPERTIES.get(configEnum.getName());

        if (StringUtils.isAnyBlank(get)) {
            return def;
        }
        return Long.valueOf(get);
    }

    public Long getValueAsLong(Long storeId, ConfigEnumInterface configEnum) {
        return Long.valueOf(getValue(storeId, configEnum));
    }

    public int getValueAsInteger(Long storeId, ConfigEnumInterface configEnum) {
        return Integer.valueOf(getValue(storeId, configEnum));
    }

    public void loadAppConfigProperties(Long storeId, ConfigEnumInterface[] configEnumValues) {
        try {
            PROPERTIES.clear();
            for (ConfigEnumInterface configEnumValue : configEnumValues) {
                findAndSet(storeId, configEnumValue);
            }

        } catch (Exception e) {
            LOG.error("Erro ao carregar APP_CONFIG", e);
            throw new RuntimeException("Appconfig nao pode ser carregado");
        }
    }

    public static Map<String, String> getPROPERTIES() {
        return PROPERTIES;
    }

    private void findAndSet(Long storeId, ConfigEnumInterface configEnum) {
        AppConfigDomain config = appConfigDao.findByName(storeId, configEnum.getName());
        if (config == null) {
            config = new AppConfigDomain(configEnum);
            config = appConfigDao.save(config);
        }

        PROPERTIES.put(config.getName(), config.getValue());
    }
}
