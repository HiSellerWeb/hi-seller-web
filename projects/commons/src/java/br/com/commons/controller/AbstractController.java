package br.com.commons.controller;

import br.com.commons.exception.ForbidenException;
import br.com.commons.json.CustomerJson;
import br.com.commons.json.ManagerJson;
import java.io.Serializable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

public abstract class AbstractController {

    @Autowired
    private RestTemplate restTemplate;

    public <T> ResponseEntity<T> postForEntity(String url, String authorization, Serializable body, Class<T> responseType) {
        return restTemplate.exchange(url, HttpMethod.POST, getRequest(body, authorization), responseType);
    }

    public <T> ResponseEntity<T> getForEntity(String url, String authorization, Class<T> responseType) {
        return restTemplate.exchange(url, HttpMethod.GET, getRequest(null, authorization), responseType);
    }

    public <T> ResponseEntity<T> deleteForEntity(String url, String authorization, Class<T> responseType) {
        return restTemplate.exchange(url, HttpMethod.DELETE, getRequest(null, authorization), responseType);
    }

    public <T> ResponseEntity<T> putForEntity(String url, String authorization, Serializable body, Class<T> responseType) {
        return restTemplate.exchange(url, HttpMethod.PUT, getRequest(body, authorization), responseType);
    }

    public Long validateAuthorization(String authorization) {
//        TODO: Hebert
//        Método que vai validar a autorização e retornar o storeID
//              validar se o valor passado já é um storeID, nesse caso a principio é para não lançar exceção e retornar nullo. procedimento necessário para não impactar a segurança da aplicação

//        Vai haver um metodo que irá receber o storeID e retornará para o front com a chave de autorização.
//          essa chave será enviada junto com as configurações no prieiro acesso. será como o token, só que retá uma validade maior
        return Long.valueOf(authorization);
    }

    public void validateManagerToken(String accountUrl, Long storeId, String token) throws ForbidenException {
        try {
            ResponseEntity<ManagerJson> entity = restTemplate.getForEntity(accountUrl + "/manager/" + token, ManagerJson.class);
//            ManagerJson managerJson = entity.getBody();
            if (!HttpStatus.OK.equals(entity.getStatusCode())) {
                throw new ForbidenException();
            }
        } catch (Exception e) {
            throw new ForbidenException();
        }
    }

    public void validateCustomerToken(String accountUrl, Long storeId, String token) throws ForbidenException {
        try {
            ResponseEntity<CustomerJson> entity = restTemplate.getForEntity(accountUrl + "/customer/" + token, CustomerJson.class);
//            CustomerJson managerJson = entity.getBody();
            if (!HttpStatus.OK.equals(entity.getStatusCode())) {
                throw new ForbidenException();
            }
        } catch (Exception e) {
            throw new ForbidenException();
        }
    }

    public RestTemplate getRestTemplate() {
        return restTemplate;
    }

    private HttpEntity getRequest(Object body, String authorization) {
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.set("Authorization", authorization);

        return new HttpEntity(body, headers);
    }

}
