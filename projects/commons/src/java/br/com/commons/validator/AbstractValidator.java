package br.com.commons.validator;

import br.com.caelum.stella.validation.CNPJValidator;
import br.com.caelum.stella.validation.CPFValidator;
import br.com.caelum.stella.validation.Validator;
import br.com.commons.exception.UnprocessableEntityException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public abstract class AbstractValidator<T> {

    private static final String STRING_EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    private static final Pattern EMAIL_PATTERN = Pattern.compile(STRING_EMAIL_PATTERN);

    private static final Log LOG = LogFactory.getLog(AbstractValidator.class);

    private boolean invalid;
    private List<ValidationError> validationErrors;
    protected List<AbstractValidator<T>> validators;

    public AbstractValidator() {
        validationErrors = new ArrayList<ValidationError>();
        validators = new ArrayList<AbstractValidator<T>>();
    }

    public AbstractValidator<T> createError(String field, RestrictionType type, String error) {
        clearState();
        validationErrors = new ArrayList<ValidationError>();
        validationErrors.add(new ValidationError(field, type, error));

        return this;
    }

    public void validate(T validatable, AbstractValidator<T>... validators) throws UnprocessableEntityException {
        if (!isValid(validatable, validators)) {
            throw new UnprocessableEntityException("Nao foi possível processar as instruções contidas na requisição", getValidationErrors());
        }
    }

    public Boolean isValid(T validatable, AbstractValidator<T>... validators) {
        clearState();
        this.validators.addAll(Arrays.asList(validators));

        this.apply(validatable);
        for (AbstractValidator<T> validator : this.validators) {
            validator.clearState();
            validator.apply(validatable);
            validationErrors.addAll(validator.getValidationErrors());
        }

        return validationErrors.isEmpty();
    }

    protected abstract void apply(T validatable);

    protected AbstractValidator<T> isBlank(Object value) {
        invalid = StringUtils.isBlank(String.valueOf(value));
        return this;
    }

    protected AbstractValidator<T> isNull(Object object) {
        invalid = (object == null);
        return this;
    }

    protected AbstractValidator<T> isNotNull(Object object) {
        invalid = (object != null);
        return this;
    }

    public void addErrors(List<ValidationError> errors) {
        validationErrors.addAll(errors);
    }

    public void setError(String field, RestrictionType type, String reason) {
        if (invalid) {
            validationErrors.add(new ValidationError(field, type, reason));
            invalid = false;
        }
    }

    public boolean isInvalid() {
        return invalid;
    }

    protected void clearState() {
        invalid = false;
        validationErrors.clear();
        validators.clear();
    }

    public AbstractValidator<T> setInvalid(boolean invalid) {
        this.invalid = invalid;
        return this;
    }

    protected AbstractValidator<T> isMaxLength(String value, int max) {
        value = StringUtils.trim(value);
        setInvalid(StringUtils.isNotBlank(value) && value.length() > max);
        return this;
    }

    protected AbstractValidator<T> isMinLength(String value, int min) {
        value = StringUtils.trim(value);
        setInvalid(StringUtils.isNotBlank(value) && value.length() < min);
        return this;
    }

    protected AbstractValidator<T> isLengthBetween(String value, int min, int max) {
        boolean invalidMaxLength = isMaxLength(value, max).isInvalid();
        boolean invalidMinLength = isMinLength(value, min).isInvalid();
        setInvalid(invalidMinLength || invalidMaxLength);
        return this;
    }

    protected AbstractValidator<T> isLengthBetween(int value, int min, int max) {
        boolean invalidMaxLength = value > max;
        boolean invalidMinLength = value < min;
        setInvalid(invalidMinLength || invalidMaxLength);
        return this;
    }

    protected AbstractValidator<T> isValidEmail(String email) {
        setInvalid(StringUtils.isBlank(email) || !EMAIL_PATTERN.matcher(email).matches());
        return this;
    }

    protected AbstractValidator<T> isValidCpf(String cpf) {
        try {
            Validator<String> cpfValidator = new CPFValidator(false);
            cpfValidator.assertValid(cpf);
            setInvalid(false);
        } catch (Exception e) {
            setInvalid(true);
        }

        if ("00000000000".equals(cpf)) {
            setInvalid(true);
        }

        return this;
    }

    protected AbstractValidator<T> isValidCnpj(String cnpj) {
        try {
            Validator<String> cnpjValidator = new CNPJValidator(false);
            cnpjValidator.assertValid(cnpj);
            setInvalid(false);
        } catch (Exception e) {
            setInvalid(true);
        }

        if ("00000000000000".equals(cnpj)) {
            setInvalid(true);
        }

        return this;
    }

    public List<ValidationError> getValidationErrors() {
        return validationErrors;
    }
}
