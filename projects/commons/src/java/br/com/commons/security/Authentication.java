package br.com.commons.security;

import br.com.commons.exception.ForbidenException;
import java.util.Arrays;
import java.util.List;
import org.joda.time.DateTime;

public class Authentication {

    private static final String SEPARATOR = "##";
    private final String id;
    private final AuthenticationRole authenticationRole;
    private DateTime expirationDate;

    /**
     *
     * @param id
     * @param authenticationRole
     */
    public Authentication(Long id, AuthenticationRole authenticationRole) {
        this(String.valueOf(id), authenticationRole);
    }

    /**
     *
     * @param id
     * @param authenticationRole
     */
    public Authentication(String id, AuthenticationRole authenticationRole) {
        this.id = id;
        this.authenticationRole = authenticationRole;
        this.expirationDate = new DateTime().plusMinutes(30);
    }

    /**
     *
     * @param id
     * @param authenticationRole
     * @param expirationDate
     */
    public Authentication(String id, AuthenticationRole authenticationRole, DateTime expirationDate) {
        this.id = id;
        this.authenticationRole = authenticationRole;
        this.expirationDate = expirationDate;
    }

    public Boolean isExpired() {
        return new DateTime().isAfter(expirationDate);
    }

    private Long getExpirationDateInMillis() {
        if (expirationDate == null) {
            expirationDate = new DateTime().plusMinutes(30);
        }

        return expirationDate.getMillis();
    }

    public static Authentication fromString(String authentication) throws Exception {
        String decrypt = Encrypter.decrypt(authentication);
        if (decrypt == null) {
            throw new ForbidenException();
        }
        List<String> parts = Arrays.asList(decrypt.split(SEPARATOR));

        if (parts.size() != 3) {
            throw new ClassCastException("Impossivel transformar [" + authentication + "] em Authentication");
        }

        String id = parts.get(0);
        AuthenticationRole authenticationRole = AuthenticationRole.valueOf(parts.get(1));
        DateTime expirationDate = new DateTime(Long.valueOf(parts.get(2)));

        return new Authentication(id, authenticationRole, expirationDate);
    }

    private String getStringToCrypt() {
        return new StringBuilder()
                .append(getId()).append(SEPARATOR)
                .append(getAuthenticationRole()).append(SEPARATOR)
                .append(getExpirationDateInMillis())
                .toString();
    }

    public String getAuthenticationCode() throws Exception {
        return Encrypter.encrypt(getStringToCrypt());
    }

    public DateTime getExpirationDate() {
        return expirationDate;
    }

    public void updateExpirationDate() {
        this.expirationDate = new DateTime().plusMinutes(30);
    }

    public String getId() {
        return id;
    }

    public AuthenticationRole getAuthenticationRole() {
        return authenticationRole;
    }

}
