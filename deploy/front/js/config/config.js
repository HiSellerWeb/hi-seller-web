function getConfig() {
    var config = {};
    config.title = "Loja Alabastrum";
    config.accountUrl = "http://106.186.24.86:8080/account/";
    config.checkoutUrl = "http://106.186.24.86:8080/checkout/";
    config.stockUrl = "http://106.186.24.86:8080/stock/";
    config.pagSeguro_checkout = "https://pagseguro.uol.com.br/v2/checkout/payment.html?code=";

    return config;
}
