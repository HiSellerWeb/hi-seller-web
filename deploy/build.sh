echo 'removendo arquivos antigos'
rm -rf /temp/account/*
rm -rf /temp/checkout/*
rm -rf /temp/stock/*
rm -rf /temp/front/*

echo 'copiando arquivos .war'
cp ../projects/account/dist/account.war ./temp/account
cp ../projects/checkout/dist/checkout.war ./temp/checkout
cp ../projects/stock/dist/stock.war ./temp/stock

echo 'copiando arquivos do front'
cp -fr ../projects/front/public_html/* ./temp/front

echo 'extraindo arquivos'
cd temp/account
jar -xf account.war
cd ../checkout
jar -xf checkout.war
cd ../stock
jar -xf stock.war
cd ../..

echo 'substituindo arquivos de configuração'
cp -fr account temp/
cp -fr checkout temp/
cp -fr stock temp/
cp -fr front temp/

echo 'gerando .war'
cd temp/account
jar -cf account.war *
cd ../checkout
jar -cf checkout.war *
cd ../stock
jar -cf stock.war *
cd ../..





